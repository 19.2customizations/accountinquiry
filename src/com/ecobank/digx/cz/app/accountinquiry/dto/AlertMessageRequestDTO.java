package com.ecobank.digx.cz.app.accountinquiry.dto;


import com.ofss.digx.app.common.dto.DataTransferObject;

public class AlertMessageRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	
    private String affCode;
	private String mobileNo;
	private String message;
	
	
	public String getAffCode() {
		return affCode;
	}
	public void setAffCode(String affCode) {
		this.affCode = affCode;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
