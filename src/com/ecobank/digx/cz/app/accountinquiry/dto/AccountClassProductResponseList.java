package com.ecobank.digx.cz.app.accountinquiry.dto;

import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class AccountClassProductResponseList extends BaseResponseObject

{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<AccountClassProductInfo> accountClasses;

	public List<AccountClassProductInfo> getAccountClasses() {
		return accountClasses;
	}

	public void setAccountClasses(List<AccountClassProductInfo> accountClasses) {
		this.accountClasses = accountClasses;
	}

	

}
