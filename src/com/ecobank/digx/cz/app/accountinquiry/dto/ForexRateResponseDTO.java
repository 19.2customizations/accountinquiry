package com.ecobank.digx.cz.app.accountinquiry.dto;



import com.ofss.digx.service.response.BaseResponseObject;

public final class ForexRateResponseDTO extends BaseResponseObject {
	
	
	private static final long serialVersionUID = 1L;
	private String baseCcy;
	private String quoteCcy;
	private String branchCode;
	private double midRate;
	private double buyRate;
	private double sellRate;
	
	
	public String getBaseCcy() {
		return baseCcy;
	}
	public void setBaseCcy(String baseCcy) {
		this.baseCcy = baseCcy;
	}
	public String getQuoteCcy() {
		return quoteCcy;
	}
	public void setQuoteCcy(String quoteCcy) {
		this.quoteCcy = quoteCcy;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public double getMidRate() {
		return midRate;
	}
	public void setMidRate(double midRate) {
		this.midRate = midRate;
	}
	public double getBuyRate() {
		return buyRate;
	}
	public void setBuyRate(double buyRate) {
		this.buyRate = buyRate;
	}
	public double getSellRate() {
		return sellRate;
	}
	public void setSellRate(double sellRate) {
		this.sellRate = sellRate;
	}
	
	

}
