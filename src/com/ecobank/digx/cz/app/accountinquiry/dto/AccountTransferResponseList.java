package com.ecobank.digx.cz.app.accountinquiry.dto;

import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class AccountTransferResponseList extends BaseResponseObject

{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<AccountTransferInfo> transfers;

	public List<AccountTransferInfo> getTransfers() {
		return transfers;
	}

	public void setTransfers(List<AccountTransferInfo> transfers) {
		this.transfers = transfers;
	}

}
