package com.ecobank.digx.cz.app.accountinquiry.dto;

import com.ofss.digx.service.response.BaseResponseObject;

public class PhoneNumberResponseDTO extends BaseResponseObject {
	
	private static final long serialVersionUID = 1L;
	
	private String phoneNumber;
	private boolean numberValid;
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public boolean isNumberValid() {
		return numberValid;
	}
	public void setNumberValid(boolean numberValid) {
		this.numberValid = numberValid;
	}
	
	
	

}
