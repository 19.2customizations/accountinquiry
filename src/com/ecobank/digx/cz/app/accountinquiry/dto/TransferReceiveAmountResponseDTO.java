package com.ecobank.digx.cz.app.accountinquiry.dto;



import com.ofss.digx.service.response.BaseResponseObject;

public final class TransferReceiveAmountResponseDTO extends BaseResponseObject {
	
	
	private static final long serialVersionUID = 1L;
	private String baseCcy;
	private String quoteCcy;
	private String branchCode;
	private double midRate;
	private double buyRate;
	private double sellRate;
	
	private double receiveAmount;
	private double sendAmount;
	private double exchRate;
	
	private String cuurencyPair;
	
	private String responseCode;
	private String responseMessage;
	

	public String getBaseCcy() {
		return baseCcy;
	}
	public void setBaseCcy(String baseCcy) {
		this.baseCcy = baseCcy;
	}
	public String getQuoteCcy() {
		return quoteCcy;
	}
	public void setQuoteCcy(String quoteCcy) {
		this.quoteCcy = quoteCcy;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public double getMidRate() {
		return midRate;
	}
	public void setMidRate(double midRate) {
		this.midRate = midRate;
	}
	public double getBuyRate() {
		return buyRate;
	}
	public void setBuyRate(double buyRate) {
		this.buyRate = buyRate;
	}
	public double getSellRate() {
		return sellRate;
	}
	public void setSellRate(double sellRate) {
		this.sellRate = sellRate;
	}
	public double getReceiveAmount() {
		return receiveAmount;
	}
	public void setReceiveAmount(double receiveAmount) {
		this.receiveAmount = receiveAmount;
	}
	public double getSendAmount() {
		return sendAmount;
	}
	public void setSendAmount(double sendAmount) {
		this.sendAmount = sendAmount;
	}
	public double getExchRate() {
		return exchRate;
	}
	public void setExchRate(double exchRate) {
		this.exchRate = exchRate;
	}
	public String getCuurencyPair() {
		return cuurencyPair;
	}
	public void setCuurencyPair(String cuurencyPair) {
		this.cuurencyPair = cuurencyPair;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	
}
