package com.ecobank.digx.cz.app.accountinquiry.dto;



import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public final class RMDetailListResponse extends BaseResponseObject {
	
	
	private static final long serialVersionUID = 1L;
	private List<RMDetailInfoResponseDTO> listRMs;
	public List<RMDetailInfoResponseDTO> getListRMs() {
		return listRMs;
	}
	public void setListRMs(List<RMDetailInfoResponseDTO> listRMs) {
		this.listRMs = listRMs;
	}
	
	

}
