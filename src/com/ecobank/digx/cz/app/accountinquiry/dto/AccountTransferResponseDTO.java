package com.ecobank.digx.cz.app.accountinquiry.dto;

import java.math.BigDecimal;

import com.ofss.digx.datatype.CurrencyAmount;
import com.ofss.digx.service.response.BaseResponseObject;

public class AccountTransferResponseDTO extends BaseResponseObject

{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cbaReferenceNo;
	private String responseCode;
	private String responseMessage;
	
	private String externalRefNo;
	
	private CurrencyAmount receiveAmount;
	private CurrencyAmount sendAmount;
	
	private BigDecimal exchRate;
	
	private String currencyPair;
	
	

	public String getCbaReferenceNo() {
		return cbaReferenceNo;
	}

	public void setCbaReferenceNo(String cbaReferenceNo) {
		this.cbaReferenceNo = cbaReferenceNo;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getExternalRefNo() {
		return externalRefNo;
	}

	public void setExternalRefNo(String externalRefNo) {
		this.externalRefNo = externalRefNo;
	}

	public CurrencyAmount getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(CurrencyAmount receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public CurrencyAmount getSendAmount() {
		return sendAmount;
	}

	public void setSendAmount(CurrencyAmount sendAmount) {
		this.sendAmount = sendAmount;
	}

	public BigDecimal getExchRate() {
		return exchRate;
	}

	public void setExchRate(BigDecimal exchRate) {
		this.exchRate = exchRate;
	}

	public String getCurrencyPair() {
		return currencyPair;
	}

	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}
	
	
	

}
