package com.ecobank.digx.cz.app.accountinquiry.dto;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.datatype.complex.Account;

import io.swagger.v3.oas.annotations.media.Schema;

public class FetchAccountDetailRequestDTO extends DataTransferObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
    private String affiliateCode;
	
	@Schema(description = "Account No", type = "String", required = true)
	private String accountNo;
	@Schema(description = "Account Id", type = "Account", required = true)
	private Account accountId;
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public Account getAccountId() {
		return accountId;
	}
	public void setAccountId(Account accountId) {
		this.accountId = accountId;
	}
	
	
	
	
	

}
