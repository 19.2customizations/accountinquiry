package com.ecobank.digx.cz.app.accountinquiry.dto;



import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public final class AccountInquiryListResponse extends BaseResponseObject {
	
	
	private static final long serialVersionUID = 1L;
	private List<AccountInquiryResponseDTO> accounts;
	
	public List<AccountInquiryResponseDTO> getAccounts() {
		return accounts;
	}
	public void setAccounts(List<AccountInquiryResponseDTO> accounts) {
		this.accounts = accounts;
	}
	
	

}
