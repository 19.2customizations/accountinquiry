package com.ecobank.digx.cz.app.accountinquiry.dto;


import com.ofss.digx.app.common.dto.DataTransferObject;

public final class RMDetailInfoRequestDTO extends DataTransferObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String accountNo;
	private String customerNo;
	private String affiliateCode;
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	
	
	
	
	

}
