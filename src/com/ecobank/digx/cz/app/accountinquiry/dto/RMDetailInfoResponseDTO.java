package com.ecobank.digx.cz.app.accountinquiry.dto;



import com.ofss.digx.service.response.BaseResponseObject;

public final class RMDetailInfoResponseDTO extends BaseResponseObject {
	
	
	private static final long serialVersionUID = 1L;
	private String accountNo;
	private String accountName;
	private String code;
	private String name;
	private String email;
	private String phoneNo;
	
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	

}
