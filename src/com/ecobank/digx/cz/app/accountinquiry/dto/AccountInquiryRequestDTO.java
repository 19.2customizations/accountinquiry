package com.ecobank.digx.cz.app.accountinquiry.dto;

import com.ofss.digx.app.common.dto.DataTransferObject;
//import com.ofss.digx.datatype.complex.Account;

public class AccountInquiryRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	
	private String accountNo;
	private String affiliateCode;
	private String bankCode;
	private boolean isExpressAccount;
	
	
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public boolean isExpressAccount() {
		return isExpressAccount;
	}
	public void setExpressAccount(boolean isExpressAccount) {
		this.isExpressAccount = isExpressAccount;
	}

	
	
	

}
