package com.ecobank.digx.cz.app.accountinquiry.dto;


import com.ofss.digx.service.response.BaseResponseObject;

public class AlertMessageResponseDTO extends BaseResponseObject
{

	private static final long serialVersionUID = 1L;
	

	private String responseCode;
	private String responseMessage;
	private String messageId;
	
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	

	
	

}
