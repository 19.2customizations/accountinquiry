package com.ecobank.digx.cz.app.accountinquiry.dto;


public class AccountClassProductInfo {
	
	private String affiliateCode;
	private String accountClass;
	private String accountClassName;
	private String features;
	private String pndAllowed;
	private String chequebookRequired;
	private String chequeLeaves;
	private String accStatementFrequency;
	private String ccyCode;
	private long id;
	
	
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getAccountClass() {
		return accountClass;
	}
	public void setAccountClass(String accountClass) {
		this.accountClass = accountClass;
	}
	public String getAccountClassName() {
		return accountClassName;
	}
	public void setAccountClassName(String accountClassName) {
		this.accountClassName = accountClassName;
	}
	public String getFeatures() {
		return features;
	}
	public void setFeatures(String features) {
		this.features = features;
	}
	public String getPndAllowed() {
		return pndAllowed;
	}
	public void setPndAllowed(String pndAllowed) {
		this.pndAllowed = pndAllowed;
	}
	public String getChequebookRequired() {
		return chequebookRequired;
	}
	public void setChequebookRequired(String chequebookRequired) {
		this.chequebookRequired = chequebookRequired;
	}
	public String getChequeLeaves() {
		return chequeLeaves;
	}
	public void setChequeLeaves(String chequeLeaves) {
		this.chequeLeaves = chequeLeaves;
	}
	public String getAccStatementFrequency() {
		return accStatementFrequency;
	}
	public void setAccStatementFrequency(String accStatementFrequency) {
		this.accStatementFrequency = accStatementFrequency;
	}
	public String getCcyCode() {
		return ccyCode;
	}
	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
	
	

}
