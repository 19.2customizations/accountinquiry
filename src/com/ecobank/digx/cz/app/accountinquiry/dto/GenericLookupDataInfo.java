package com.ecobank.digx.cz.app.accountinquiry.dto;

public class GenericLookupDataInfo {
	
	private String dataCode;
	private String dataName;
	private String dataDesc;
	
	public String getDataCode() {
		return dataCode;
	}
	public void setDataCode(String dataCode) {
		this.dataCode = dataCode;
	}
	public String getDataName() {
		return dataName;
	}
	public void setDataName(String dataName) {
		this.dataName = dataName;
	}
	public String getDataDesc() {
		return dataDesc;
	}
	public void setDataDesc(String dataDesc) {
		this.dataDesc = dataDesc;
	}
	
	
	

}
