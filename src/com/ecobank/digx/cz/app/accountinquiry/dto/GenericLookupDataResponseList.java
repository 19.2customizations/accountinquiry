package com.ecobank.digx.cz.app.accountinquiry.dto;

import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class GenericLookupDataResponseList extends BaseResponseObject

{
	
	private static final long serialVersionUID = 1L;

	private List<GenericLookupDataInfo> list;

	public List<GenericLookupDataInfo> getList() {
		return list;
	}

	public void setList(List<GenericLookupDataInfo> list) {
		this.list = list;
	}
	
	

	

}
