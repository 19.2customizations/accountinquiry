package com.ecobank.digx.cz.app.accountinquiry.dto;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.app.dto.validation.Mandatory;

import io.swagger.v3.oas.annotations.media.Schema;

public class AccountTransferRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	
	
	
	
	  
	  private String senderAccountNo;
	  private String senderName;
	  private String senderAccountCcy;
	  private String affiliateCode;
	  private String beneficiaryAccountNo;
	  private String beneficiaryName;
	  private String beneficiaryAccountCcy;
	  
	  @Schema(description = "Source Account", type = "Account", required = true)
	  private Account sourceAccountId;
	  @Schema(description = "Beneficiary Account", type = "Account", required = true)
	  private Account beneficiaryAccountId;
	  
	  @Mandatory(errorCode = "DIGX_CZ_0003")
	  //@Schema(description = "  Amount must be greater than zero. ", type = "Double", required = true, minLength = 1, maxLength = 80)
	  @Schema(description = "Amount must be greater than zero. ", type = "Double", required = true)
	  private double amount;
	  private double receiveAmount;
	  private double exchRate;
	  private String status;
	  
	  private String responseCode;
	  private String responseMessage;
	  private String CBAReferenceNo;
	  
	  private String tranType;
	  private String tranCode;
	  private String userId;
	  private String externalRefNo;
	  
	  private String purpose;
	  private String transferWhen;
	  private String transferDate;
	  private String senderCustomerId;
	  private String beneficiaryCustomerId;
	  private String beneficiaryEmail;
	  
	  
	  
	  
	  
	public String getSenderAccountNo() {
		return senderAccountNo;
	}
	public void setSenderAccountNo(String senderAccountNo) {
		this.senderAccountNo = senderAccountNo;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getSenderAccountCcy() {
		return senderAccountCcy;
	}
	public void setSenderAccountCcy(String senderAccountCcy) {
		this.senderAccountCcy = senderAccountCcy;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getBeneficiaryAccountNo() {
		return beneficiaryAccountNo;
	}
	public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
		this.beneficiaryAccountNo = beneficiaryAccountNo;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getBeneficiaryAccountCcy() {
		return beneficiaryAccountCcy;
	}
	public void setBeneficiaryAccountCcy(String beneficiaryAccountCcy) {
		this.beneficiaryAccountCcy = beneficiaryAccountCcy;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getReceiveAmount() {
		return receiveAmount;
	}
	public void setReceiveAmount(double receiveAmount) {
		this.receiveAmount = receiveAmount;
	}
	public double getExchRate() {
		return exchRate;
	}
	public void setExchRate(double exchRate) {
		this.exchRate = exchRate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getCBAReferenceNo() {
		return CBAReferenceNo;
	}
	public void setCBAReferenceNo(String cBAReferenceNo) {
		CBAReferenceNo = cBAReferenceNo;
	}
	
	public String getTranType() {
		return tranType;
	}
	public void setTranType(String tranType) {
		this.tranType = tranType;
	}
	public String getTranCode() {
		return tranCode;
	}
	public void setTranCode(String tranCode) {
		this.tranCode = tranCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getExternalRefNo() {
		return externalRefNo;
	}
	public void setExternalRefNo(String externalRefNo) {
		this.externalRefNo = externalRefNo;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getTransferWhen() {
		return transferWhen;
	}
	public void setTransferWhen(String transferWhen) {
		this.transferWhen = transferWhen;
	}
	public String getTransferDate() {
		return transferDate;
	}
	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}
	public String getSenderCustomerId() {
		return senderCustomerId;
	}
	public void setSenderCustomerId(String senderCustomerId) {
		this.senderCustomerId = senderCustomerId;
	}
	public String getBeneficiaryCustomerId() {
		return beneficiaryCustomerId;
	}
	public void setBeneficiaryCustomerId(String beneficiaryCustomerId) {
		this.beneficiaryCustomerId = beneficiaryCustomerId;
	}
	public Account getSourceAccountId() {
		return sourceAccountId;
	}
	public void setSourceAccountId(Account sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}
	public Account getBeneficiaryAccountId() {
		return beneficiaryAccountId;
	}
	public void setBeneficiaryAccountId(Account beneficiaryAccountId) {
		this.beneficiaryAccountId = beneficiaryAccountId;
	}
	public String getBeneficiaryEmail() {
		return beneficiaryEmail;
	}
	public void setBeneficiaryEmail(String beneficiaryEmail) {
		this.beneficiaryEmail = beneficiaryEmail;
	}
	
	  

}
