package com.ecobank.digx.cz.app.accountinquiry.dto;

import com.ofss.digx.app.common.dto.DataTransferObject;
//import com.ofss.digx.datatype.complex.Account;

public class PhoneNumberValidateRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	
	private String countryCode;
	private String phone;
	
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	
	

}
