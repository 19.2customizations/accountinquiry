package com.ecobank.digx.cz.app.accountinquiry.dto;


import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.datatype.complex.Account;

import io.swagger.v3.oas.annotations.media.Schema;

public final class ForexRateRequestDTO extends DataTransferObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String baseCcy;
	private String quoteCcy;
	private String branchCode;
	private String affiliateCode;
	
	private double amount;
	
	private String tranCcy;
	
	
	@Schema(description = "Source Account", type = "String", required = true)
	private String sourceAccountNo;
	@Schema(description = "Source Account Id", type = "Account", required = true)
	private Account sourceAccountId;
	
	
	@Schema(description = "Beneficiary Account", type = "String", required = true)
	private String beneficiaryAccountNo;
	@Schema(description = "Beneficiary Account Id", type = "Account", required = true)
	private Account beneficiaryAccountId;
	
	
	public String getBaseCcy() {
		return baseCcy;
	}
	public void setBaseCcy(String baseCcy) {
		this.baseCcy = baseCcy;
	}
	public String getQuoteCcy() {
		return quoteCcy;
	}
	public void setQuoteCcy(String quoteCcy) {
		this.quoteCcy = quoteCcy;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getSourceAccountNo() {
		return sourceAccountNo;
	}
	public void setSourceAccountNo(String sourceAccountNo) {
		this.sourceAccountNo = sourceAccountNo;
	}
	public Account getSourceAccountId() {
		return sourceAccountId;
	}
	public void setSourceAccountId(Account sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}
	
	public Account getBeneficiaryAccountId() {
		return beneficiaryAccountId;
	}
	public void setBeneficiaryAccountId(Account beneficiaryAccountId) {
		this.beneficiaryAccountId = beneficiaryAccountId;
	}
	public String getTranCcy() {
		return tranCcy;
	}
	public void setTranCcy(String tranCcy) {
		this.tranCcy = tranCcy;
	}
	public String getBeneficiaryAccountNo() {
		return beneficiaryAccountNo;
	}
	public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
		this.beneficiaryAccountNo = beneficiaryAccountNo;
	}
	
	
	
	
	

}
