package com.ecobank.digx.cz.app.accountinquiry.assembler;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ecobank.digx.cz.app.accountinquiry.dto.AccountClassProductInfo;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountClassProductResponseList;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferInfo;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferResponseList;
import com.ecobank.digx.cz.app.accountinquiry.dto.GenericLookupDataInfo;
import com.ecobank.digx.cz.app.accountinquiry.dto.GenericLookupDataResponseList;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.AccountTransfer;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.AccountTransferKey;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.GenericLookupData;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.policy.AccountTransferBusinessPolicyData;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.framework.domain.IAbstractDomainObject;
import com.ofss.fc.framework.domain.assembler.AbstractAssembler;
import com.ofss.fc.framework.domain.common.dto.DomainObjectDTO;
import com.ofss.fc.infra.exception.FatalException;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class AccountTransferAssembler extends AbstractAssembler {
	private static final String THIS_COMPONENT_NAME = AccountTransferAssembler.class.getName();
	private transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);

	public DomainObjectDTO fromDomainObject(IAbstractDomainObject arg0) throws FatalException {
		return null;
	}

	public IAbstractDomainObject toDomainObject(DomainObjectDTO arg0) throws FatalException {
		return null;
	}

	public AccountTransfer toAccountTransferDomainObjectCreate(AccountTransferRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {
		AccountTransfer accountTransferDomain = null;
		if (requestDTO != null) {
			populateDataTransferObjectDTOMap("com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferRequestDTO",
					requestDTO);
			try {
				if (retrieveDataTransferObjectDTOMapElement(
						"com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferRequestDTO")
								.getDictionaryArray() == null) {
					accountTransferDomain = new AccountTransfer();
				} else {
					accountTransferDomain = (AccountTransfer) getCustomizedDomainObject(
							retrieveDataTransferObjectDTOMapElement(
									"com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferRequestDTO"));
				}
			} catch (Exception e) {
				this.logger.log(Level.WARNING,
						"Customized Domain Object failed to be instantiated from com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferRequestDTO",
						e);

				accountTransferDomain = new AccountTransfer();
			}
			AccountTransferKey key = new AccountTransferKey();

			accountTransferDomain.setAffiliateCode(requestDTO.getAffiliateCode());
			accountTransferDomain.setAmount(requestDTO.getAmount());
			accountTransferDomain.setBeneficiaryAccountNo(requestDTO.getBeneficiaryAccountNo());
			accountTransferDomain.setBeneficiaryName(requestDTO.getBeneficiaryName());
			accountTransferDomain.setBeneficiaryAccountCcy(requestDTO.getBeneficiaryAccountCcy());
			accountTransferDomain.setSenderAccountCcy(requestDTO.getSenderAccountCcy());
			accountTransferDomain.setSenderAccountNo(requestDTO.getSenderAccountNo());
			accountTransferDomain.setSenderName(requestDTO.getSenderName());
			accountTransferDomain.setStatus("PENDING");
			accountTransferDomain.setTranCode(requestDTO.getTranCode());
			accountTransferDomain.setReceiveAmount(requestDTO.getReceiveAmount());
			accountTransferDomain.setExchRate(requestDTO.getExchRate());
			
			accountTransferDomain.setResponseCode("PP");
			
	
			accountTransferDomain.setUserId(requestDTO.getUserId());
			accountTransferDomain.setRequestDate(new Date());
			
			String email = requestDTO.getBeneficiaryEmail();
			if(email == null)
				email ="";
			
			accountTransferDomain.setNarration(requestDTO.getPurpose() + " " + email);
			accountTransferDomain.setTransferWhen(requestDTO.getTransferWhen());
			accountTransferDomain.setTranType(requestDTO.getTranType());
			
			key.setExternalRefNo(requestDTO.getExternalRefNo());

			accountTransferDomain.setRefKey(key);

		}
		return accountTransferDomain;
	}

	public AccountTransferRequestDTO fromDomainObjectToAccountTransferRequestTO(AccountTransfer tInfo)
			throws com.ofss.digx.infra.exceptions.Exception {
		AccountTransferRequestDTO aInfo = new AccountTransferRequestDTO();

				aInfo.setAffiliateCode(tInfo.getAffiliateCode());
				aInfo.setAmount(tInfo.getAmount());
				aInfo.setBeneficiaryAccountCcy(tInfo.getBeneficiaryAccountCcy());
				aInfo.setBeneficiaryAccountNo(tInfo.getBeneficiaryAccountNo());
				aInfo.setBeneficiaryName(tInfo.getBeneficiaryName());
				aInfo.setCBAReferenceNo(tInfo.getCBAReferenceNo());
				aInfo.setExchRate(tInfo.getExchRate());
				aInfo.setExternalRefNo(tInfo.getRefKey().getExternalRefNo());
				aInfo.setReceiveAmount(tInfo.getReceiveAmount());
				aInfo.setResponseCode(tInfo.getResponseCode());
				aInfo.setResponseMessage(tInfo.getResponseMessage());
				aInfo.setStatus(tInfo.getStatus());
				aInfo.setSenderAccountCcy(tInfo.getSenderAccountCcy());
				aInfo.setSenderAccountNo(tInfo.getSenderAccountNo());
				aInfo.setSenderName(tInfo.getSenderName());
				aInfo.setTranCode(tInfo.getTranCode());
				aInfo.setTranType(tInfo.getTranType());
				aInfo.setUserId(tInfo.getUserId());
				aInfo.setPurpose(tInfo.getNarration());
				aInfo.setTransferWhen(tInfo.getTransferWhen());
				//aInfo.setTransferDate(tInfo.getTransferDate());
				
			

		return aInfo;
	}
	
	
	public AccountTransferResponseList listFromDomainObjectToAccountTransferDTO(List<AccountTransfer> list)
			throws com.ofss.digx.infra.exceptions.Exception {
		AccountTransferInfo aInfo = null;

		AccountTransferResponseList response = new AccountTransferResponseList();

		List<AccountTransferInfo> listResponse = new ArrayList<AccountTransferInfo>();

		if (list != null && list.size() > 0) {
			for (AccountTransfer tInfo : list) {
				aInfo = new AccountTransferInfo();
				aInfo.setAffiliateCode(tInfo.getAffiliateCode());
				aInfo.setAmount(tInfo.getAmount());
				aInfo.setBeneficiaryAccountCcy(tInfo.getBeneficiaryAccountCcy());
				aInfo.setBeneficiaryAccountNo(tInfo.getBeneficiaryAccountNo());
				aInfo.setBeneficiaryName(tInfo.getBeneficiaryName());
				aInfo.setCBAReferenceNo(tInfo.getCBAReferenceNo());
				aInfo.setExchRate(tInfo.getExchRate());
				aInfo.setExternalRefNo(tInfo.getRefKey().getExternalRefNo());
				aInfo.setReceiveAmount(tInfo.getReceiveAmount());
				aInfo.setResponseCode(tInfo.getResponseCode());
				aInfo.setResponseMessage(tInfo.getResponseMessage());
				aInfo.setStatus(tInfo.getStatus());
				aInfo.setSenderAccountCcy(tInfo.getSenderAccountCcy());
				aInfo.setSenderAccountNo(tInfo.getSenderAccountNo());
				aInfo.setSenderName(tInfo.getSenderName());
				aInfo.setTranCode(tInfo.getTranCode());
				aInfo.setTranType(tInfo.getTranType());
				aInfo.setUserId(tInfo.getUserId());
				aInfo.setNarration(tInfo.getNarration());
				aInfo.setTransferWhen(tInfo.getTransferWhen());
				
				listResponse.add(aInfo);

			}

			response.setTransfers(listResponse);
		}

		return response;
	}

	public AccountTransferResponseList fromDomainObjectToAccountTransferDTO(AccountTransfer aInfo)
			throws com.ofss.digx.infra.exceptions.Exception {

		List<AccountTransfer> list = new ArrayList<AccountTransfer>();
		list.add(aInfo);

		AccountTransferResponseList response = listFromDomainObjectToAccountTransferDTO(list);

		return response;
	}
	
	public GenericLookupDataResponseList fromDomainObjectToGenericLookupList(List<GenericLookupData> list)
			throws com.ofss.digx.infra.exceptions.Exception {

		List<GenericLookupDataInfo> listG = new ArrayList<GenericLookupDataInfo>();
		
		if (list != null && list.size() > 0) {
			for (GenericLookupData tInfo : list) {
				
				GenericLookupDataInfo aInfo = new GenericLookupDataInfo();
				aInfo.setDataCode(tInfo.getDataCode());
				aInfo.setDataName(tInfo.getDataName());
				aInfo.setDataDesc(tInfo.getDataDesc());
				listG.add(aInfo);
			}
		}
		
		
		GenericLookupDataResponseList response = new GenericLookupDataResponseList();
		response.setList(listG);

		return response;
	}
	
	public AccountTransferBusinessPolicyData fromDTOObjectToBusinessPolicyDTO(AccountTransferRequestDTO aInfo)
			throws com.ofss.digx.infra.exceptions.Exception {

		AccountTransferBusinessPolicyData policyData = new AccountTransferBusinessPolicyData();
		policyData.setAffiliateCode(aInfo.getAffiliateCode());
		policyData.setSenderAccountCcy(aInfo.getSenderAccountCcy());
		policyData.setSenderCustomerId(aInfo.getSenderCustomerId());
		policyData.setBeneficiaryAccountCcy(aInfo.getBeneficiaryAccountCcy());
		policyData.setBeneficiaryCustomerId(aInfo.getBeneficiaryCustomerId());
		policyData.setTranType(aInfo.getTranType());
		//policyData.setSenderAccountCcy(senderAccountCcy);

		

		return policyData;
	}
	
	
	
	

}