package com.ecobank.digx.cz.app.accountinquiry.service;

import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferResponseList;
import com.ecobank.digx.cz.app.accountinquiry.dto.AlertMessageRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AlertMessageResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.GenericLookupDataResponseList;
import com.ecobank.digx.cz.app.accountinquiry.dto.PhoneNumberResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.PhoneNumberValidateRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.TransferReceiveAmountResponseDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailRequestDTO;
import com.ofss.fc.app.context.SessionContext;

public interface IAccountInquiry {
	
	 public abstract AccountInquiryResponseDTO read(SessionContext sessionContext, AccountInquiryRequestDTO requestDTO)
	    throws Exception;

	 public AccountInquiryResponseDTO fetchAccountDetail(SessionContext sessionContext,
			 AccountInquiryRequestDTO requestDTO) throws Exception;
	 
	 public ForexRateResponseDTO fetchForexRate(SessionContext sessionContext, ForexRateRequestDTO requestDTO)
				throws Exception;
	 
	 public TransferReceiveAmountResponseDTO computeReceiveAmount(SessionContext sessionContext, ForexRateRequestDTO requestDTO)
				throws Exception;
	 
	// public AccountTransferResponseDTO update(SessionContext sessionContext, String paymentId)
	//			throws com.ofss.digx.infra.exceptions.Exception ;
	 
	 public AccountTransferResponseList listTransfers(SessionContext sessionContext)
				throws com.ofss.digx.infra.exceptions.Exception ;
	 
	 public AlertMessageResponseDTO sendMessage(SessionContext sessionContext, AlertMessageRequestDTO requestDTO)
				throws com.ofss.digx.infra.exceptions.Exception;
	 
	 public GenericLookupDataResponseList listGenericLookupData(SessionContext sessionContext, String affCode, String lookupCode)
				throws com.ofss.digx.infra.exceptions.Exception ;
	 
	 public AccountDetailDTO fetchAccountDetailFullInfo(SessionContext sessionContext, AccountDetailRequestDTO requestDTO)
				throws com.ofss.digx.infra.exceptions.Exception ;
	 
	 public TransferReceiveAmountResponseDTO computeTransferForexRate(SessionContext sessionContext, ForexRateRequestDTO requestDTO)
				throws com.ofss.digx.infra.exceptions.Exception;
	 
	 public PhoneNumberResponseDTO validatePhoneNo(SessionContext sessionContext, PhoneNumberValidateRequestDTO requestDTO)
				throws com.ofss.digx.infra.exceptions.Exception ;

}
