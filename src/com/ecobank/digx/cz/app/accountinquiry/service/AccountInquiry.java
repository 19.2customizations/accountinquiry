package com.ecobank.digx.cz.app.accountinquiry.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.accountinquiry.assembler.AccountTransferAssembler;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferResponseList;
import com.ecobank.digx.cz.app.accountinquiry.dto.AlertMessageRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AlertMessageResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.GenericLookupDataResponseList;
import com.ecobank.digx.cz.app.accountinquiry.dto.PhoneNumberResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.PhoneNumberValidateRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.RMDetailInfoRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.RMDetailInfoResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.TransferReceiveAmountResponseDTO;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.AccountTransfer;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.GenericLookupData;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.policy.AccountTransferBusinessPolicyData;
import com.ecobank.digx.cz.extsystem.dto.BatchPostingRequestDTO;
import com.ecobank.digx.cz.extsystem.dto.BatchPostingResponseDTO;
import com.ecobank.digx.cz.extsystem.dto.TransactionEntry;
import com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.FlexIFPostingAdapter;
import com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.IAccountInquiryAdapter;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailRequestDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountMISAndSignatoryDetailDTO;
import com.ecobank.digx.cz.extxface.mule.adapter.client.RemoteSMSAlertRepositoryAdapter;
import com.ecobank.digx.cz.extxface.mule.dto.ESBResponseDTO;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.ofss.digx.app.AbstractApplication;
import com.ofss.digx.app.Interaction;
import com.ofss.digx.app.adapter.AdapterFactoryConfigurator;
import com.ofss.digx.app.adapter.IAdapterFactory;
import com.ofss.digx.app.exception.RunTimeException;
import com.ofss.digx.datatype.CurrencyAmount;
import com.ofss.digx.domain.account.entity.core.AccountKey;
import com.ofss.digx.domain.config.entity.ConfigVarBDomain;
import com.ofss.digx.domain.dda.entity.DemandDepositAccount;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.digx.framework.domain.business.policy.factory.BusinessPolicyFactory;
import com.ofss.fc.app.context.SessionContext;
import com.ofss.fc.framework.domain.policy.AbstractBusinessPolicy;
import com.ofss.fc.framework.domain.policy.IBusinessPolicyDTO;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.service.response.TransactionStatus;
import com.thoughtworks.xstream.XStream;


public class AccountInquiry extends AbstractApplication implements IAccountInquiry {

	private static final String THIS_COMPONENT_NAME = AccountInquiry.class.getName();
	private MultiEntityLogger FORMATTER = MultiEntityLogger.getUniqueInstance();
	private transient Logger logger = this.FORMATTER.getLogger(THIS_COMPONENT_NAME);
	// private IPassportDetailExtExecutor extensionExecutor = null;
	
	//protected static final Preferences preferences = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");
	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	public AccountInquiry() {
		// this.extensionExecutor =
		// ((IPassportDetailExtExecutor)ServiceExtensionFactory.getServiceExtensionExecutor(THIS_COMPONENT_NAME));
	}

	public AccountInquiryResponseDTO read(SessionContext sessionContext, AccountInquiryRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("Account-Inquiry-Service-Welcome: " + requestDTO.getAccountNo());

		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Entered into create method of Self Transfer service  Input: SelfTransferReadRequestDTO: %s in class '%s'",
					new Object[] { requestDTO, THIS_COMPONENT_NAME }));
		}
		// super.checkAccessPolicy("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
		// new Object[] { sessionContext, passRequestDTO });

		super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		AccountInquiryResponseDTO responseDTO = new AccountInquiryResponseDTO();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		// PassportDetailBusinessPolicyData passBusinessPolicyData = null;
		BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();

		try {

			// com.ofss.digx.app.sms.adapter.impl.user.UserPartyAdapterFactory
			// REST ->Service Class-> Domain/Adapter -> Backend DB/FCUBS/ESB/Unified API
			requestDTO.validate(sessionContext);

			// this.extensionExecutor.preCreate(sessionContext, passRequestDTO);
			// abstractBusinessPolicy =
			// businessPolicyFactory.getBusinesPolicyInstance("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
			// passBusinessPolicyData);

			System.out.println("Before sending to domain--->" + requestDTO.getAccountNo());

			IAdapterFactory adapterFactory = AdapterFactoryConfigurator.getInstance()
					.getAdapterFactory("CZ_ACCOUNT_INQUIRY_ADAPTER_FACTORY");
			IAccountInquiryAdapter adapter = (IAccountInquiryAdapter) adapterFactory
					.getAdapter("CZ_ACCOUNT_INQUIRY_ADAPTER");
			responseDTO = adapter.fetchAccountName(sessionContext, requestDTO); // fetchParty(sessionContext,
																				// partyDetailsRequest)

			// com.ecobank.digx.cz.domain.IdentityManagement.entity.PassportDetail
			// passportDomain = new
			// PassportDetailAssembler().toPassportDomainObjectCreate(passRequestDTO);
			// passportDomain.create(passportDomain);
			// selfTransferCreateResponse.setPaymentId(selfTransferDomain.getKey().getId());
			responseDTO.setStatus(buildStatus(status));
			// this.extensionExecutor.postCreate(sessionContext, passRequestDTO,
			// passResponse);

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("Fatal Exception from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("RunTimeException from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	
	
	public ForexRateResponseDTO fetchForexRate(SessionContext sessionContext, ForexRateRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("Forex Rate-Service-Welcome: " + requestDTO.getBaseCcy());

		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Entered into create method of Self Transfer service  Input: SelfTransferReadRequestDTO: %s in class '%s'",
					new Object[] { requestDTO, THIS_COMPONENT_NAME }));
		}
		// super.checkAccessPolicy("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
		// new Object[] { sessionContext, passRequestDTO });

		super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		ForexRateResponseDTO responseDTO = new ForexRateResponseDTO();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		// PassportDetailBusinessPolicyData passBusinessPolicyData = null;
		BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();

		try {

			
			requestDTO.validate(sessionContext);

			// this.extensionExecutor.preCreate(sessionContext, passRequestDTO);
			// abstractBusinessPolicy =
			// businessPolicyFactory.getBusinesPolicyInstance("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
			// passBusinessPolicyData);

			System.out.println("Before sending to domain--->" + requestDTO.getBaseCcy() + " " + requestDTO.getQuoteCcy());

			IAdapterFactory adapterFactory = AdapterFactoryConfigurator.getInstance().getAdapterFactory("CZ_ACCOUNT_INQUIRY_ADAPTER_FACTORY");
			IAccountInquiryAdapter adapter = (IAccountInquiryAdapter) adapterFactory.getAdapter("CZ_ACCOUNT_INQUIRY_ADAPTER");
			
			if(requestDTO.getSourceAccountId() != null &&  requestDTO.getBeneficiaryAccountId() != null )
			{
				
				
					String sAcc = requestDTO.getSourceAccountId().getValue();
					System.out.println("LCY-Source Account: " + sAcc);
					int x = 0;
					String sourceAccount = sAcc;
					if(sAcc.contains("~"))
					{
						x = sAcc.indexOf("~");
						sourceAccount = sAcc.substring(x + 1);
					}
					
					System.out.println("LCY-Source Account: " + sourceAccount);
					
					String bAcc = requestDTO.getBeneficiaryAccountId().getValue();
					
					String benAccount = bAcc;
					if(bAcc.equals("~"))
					{
						x = bAcc.indexOf("~");
						benAccount = bAcc.substring(x + 1);
					}
					
					System.out.println("LCY-Beneficiary Account: " + benAccount);
					
					String affCode = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
					AccountInquiryRequestDTO requestDTO1 = new AccountInquiryRequestDTO();
					requestDTO1.setAccountNo(sourceAccount);
					requestDTO1.setAffiliateCode(affCode);
					AccountInquiryResponseDTO responseDTO2 = fetchAccountDetail(sessionContext, requestDTO1);
					
					String ccy1 = "";
					String ccy2 = "";
					
					AccountInquiryRequestDTO requestDTO2 = new AccountInquiryRequestDTO();
					requestDTO2.setAccountNo(benAccount);
					requestDTO2.setAffiliateCode(affCode);
					AccountInquiryResponseDTO responseDTO3 = fetchAccountDetail(sessionContext, requestDTO2);
					
					if(requestDTO.getTranCcy().equals(responseDTO2.getCcy()))
					{
						ccy1 = responseDTO2.getCcy();
						ccy2 = responseDTO3.getCcy();
					}
					else if(requestDTO.getTranCcy().equals(responseDTO3.getCcy()))
					{
						ccy1 = responseDTO3.getCcy();
						ccy2 = responseDTO2.getCcy();
					}
					
					System.out.println("LCY- Account: " + ccy1 + " " + ccy2 + " " + affCode);
					requestDTO.setBaseCcy(ccy1);
					requestDTO.setQuoteCcy(ccy2);
					requestDTO.setAffiliateCode(affCode);
					
					
				
			}
			
			
			responseDTO = adapter.fetchForexRate(sessionContext, requestDTO); 

			// com.ecobank.digx.cz.domain.IdentityManagement.entity.PassportDetail
			// passportDomain = new
			// PassportDetailAssembler().toPassportDomainObjectCreate(passRequestDTO);
			// passportDomain.create(passportDomain);
			// selfTransferCreateResponse.setPaymentId(selfTransferDomain.getKey().getId());
			responseDTO.setStatus(buildStatus(status));
			// this.extensionExecutor.postCreate(sessionContext, passRequestDTO,
			// passResponse);

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("Fatal Exception from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("RunTimeException from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	public TransferReceiveAmountResponseDTO computeTransferForexRate(SessionContext sessionContext, ForexRateRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("Forex Rate-Service-Welcome: " + requestDTO.getBaseCcy());

		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Entered into create method of Self Transfer service  Input: SelfTransferReadRequestDTO: %s in class '%s'",
					new Object[] { requestDTO, THIS_COMPONENT_NAME }));
		}
		// super.checkAccessPolicy("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
		// new Object[] { sessionContext, passRequestDTO });

		super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		TransferReceiveAmountResponseDTO responseDTO = new TransferReceiveAmountResponseDTO();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		// PassportDetailBusinessPolicyData passBusinessPolicyData = null;
		BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();

		try {

			
			requestDTO.validate(sessionContext);

			// this.extensionExecutor.preCreate(sessionContext, passRequestDTO);
			// abstractBusinessPolicy =
			// businessPolicyFactory.getBusinesPolicyInstance("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
			// passBusinessPolicyData);

			System.out.println("Before sending to domain--->" + requestDTO.getBaseCcy() + " " + requestDTO.getQuoteCcy());

			IAdapterFactory adapterFactory = AdapterFactoryConfigurator.getInstance().getAdapterFactory("CZ_ACCOUNT_INQUIRY_ADAPTER_FACTORY");
			IAccountInquiryAdapter adapter = (IAccountInquiryAdapter) adapterFactory.getAdapter("CZ_ACCOUNT_INQUIRY_ADAPTER");
			
			String branchCode ="";
			String affCode = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			
			if(requestDTO.getSourceAccountId() != null  )
			{
				
				
					String sAcc = requestDTO.getSourceAccountId().getValue();
					System.out.println("LCY-Source Account: " + sAcc);
					int x = 0;
					String sourceAccount = sAcc;
					if(sAcc.contains("~"))
					{
						x = sAcc.indexOf("~");
						sourceAccount = sAcc.substring(x + 1);
					}
					
					System.out.println("LCY-Source Account: " + sourceAccount);
					String benAccount = "";
					
					if((requestDTO.getBeneficiaryAccountId() != null && requestDTO.getBeneficiaryAccountId().getValue() != null) || (requestDTO.getBeneficiaryAccountNo() != null && !requestDTO.getBeneficiaryAccountNo().equals("")) )
					{
						String bAcc = requestDTO.getBeneficiaryAccountId().getValue();
						
						 benAccount = bAcc;
						if(bAcc != null && bAcc.contains("~"))
						{
							x = bAcc.indexOf("~");
							benAccount = bAcc.substring(x + 1);
						}
						else
							benAccount = requestDTO.getBeneficiaryAccountNo();
						
					
						System.out.println("LCY-Beneficiary Account: " + benAccount);
					}
			
					
					
					AccountInquiryRequestDTO requestDTO1 = new AccountInquiryRequestDTO();
					requestDTO1.setAccountNo(sourceAccount);
					requestDTO1.setAffiliateCode(affCode);
					//AccountInquiryResponseDTO responseDTO2 = fetchAccountDetail(sessionContext, requestDTO1);
					
					
					requestDTO1.setExpressAccount(this.isExpressAccount(affCode, sourceAccount));
					if(requestDTO1.isExpressAccount())
					  requestDTO1.setAccountNo(this.formatExpressMobileNo(affCode, requestDTO1.getAccountNo()));
					
					System.out.println("Before sending to domain--->" + requestDTO1.getAccountNo() + " " + requestDTO1.isExpressAccount());
					
					    AccountInquiryResponseDTO responseDTO2 = adapter.fetchAccountName(sessionContext, requestDTO1);
						String ccy1 = responseDTO2.getCcy();
						branchCode = responseDTO2.getBranchId();
						
					//	String ccy1 = "";
						String ccy2 = "";
						if(benAccount != null &&  !benAccount.equals(""))  // (requestDTO.getBeneficiaryAccountId() != null && requestDTO.getBeneficiaryAccountId().getValue() != null) || (requestDTO.getBeneficiaryAccountNo() != null && !requestDTO.getBeneficiaryAccountNo().equals("")) )
						{
							AccountInquiryRequestDTO requestDTO2 = new AccountInquiryRequestDTO();
							requestDTO2.setAccountNo(benAccount);
							requestDTO2.setAffiliateCode(affCode);
							//AccountInquiryResponseDTO responseDTO3 = fetchAccountDetail(sessionContext, requestDTO2);
							
							
							requestDTO2.setExpressAccount(this.isExpressAccount(affCode, benAccount));
							if(requestDTO2.isExpressAccount())
							  requestDTO2.setAccountNo(this.formatExpressMobileNo(affCode, requestDTO2.getAccountNo()));
							
							System.out.println("Before sending to domain Ben--->" + requestDTO2.getAccountNo() + " " + requestDTO2.isExpressAccount());
							
							AccountInquiryResponseDTO responseDTO3 = adapter.fetchAccountName(sessionContext, requestDTO2);
							
							ccy2 = responseDTO3.getCcy();
					   }
					   else
						 ccy2 = requestDTO.getQuoteCcy();
					
					/*if(requestDTO.getTranCcy().equals(responseDTO2.getCcy()))
					{
						ccy1 = responseDTO2.getCcy();
						ccy2 = responseDTO3.getCcy();
					}
					else if(requestDTO.getTranCcy().equals(responseDTO3.getCcy()))
					{
						ccy1 = responseDTO3.getCcy();
						ccy2 = responseDTO2.getCcy();
					}*/
					
					System.out.println("LCY- Account-Update: " + ccy1 + " " + ccy2 + " " + affCode);
					requestDTO.setBaseCcy(ccy1);
					requestDTO.setQuoteCcy(ccy2);
					requestDTO.setBranchCode(branchCode);
					requestDTO.setAffiliateCode(affCode);
					
					
				
			}
			
			
			String ccy1 = requestDTO.getBaseCcy();
			String ccy2 = requestDTO.getQuoteCcy();
			double exchRate = 0;
			double amt = requestDTO.getAmount();
			//String affCode = requestDTO.getAffiliateCode();
		    affCode = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			responseDTO.setBaseCcy(ccy1);
			responseDTO.setQuoteCcy(ccy2);
			System.out.println("Aff Code --->" + affCode + " " + requestDTO.getBaseCcy());
			if(ccy1.equals(ccy2))
			{
				exchRate = 1;
				
				responseDTO.setBuyRate(exchRate);
				responseDTO.setMidRate(exchRate);
				responseDTO.setSellRate(exchRate);
				responseDTO.setReceiveAmount(amt);
				responseDTO.setSendAmount(amt);
				responseDTO.setExchRate(exchRate);
				
				String pair = "1 " + ccy1 +  " = 1 " + ccy2;
				responseDTO.setCuurencyPair(pair);
				responseDTO.setStatus(buildStatus(status));
	          
				return responseDTO;
			
			}
			
			double receiveAmt = 0, sendAmt = 0;
			
			System.out.println("Home-Branch-Aff-Code --->" + affCode + " " + requestDTO.getBranchCode());
			String homeBranch = digx_consulting.get("HOMEBRANCH", "");
			if(homeBranch != null && !homeBranch.equals(""))
				requestDTO.setBranchCode(homeBranch);
			
			System.out.println("Home-Branch-Aff-Code --->" + affCode + " " + homeBranch);
			
			
			ForexRateResponseDTO rateResponse = adapter.fetchForexRate(sessionContext, requestDTO); 
			String lcyCcy = adapter.fetchLocalCurrencyCode(sessionContext, affCode);
			
			System.out.println("LCY CCY --->" + lcyCcy + " " + ccy1 + " " + ccy2 + " " + rateResponse.getMidRate() );
			
			if(rateResponse != null && rateResponse.getMidRate() > 0 )
			{
				
				//if(ccy1.equals(lcyCcy))
					
				
				if(requestDTO.getTranCcy().equals(ccy2))
				{
					receiveAmt = amt;
					exchRate = rateResponse.getMidRate();
					
					if(ccy2.equals(ccy1))
					{
					   exchRate = rateResponse.getMidRate();
					   System.out.println("MID RATE --->" +  exchRate);
					}
					else if(ccy2.equals(lcyCcy))
					{
					    exchRate = rateResponse.getBuyRate();
						System.out.println("BUY RATE --->" +  exchRate);
					}
					else if(ccy1.equals(lcyCcy))
					{
						exchRate = rateResponse.getSellRate();
						System.out.println("SELL RATE --->" +  exchRate);
					}
					
					if(exchRate > 0)
					   sendAmt = amt / exchRate;
					
					System.out.println("Send Amt: " + sendAmt + " " + receiveAmt);
				}
				else
				{
					
					exchRate = rateResponse.getMidRate();
					
					if(ccy2.equals(ccy1))
					{
					   exchRate = rateResponse.getMidRate();
					   System.out.println("MID RATE-2 --->" +  exchRate);
					}
					else if(ccy2.equals(lcyCcy))
					{
					    exchRate = rateResponse.getBuyRate();
						System.out.println("BUY RATE-2 --->" +  exchRate);
					}
					else if(ccy1.equals(lcyCcy))
					{
						exchRate = rateResponse.getSellRate();
						System.out.println("SELL RATE-2 --->" +  exchRate);
					}
					
					receiveAmt = amt * exchRate;
					sendAmt = amt ;
					
					System.out.println("Send Amt-2: " + sendAmt + " " + receiveAmt);
				}
				
				
				/*if(ccy2.equals(lcyCcy)) //Use Buy Rate
				{
					exchRate = rateResponse.getBuyRate();
					receiveAmt = amt * exchRate;
					
					
					
				}
				else if(ccy1.equals(lcyCcy)) //Use Sell Rate
				{
					exchRate = rateResponse.getSellRate();
					receiveAmt = amt * exchRate;
				}*/
				
				responseDTO.setExchRate(exchRate);
				responseDTO.setBuyRate(rateResponse.getBuyRate());
				responseDTO.setMidRate(rateResponse.getMidRate());
				responseDTO.setSellRate(rateResponse.getSellRate());
				responseDTO.setReceiveAmount(receiveAmt);
				responseDTO.setSendAmount(sendAmt);
				String pair = "1 " + ccy1 +  " = " + exchRate + " " +  ccy2;
				
				if(exchRate < 1)
				{
					exchRate = new BigDecimal( 1/exchRate).setScale(4,RoundingMode.FLOOR).doubleValue();
					pair = "1 " + ccy2 +  " = " + exchRate + " " +  ccy1;
				}
				
				
				responseDTO.setCuurencyPair(pair);
				
				
			}
			
			
			responseDTO.setStatus(buildStatus(status));
			

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("Fatal Exception from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("RunTimeException from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		//super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	
	public PhoneNumberResponseDTO validatePhoneNo(SessionContext sessionContext, PhoneNumberValidateRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("Validate Phone No: " + requestDTO.getCountryCode() + " " + requestDTO.getPhone());

		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Entered validate Phone No %s in class '%s'",
					new Object[] { requestDTO, THIS_COMPONENT_NAME }));
		}
		

		super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		PhoneNumberResponseDTO responseDTO = new PhoneNumberResponseDTO();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		
		BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();

		try {

			
			requestDTO.validate(sessionContext);

			// this.extensionExecutor.preCreate(sessionContext, passRequestDTO);
			// abstractBusinessPolicy =
			// businessPolicyFactory.getBusinesPolicyInstance("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
			// passBusinessPolicyData);

			//IAdapterFactory adapterFactory = AdapterFactoryConfigurator.getInstance().getAdapterFactory("CZ_ACCOUNT_INQUIRY_ADAPTER_FACTORY");
			//IAccountInquiryAdapter adapter = (IAccountInquiryAdapter) adapterFactory.getAdapter("CZ_ACCOUNT_INQUIRY_ADAPTER");
			
			//String affCode = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			//String countryPhoneCode = digx_consulting.get(affCode + "_PHONE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			
			PhoneNumber phone = new PhoneNumber();
			phone.setCountryCode(Integer.parseInt(requestDTO.getCountryCode()));
			phone.setRawInput(requestDTO.getPhone());
			boolean isValid = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().isValidNumber(phone);
			
			responseDTO.setNumberValid(isValid);
			responseDTO.setPhoneNumber(phone.toString());
			
			responseDTO.setStatus(buildStatus(status));
			

		}  catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("RunTimeException from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		//super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	
	public TransferReceiveAmountResponseDTO computeReceiveAmount(SessionContext sessionContext, ForexRateRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("Forex Rate-Service-Welcome: " + requestDTO.getBaseCcy() + " " + requestDTO.getAffiliateCode());

		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Entered into create method of Self Transfer service  Input: SelfTransferReadRequestDTO: %s in class '%s'",
					new Object[] { requestDTO, THIS_COMPONENT_NAME }));
		}
		// super.checkAccessPolicy("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
		// new Object[] { sessionContext, passRequestDTO });

		super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		TransferReceiveAmountResponseDTO responseDTO = new TransferReceiveAmountResponseDTO();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		// PassportDetailBusinessPolicyData passBusinessPolicyData = null;
		BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();

		try {

			
			requestDTO.validate(sessionContext);

			// this.extensionExecutor.preCreate(sessionContext, passRequestDTO);
			// abstractBusinessPolicy =
			// businessPolicyFactory.getBusinesPolicyInstance("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
			// passBusinessPolicyData);
			
			System.out.println("Before sending to domain--->" + requestDTO.getBaseCcy() + " " + requestDTO.getQuoteCcy());

			String ccy1 = requestDTO.getBaseCcy();
			String ccy2 = requestDTO.getQuoteCcy();
			double exchRate = 0;
			double amt = requestDTO.getAmount();
			//String affCode = requestDTO.getAffiliateCode();
			String affCode = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			responseDTO.setBaseCcy(ccy1);
			responseDTO.setQuoteCcy(ccy2);
			System.out.println("Aff Code --->" + affCode);
			if(ccy1.equals(ccy2))
			{
				exchRate = 1;
				
				responseDTO.setBuyRate(exchRate);
				responseDTO.setMidRate(exchRate);
				responseDTO.setSellRate(exchRate);
				responseDTO.setReceiveAmount(amt);
				responseDTO.setSendAmount(amt);
				responseDTO.setExchRate(exchRate);
				
				String pair = "1 " + ccy1 +  " = 1 " + ccy2;
				responseDTO.setCuurencyPair(pair);
				responseDTO.setStatus(buildStatus(status));
	          
				return responseDTO;
			
			}
			
			double receiveAmt = 0;
			
			IAdapterFactory adapterFactory = AdapterFactoryConfigurator.getInstance().getAdapterFactory("CZ_ACCOUNT_INQUIRY_ADAPTER_FACTORY");
			IAccountInquiryAdapter adapter = (IAccountInquiryAdapter) adapterFactory.getAdapter("CZ_ACCOUNT_INQUIRY_ADAPTER");
			ForexRateResponseDTO rateResponse = adapter.fetchForexRate(sessionContext, requestDTO); 
			String lcyCcy = adapter.fetchLocalCurrencyCode(sessionContext, affCode);
			
			System.out.println("LCY CCY --->" + lcyCcy + " " + rateResponse.getMidRate() );
			
			if(rateResponse != null && rateResponse.getMidRate() > 0 && !lcyCcy.equals(""))
			{
				if(ccy2.equals(lcyCcy)) //Use Buy Rate
				{
					exchRate = rateResponse.getBuyRate();
					receiveAmt = amt * exchRate;
				}
				else if(ccy1.equals(lcyCcy)) //Use Sell Rate
				{
					exchRate = rateResponse.getSellRate();
					receiveAmt = amt * exchRate;
				}
				
				responseDTO.setExchRate(exchRate);
				responseDTO.setBuyRate(rateResponse.getBuyRate());
				responseDTO.setMidRate(rateResponse.getMidRate());
				responseDTO.setSellRate(rateResponse.getSellRate());
				responseDTO.setReceiveAmount(receiveAmt);
				responseDTO.setSendAmount(amt);
				String pair = "1 " + ccy1 +  " = " + exchRate + " " +  ccy2;
				responseDTO.setCuurencyPair(pair);
				
				
			}
			
			
			responseDTO.setStatus(buildStatus(status));
			
			
			
			// this.extensionExecutor.postCreate(sessionContext, passRequestDTO,passResponse);

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("Fatal Exception from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("RunTimeException from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	
	public AccountTransferResponseList listTransfers(SessionContext sessionContext)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("List-Service-Welcome: " );

		
		// super.checkAccessPolicy("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
		// new Object[] { sessionContext, passRequestDTO });

		//super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		AccountTransferResponseList responseDTO = new AccountTransferResponseList();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		// PassportDetailBusinessPolicyData passBusinessPolicyData = null;
		BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();

		try {

			
			IAdapterFactory adapterFactory = AdapterFactoryConfigurator.getInstance().getAdapterFactory("CZ_ACCOUNT_INQUIRY_ADAPTER_FACTORY");
			IAccountInquiryAdapter adapter = (IAccountInquiryAdapter) adapterFactory.getAdapter("CZ_ACCOUNT_INQUIRY_ADAPTER");
			
			AccountTransferAssembler assembler = new AccountTransferAssembler();
			
			AccountTransfer accountTransferDomain = new AccountTransfer();
			
			String affiliateCode ="ENG";
			
			responseDTO = assembler.listFromDomainObjectToAccountTransferDTO(accountTransferDomain.list(affiliateCode));
			
			
			
			// com.ecobank.digx.cz.domain.IdentityManagement.entity.PassportDetail
			// passportDomain = new
			// PassportDetailAssembler().toPassportDomainObjectCreate(passRequestDTO);
			// passportDomain.create(passportDomain);
			// selfTransferCreateResponse.setPaymentId(selfTransferDomain.getKey().getId());
			responseDTO.setStatus(buildStatus(status));
			// this.extensionExecutor.postCreate(sessionContext, passRequestDTO,
			// passResponse);

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	
	
	
	public GenericLookupDataResponseList listGenericLookupData(SessionContext sessionContext, String affCode, String lookupCode)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("List-Generic-Lookup-Welcome: " + affCode );

		
		// super.checkAccessPolicy("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
		// new Object[] { sessionContext, passRequestDTO });

		//super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		GenericLookupDataResponseList responseDTO = new GenericLookupDataResponseList();
	
		

		try {
			
			if(affCode == null || affCode.equals("") || affCode.equals("X"))
			   affCode = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			

			AccountTransferAssembler assembler = new AccountTransferAssembler();
			
			GenericLookupData genericLookupDataDomain = new GenericLookupData();
			
			responseDTO = assembler.fromDomainObjectToGenericLookupList(genericLookupDataDomain.list(affCode, lookupCode));
			
			responseDTO.setStatus(buildStatus(status));
			

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of List Generic Lookup Data, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	

	public AccountTransferResponseDTO create(SessionContext sessionContext, AccountTransferRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("Account-Transfer-Service-Welcome: " + requestDTO.getSenderAccountNo());
		System.out.println("Account-Transfer-Service-Welcome: " + requestDTO.getBeneficiaryAccountNo());

		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Entered into create method of Account Transfer service  Input: AccountTransferReadRequestDTO: %s in class '%s'",
					new Object[] { requestDTO, THIS_COMPONENT_NAME }));
		}
		// super.checkAccessPolicy("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
		// new Object[] { sessionContext, passRequestDTO });

		super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		AccountTransferResponseDTO responseDTO = new AccountTransferResponseDTO();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		// PassportDetailBusinessPolicyData passBusinessPolicyData = null;
		BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();
		
		AccountTransferAssembler assembler = new AccountTransferAssembler();
		
	      

		try {

			//String affCode = "ENG";
			String affCode = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			
			requestDTO.validate(sessionContext);
			
			DemandDepositAccount ddAccount = new DemandDepositAccount();
			AccountKey key =new AccountKey();
			key.setAccountId(requestDTO.getSenderAccountNo());
			DemandDepositAccount sendAccountInfo = ddAccount.read(key);
			
			System.out.println("Fetch Sender account --->" + sendAccountInfo.getPartyId().getDisplayValue() + " " + sendAccountInfo.getCurrency() + " " + sendAccountInfo.getAvailableBalance().getAmount());
			 
			requestDTO.setSenderAccountCcy(sendAccountInfo.getCurrency());
			requestDTO.setSenderCustomerId(sendAccountInfo.getPartyId().getValue());
			//requestDTO.setSenderName(sendAccountInfo.getDisplayName());
			
			
			ddAccount = new DemandDepositAccount();
			key =new AccountKey();
			key.setAccountId(requestDTO.getBeneficiaryAccountNo());
			DemandDepositAccount beneficiaryAccountInfo = ddAccount.read(key);
			
			System.out.println("Fetch Beneficiary account --->" + beneficiaryAccountInfo.getTitle() + " " + beneficiaryAccountInfo.getPartyId().getDisplayValue()  + " " + beneficiaryAccountInfo.getCurrency() + " " + beneficiaryAccountInfo.getAvailableBalance().getAmount());
			 
			requestDTO.setBeneficiaryAccountCcy(beneficiaryAccountInfo.getCurrency());
			requestDTO.setBeneficiaryCustomerId(beneficiaryAccountInfo.getPartyId().getValue());
			//requestDTO.setBeneficiaryName(sendAccountInfo.getDisplayName());
			
			requestDTO.setExternalRefNo(GetRefNumber("DIG",12));
			requestDTO.setUserId(sessionContext.getUserId());
			requestDTO.setAffiliateCode(affCode);
			
			String tranCode = digx_consulting.get("DIGX_CZ_INTERNAL_TRANSFER_TRAN_CODE", "");
			
			requestDTO.setTranCode(tranCode);
			
			
			double exchRate = 0;
			double receiveAmt = 0;
			String ccy1 = requestDTO.getSenderAccountCcy();
			String ccy2 = requestDTO.getBeneficiaryAccountCcy();
			
			System.out.println("Fetch CCY 1 & CCY 2 --->" + ccy1 + " " + ccy2);
			String pair = "";
			
			if(ccy1.equals(ccy2))
			{
				exchRate = 1;
				receiveAmt = requestDTO.getAmount();
				pair = "1 " + ccy1 +  " = " + exchRate + " " +  ccy2;
				
			}
			else
			{
				IAdapterFactory adapterFactory = AdapterFactoryConfigurator.getInstance().getAdapterFactory("CZ_ACCOUNT_INQUIRY_ADAPTER_FACTORY");
				IAccountInquiryAdapter adapter = (IAccountInquiryAdapter) adapterFactory.getAdapter("CZ_ACCOUNT_INQUIRY_ADAPTER");
				ForexRateRequestDTO rateDTO = new ForexRateRequestDTO();
				rateDTO.setAffiliateCode(affCode);
				rateDTO.setBaseCcy(ccy1);
				rateDTO.setBranchCode(affCode);
				rateDTO.setQuoteCcy(ccy2);
				rateDTO.setAmount(requestDTO.getAmount());
				
				ForexRateResponseDTO rateResponse = adapter.fetchForexRate(sessionContext, rateDTO); 
				String lcyCcy = adapter.fetchLocalCurrencyCode(sessionContext, affCode);
				
				if(ccy2.equals(lcyCcy)) //Use Buy Rate
				{
					exchRate = rateResponse.getBuyRate();
					receiveAmt = requestDTO.getAmount() * exchRate;
					pair = "1 " + ccy1 +  " = " + exchRate + " " +  ccy2;
				}
				else if(ccy1.equals(lcyCcy)) //Use Sell Rate
				{
					exchRate = rateResponse.getSellRate();
					receiveAmt = requestDTO.getAmount() * exchRate;
					
					pair = "1 " + ccy2 +  " = " + new BigDecimal(1/exchRate).setScale(4,RoundingMode.FLOOR) + " " +  ccy1;
				}
				
				
			}
			
			System.out.println("Receive Amount: " + exchRate + " " + receiveAmt );
			int ccyDecimal = 2;
			
			requestDTO.setExchRate(exchRate);
			requestDTO.setReceiveAmount(receiveAmt);
			
			responseDTO.setExchRate(new BigDecimal(exchRate));
			CurrencyAmount rAmount = new CurrencyAmount();
			rAmount.setAmount(new BigDecimal(receiveAmt).setScale(ccyDecimal,RoundingMode.FLOOR));
			rAmount.setCurrency(ccy2);
			responseDTO.setReceiveAmount(rAmount);
			
			CurrencyAmount sAmount = new CurrencyAmount();
			sAmount.setAmount(new BigDecimal(requestDTO.getAmount()).setScale(ccyDecimal,RoundingMode.FLOOR));
			sAmount.setCurrency(ccy1);
			responseDTO.setSendAmount(sAmount);
			
			//String pair = "1 " + ccy1 +  " = " + exchRate + " " +  ccy2;
			
			String tranType = requestDTO.getTranType();
			if(tranType.equalsIgnoreCase("Self"))
				tranType ="SELF_INTERNAL_TRANSFER";
			else if(tranType.equalsIgnoreCase("Others"))
				tranType ="OTHER_INTERNAL_TRANSFER";
			
			requestDTO.setTranType(tranType);
			
			
			responseDTO.setCurrencyPair(pair);
			responseDTO.setExternalRefNo(requestDTO.getExternalRefNo());
			responseDTO.setResponseCode("000");
			
			AccountTransferBusinessPolicyData policyData = assembler.fromDTOObjectToBusinessPolicyDTO(requestDTO);

			abstractBusinessPolicy = businessPolicyFactory.getBusinesPolicyInstance("com.ecobank.digx.cz.app.accountinquiry.service.AccountInquiry.create", (IBusinessPolicyDTO)policyData);
		    abstractBusinessPolicy.validate("DIGX_PY_0131");
		      
			// this.extensionExecutor.preCreate(sessionContext, passRequestDTO);
			// abstractBusinessPolicy =
			// businessPolicyFactory.getBusinesPolicyInstance("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
			// passBusinessPolicyData);
			
			String partyId = sessionContext.getTransactingPartyCode();
			System.out.println("Before sending to domain--->" + partyId);

			System.out.println("Before sending to domain--->" + requestDTO.getSenderAccountNo() + " " + pair);
			
			System.out.println("Receive Amount & FCY Rate--->" + requestDTO.getExchRate() + " " + requestDTO.getReceiveAmount());

			
			 
			AccountTransfer accountTransferDomain  = assembler.toAccountTransferDomainObjectCreate(requestDTO);
			accountTransferDomain.create(accountTransferDomain);
			
			
			
			responseDTO.setResponseMessage("SUCCESS");
			
	         
			
			responseDTO.setStatus(buildStatus(status));
			
			System.out.println("Response DTO--->" + responseDTO.getExternalRefNo());
			// this.extensionExecutor.postCreate(sessionContext, passRequestDTO,
			// passResponse);

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("Fatal Exception from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("RunTimeException from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	
	
	/*public AccountTransferResponseDTO update(SessionContext sessionContext, String paymentId)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("Update Transfer to credit customer: " + paymentId);
		
		//super.checkAccessPolicy("com.ecobank.digx.cz.app.accountinquiry.service.AccountInquiry.update",
		//		new Object[] { sessionContext, paymentId });
		
		//super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		AccountTransferResponseDTO responseDTO = new AccountTransferResponseDTO();
		responseDTO.setStatus(fetchStatus());
		
		//AbstractBusinessPolicy abstractBusinessPolicy = null;
		// PassportDetailBusinessPolicyData passBusinessPolicyData = null;
		//BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();
	
		AccountTransferAssembler assembler = new AccountTransferAssembler();
		
	      

		try {

			//String affCode = "";
			
			AccountTransferKey keyTran = new AccountTransferKey();
			keyTran.setExternalRefNo(paymentId);
			
			AccountTransfer accountTransferDomain = new AccountTransfer();
			AccountTransfer transferInfo2 = accountTransferDomain.read(keyTran);
			
			
			AccountTransferRequestDTO requestDTO = assembler.fromDomainObjectToAccountTransferRequestTO(transferInfo2);
			
			DemandDepositAccount ddAccount = new DemandDepositAccount();
			AccountKey key =new AccountKey();
			key.setAccountId(requestDTO.getSenderAccountNo());
			DemandDepositAccount sendAccountInfo = ddAccount.read(key);
			
			System.out.println("Fetch Sender account --->" + sendAccountInfo.getPartyId().getDisplayValue() + " " + sendAccountInfo.getCurrency() + " " + sendAccountInfo.getAvailableBalance().getAmount());
			 
			requestDTO.setSenderAccountCcy(sendAccountInfo.getCurrency());
			requestDTO.setSenderCustomerId(sendAccountInfo.getPartyId().getValue());
			//requestDTO.setSenderName(sendAccountInfo.getDisplayName());
			
			
			ddAccount = new DemandDepositAccount();
			key =new AccountKey();
			key.setAccountId(requestDTO.getBeneficiaryAccountNo());
			DemandDepositAccount beneficiaryAccountInfo = ddAccount.read(key);
			
			System.out.println("Fetch Beneficiary account --->" + beneficiaryAccountInfo.getTitle() + " " + beneficiaryAccountInfo.getPartyId().getDisplayValue()  + " " + beneficiaryAccountInfo.getCurrency() + " " + beneficiaryAccountInfo.getAvailableBalance().getAmount());
			 
			requestDTO.setBeneficiaryAccountCcy(sendAccountInfo.getCurrency());
			requestDTO.setBeneficiaryCustomerId(sendAccountInfo.getPartyId().getValue());
			//requestDTO.setBeneficiaryName(sendAccountInfo.getDisplayName());
		
			responseDTO = this.postTransfer(sessionContext, requestDTO);
			
			
			
			if(transferInfo2 != null)
			{
				transferInfo2.setResponseCode(responseDTO.getResponseCode());
				transferInfo2.setResponseMessage(responseDTO.getResponseMessage());
				transferInfo2.setCBAReferenceNo(responseDTO.getCbaReferenceNo());
				transferInfo2.setTransferDate(new com.ofss.fc.datatype.Date());
				accountTransferDomain.update(transferInfo2);
			}
	         
			// com.ecobank.digx.cz.domain.IdentityManagement.entity.PassportDetail
			// passportDomain = new
			// PassportDetailAssembler().toPassportDomainObjectCreate(passRequestDTO);
			// passportDomain.create(passportDomain);
			// selfTransferCreateResponse.setPaymentId(selfTransferDomain.getKey().getId());
			//responseDTO.setStatus(fetchStatus());
			responseDTO.setStatus(buildStatus(status));
			// this.extensionExecutor.postCreate(sessionContext, passRequestDTO,
			// passResponse);

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}*/
	
	
	public AccountInquiryResponseDTO fetchAccountDetail(SessionContext sessionContext, AccountInquiryRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("Account-Inquiry-Service-Welcome: " + requestDTO.getAccountNo());

		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Entered into create method of Self Transfer service  Input: SelfTransferReadRequestDTO: %s in class '%s'",
					new Object[] { requestDTO, THIS_COMPONENT_NAME }));
		}
		// super.checkAccessPolicy("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
		// new Object[] { sessionContext, passRequestDTO });

		super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		AccountInquiryResponseDTO responseDTO = new AccountInquiryResponseDTO();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		// PassportDetailBusinessPolicyData passBusinessPolicyData = null;
		BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();

		try {
			
			//String affCode ="ENG";
			
			String affCode = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			System.out.println("Before Validate ::: " + affCode);
			System.out.println("AFFCODE--- ::: " + affCode + " " + sessionContext.getTargetUnit());


			// com.ofss.digx.app.sms.adapter.impl.user.UserPartyAdapterFactory
			// REST ->Service Class-> Domain/Adapter -> Backend DB/FCUBS/ESB/Unified API
			requestDTO.validate(sessionContext);
			
			System.out.println("Before sending22 to domain-bank code---> "  + sessionContext.getBankCode());

			// this.extensionExecutor.preCreate(sessionContext, passRequestDTO);
			// abstractBusinessPolicy =
			// businessPolicyFactory.getBusinesPolicyInstance("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
			// passBusinessPolicyData);

			System.out.println("Before sending to domain--->" + requestDTO.getAccountNo());
			
			/*DemandDepositAccount ddAccount = new DemandDepositAccount();
			AccountKey key =new AccountKey();
			key.setAccountId(requestDTO.getAccountNo());
			DemandDepositAccount ddx = ddAccount.read(key);
			
			System.out.println("Fetch account name--->" + ddx.getDisplayName() + " " + ddx.getCurrency() + " " + ddx.getAvailableBalance().getAmount());
 
			responseDTO.setAccountName(ddx.getDisplayName());
			responseDTO.setAvailableBalance(ddx.getAvailableBalance().getAmount().doubleValue());
			responseDTO.setBranchId(ddx.getBranchId());
			responseDTO.setAccountStatus(ddx.getStatus().name().toString());
			responseDTO.setCcy(ddx.getCurrency());*/
			
			
			//String tranCode = digx_consulting.get("DIGX_CZ_INTERNAL_TRANSFER_TRAN_CODE", "");
			IAdapterFactory adapterFactory = AdapterFactoryConfigurator.getInstance().getAdapterFactory("CZ_ACCOUNT_INQUIRY_ADAPTER_FACTORY");
			IAccountInquiryAdapter adapter = (IAccountInquiryAdapter) adapterFactory.getAdapter("CZ_ACCOUNT_INQUIRY_ADAPTER");
			
			
			
			requestDTO.setExpressAccount(this.isExpressAccount(affCode, requestDTO.getAccountNo()));
			if(requestDTO.isExpressAccount())
			  requestDTO.setAccountNo(this.formatExpressMobileNo(affCode, requestDTO.getAccountNo()));
			
			System.out.println("Before sending to domain--->" + requestDTO.getAccountNo() + " " + requestDTO.isExpressAccount());
			
			responseDTO = adapter.fetchAccountName(sessionContext, requestDTO);
			
			System.out.println("Before -- + fetch RM Detail ");
			
			/*RMDetailInfoRequestDTO reqDto = new RMDetailInfoRequestDTO();
			reqDto.setAccountNo(requestDTO.getAccountNo());
			reqDto.setAffiliateCode(affCode);
			RMDetailInfoResponseDTO respc = adapter.fetchRMDetail(sessionContext, reqDto);
			if(respc != null)
				System.out.println("Before -- " + respc.getName());*/
			
			responseDTO.setStatus(buildStatus(status));
		

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("Fatal Exception from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("RunTimeException from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	
	public AccountDetailDTO fetchAccountDetailFullInfo(SessionContext sessionContext, AccountDetailRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("Account-Inquiry-Service-Welcome: " + requestDTO.getAccountNo());

		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Entered into create method of Self Transfer service  Input: SelfTransferReadRequestDTO: %s in class '%s'",
					new Object[] { requestDTO, THIS_COMPONENT_NAME }));
		}
		// super.checkAccessPolicy("com.ecobank.digx.cz.app.IdentityManagement.service.PassportDetail.create",
		// new Object[] { sessionContext, passRequestDTO });

		super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		AccountDetailDTO responseDTO = new AccountDetailDTO();
		AbstractBusinessPolicy abstractBusinessPolicy = null;
		// PassportDetailBusinessPolicyData passBusinessPolicyData = null;
		BusinessPolicyFactory businessPolicyFactory = BusinessPolicyFactory.getInstance();

		try {
			
			
			String affCode = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			if(requestDTO.getAffiliateCode() == null || requestDTO.getAffiliateCode().equals(""))
				requestDTO.setAffiliateCode(affCode);
			
			//requestDTO.setAffiliateCode(affCode);
			System.out.println("Before Validate ::: " + affCode);
			//System.out.println("AFFCODE--- ::: " + affCode + " " + sessionContext.getTargetUnit());
            requestDTO.validate(sessionContext);
			System.out.println("Before sending to domain--->" + requestDTO.getAccountNo());
			
			
			IAdapterFactory adapterFactory = AdapterFactoryConfigurator.getInstance().getAdapterFactory("CZ_ACCOUNT_INQUIRY_ADAPTER_FACTORY");
			IAccountInquiryAdapter adapter = (IAccountInquiryAdapter) adapterFactory.getAdapter("CZ_ACCOUNT_INQUIRY_ADAPTER");
			
			
			
			//AccountDetailDTO fetchAccountDetail(SessionContext sessionContext,AccountDetailRequestDTO
			
			responseDTO = adapter.fetchAccountDetail(sessionContext, requestDTO);  //fetchAccountName(sessionContext, requestDTO);
			
			System.out.println("Before -- + fetch RM Detail ");
			
			
			
			RMDetailInfoRequestDTO reqDto = new RMDetailInfoRequestDTO();
			reqDto.setAccountNo(requestDTO.getAccountNo());
			reqDto.setAffiliateCode(affCode);
			RMDetailInfoResponseDTO respc = adapter.fetchRMDetail(sessionContext, reqDto);
			if(respc != null)
				System.out.println("Before -- " + respc.getName());
			
			requestDTO.setCustomerId(responseDTO.getCustomerId());
			
			System.out.println("Before -- + fetch MIS Business Code and Signatory Detail ");
			
			AccountMISAndSignatoryDetailDTO misSignInfo =adapter.fetchAccountAndCustomerMISAndSignatoryCount(sessionContext, requestDTO);
			
			System.out.println("after -- + a MIS Business Code : " + misSignInfo.getMisBusinessCode() + " " + misSignInfo.getSignatoryCount());
			
			responseDTO.setStatus(buildStatus(status));
		

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("Fatal Exception from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("RunTimeException from create for Self Transfer Create Request '%s'",
							new Object[] { requestDTO }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	
	
	
	private AccountTransferResponseDTO postTransfer(SessionContext sessionContext, AccountTransferRequestDTO request)
	{
		AccountTransferResponseDTO responseDTO = new AccountTransferResponseDTO();
		try
		{
			
			//String affCode = request.getAffiliateCode();
			
			String affCode = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			
			DemandDepositAccount ddAccount = new DemandDepositAccount();
			AccountKey key =new AccountKey();
			key.setAccountId(request.getSenderAccountNo());
			DemandDepositAccount senderAcc = ddAccount.read(key);
			
			String postingBranchCode = senderAcc.getBranchId();
			
			ddAccount = new DemandDepositAccount();
			 key =new AccountKey();
			key.setAccountId(request.getBeneficiaryAccountNo());
			DemandDepositAccount benAcc = ddAccount.read(key);
			
			
			
			//String ccy1 = senderAcc.getCurrency();
			//String ccy2 = benAcc.getCurrency();
			IAdapterFactory adapterFactory = AdapterFactoryConfigurator.getInstance().getAdapterFactory("CZ_ACCOUNT_INQUIRY_ADAPTER_FACTORY");
			IAccountInquiryAdapter adapter = (IAccountInquiryAdapter) adapterFactory.getAdapter("CZ_ACCOUNT_INQUIRY_ADAPTER");
			System.out.println("Affiliate Code: " + affCode);
			String lcyCcy =  digx_consulting.get(affCode + "_LCY", "");
			//adapter.fetchLocalCurrencyCode(sessionContext, affCode);
			
			
			String sourceCode = digx_consulting.get("DIGX_FLEX_SOURCE_CODE", "");
			String tranCode = request.getTranCode();
			
			System.out.println("LCY-CCY:: " + lcyCcy + "  " + affCode);
			double exchRate = 0, lcyAmt = 0;
			
			BatchPostingRequestDTO batchRequest = new BatchPostingRequestDTO();
			
			
			String nar2 = "ACCOUNT TRANSFER " + " BO " + request.getSenderName() + " " + request.getPurpose();
			String nar1 = "ACCOUNT TRANSFER " +  " IFO " + request.getBeneficiaryName() + " " + request.getPurpose();
			
			batchRequest.setAffiliateCode(request.getAffiliateCode());
			String batchRefNo = request.getExternalRefNo(); //  GetRefNumber("DIG",12);
			batchRequest.setBatchRefNo(batchRefNo);
			batchRequest.setPostingBranchCode(postingBranchCode);
			batchRequest.setSourceCode(sourceCode);
			
			int ccyDecimal = 2;
			
			List<TransactionEntry> list = new ArrayList<TransactionEntry>();
			
			TransactionEntry entryInfo = new TransactionEntry();
			entryInfo.setAccountBranchCode(senderAcc.getBranchId());
			entryInfo.setAccountNo(request.getSenderAccountNo());
			entryInfo.setAmount(new BigDecimal(request.getAmount()));
			entryInfo.setCreditOrDebit("D");
			entryInfo.setCurrencyCode(senderAcc.getCurrency());
			entryInfo.setCustomerId(senderAcc.getPartyId().getValue());
			entryInfo.setExternalRefNo(batchRefNo);
			entryInfo.setNarration(nar1);
			entryInfo.setSerialNo(1);
			String dt = GetDateFormat6(new Date());
			entryInfo.setValueDate(dt);
			entryInfo.setTranDate(dt);
			entryInfo.setTransactionCode(tranCode);
			
			exchRate = this.FetchMidRate(entryInfo.getCurrencyCode(), lcyCcy, affCode, sessionContext, adapter);
			entryInfo.setExchangeRate(new BigDecimal(exchRate).setScale(4,RoundingMode.FLOOR));
			lcyAmt = exchRate * entryInfo.getAmount().doubleValue();
			BigDecimal lcyAmt2 = new BigDecimal(lcyAmt).setScale(ccyDecimal,RoundingMode.FLOOR);
			entryInfo.setLcyAmount(lcyAmt2);
			
			list.add(entryInfo);
			
		
			
			TransactionEntry entryInfo2 = new TransactionEntry();
			entryInfo2.setAccountBranchCode(benAcc.getBranchId());
			entryInfo2.setAccountNo(request.getBeneficiaryAccountNo());
			//entryInfo2.setAmount(new BigDecimal(Double.toString(request.getReceiveAmount())).setScale(ccyDecimal,RoundingMode.FLOOR));
			entryInfo2.setCreditOrDebit("C");
			entryInfo2.setCurrencyCode(benAcc.getCurrency());
			entryInfo2.setCustomerId(benAcc.getPartyId().getValue());
		
			entryInfo2.setExternalRefNo(batchRefNo);

			entryInfo2.setNarration(nar2);
			entryInfo2.setSerialNo(2);
			
			entryInfo2.setValueDate(dt);
			entryInfo2.setTranDate(dt);
			entryInfo2.setTransactionCode(tranCode);
			
			
			if(lcyCcy.equals(entryInfo2.getCurrencyCode()))
			{
				
				entryInfo2.setLcyAmount(lcyAmt2);
				entryInfo2.setAmount(lcyAmt2);
				entryInfo2.setExchangeRate(BigDecimal.ONE);
			}
			else
			{
				exchRate = this.FetchMidRate(entryInfo2.getCurrencyCode(), lcyCcy, affCode, sessionContext, adapter);
				int ccyD = 2;
				BigDecimal fcyAmt  = new BigDecimal(lcyAmt2.doubleValue()/exchRate).setScale(ccyD,RoundingMode.FLOOR);
				entryInfo2.setExchangeRate(new BigDecimal(Double.toString(exchRate)).setScale(4,RoundingMode.FLOOR));
				//lcyAmt = exchRate * entryInfo2.getAmount().doubleValue();
				entryInfo2.setLcyAmount(lcyAmt2);//  //new BigDecimal(lcyAmt));
				entryInfo2.setAmount(fcyAmt);
			}
			
			
			list.add(entryInfo2);
			
			batchRequest.setTransactionEntries(list);
			
			XStream xs = new XStream();
			System.out.println("Batch Upload : " + xs.toXML(batchRequest));
			
			FlexIFPostingAdapter flexAdapter = new FlexIFPostingAdapter();
			BatchPostingResponseDTO response = flexAdapter.postBatchTran(batchRequest, false);
			
			TransactionStatus transactionStatus = fetchTransactionStatus();
			
			   responseDTO.setStatus(buildStatus(transactionStatus));
			   responseDTO.setCbaReferenceNo(response.getCbaReferenceNo());
			   responseDTO.setResponseCode(response.getResponseCode());
			   responseDTO.setResponseMessage(response.getResponseMessage());
			
			if(response.getResponseCode().equals("000"))
			{
			   
			}
			else
			{
				
			}
				
			//responseDTO.setStatus(Status.ACTIVE);
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
			//fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { AccountInquiry.class.getName() }),e);
		}
		
		return responseDTO;

	}
	
	public double FetchMidRate(String ccy1, String lcyCcy,String affCode,SessionContext sessionContext,IAccountInquiryAdapter adapter)
	{
		double rate = 0;
		
		if(ccy1.equals(lcyCcy))
			return 1;
		
		try
		{
			
			ForexRateRequestDTO rateDTO = new ForexRateRequestDTO();
			rateDTO.setAffiliateCode(affCode);
			rateDTO.setBaseCcy(ccy1);
			rateDTO.setBranchCode(affCode);
			rateDTO.setQuoteCcy(lcyCcy);
			rateDTO.setAmount(0);
			
			System.out.println("Fetch Mid Rate:: " + ccy1 + "  " + lcyCcy + "  " + affCode);
			
			ForexRateResponseDTO rateResponse = adapter.fetchForexRate(sessionContext, rateDTO); 
			rate = rateResponse.getMidRate();
			System.out.println("Fetch Mid Rate:: " + rate);
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return rate;
		
	}
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }
	
	public  String GetDateFormat6(Date dt)
    {
		 SimpleDateFormat dff4 = new SimpleDateFormat("yyyy-MM-dd");
		return dff4.format(dt) ;
    }
	
	
	public AlertMessageResponseDTO sendMessage(SessionContext sessionContext, AlertMessageRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {

		System.out.println("SMS Alert: " + requestDTO.getMobileNo());
		

		//super.canonicalizeInput(requestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus status = fetchTransactionStatus();
		AlertMessageResponseDTO responseDTO = new AlertMessageResponseDTO();
		
	      

		try {

			String affCode = requestDTO.getAffCode();
			
			RemoteSMSAlertRepositoryAdapter adapter = RemoteSMSAlertRepositoryAdapter.getInstance();
			ESBResponseDTO resp = adapter.sendMessage(requestDTO.getMobileNo(), affCode, requestDTO.getMessage());
		
			responseDTO.setResponseCode(resp.getResponseCode());
			responseDTO.setResponseMessage(resp.getResponseMessage());
			responseDTO.setStatus(buildStatus(status));
			// this.extensionExecutor.postCreate(sessionContext, passRequestDTO,
			// passResponse);

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			
		} finally {
			Interaction.close();
		}
		super.encodeOutput(responseDTO);
		super.checkResponsePolicy(sessionContext, responseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, responseDTO, THIS_COMPONENT_NAME }));
		}
		return responseDTO;
	}
	
	
	public boolean isExpressAccount(String affCode, String accNo)
	{
		//boolean isExpress = false;
		//String phonePrefix = digx_consulting.get(affCode + "_PHONE_PREFIX", "");
		String accLen = digx_consulting.get(affCode + "_ACCOUNT_NO_LENGTH", "");
		int len = Integer.parseInt(accLen);
		if(len == accNo.length())
			return false;
		else
			return true;
		
		//if(accNo.startsWith("0"))
			//accNo = phonePrefix + accNo.substring(1);
	}
	
	public String formatExpressMobileNo(String affCode, String accNo)
	{
		
		String phonePrefix = digx_consulting.get(affCode + "_PHONE_PREFIX", "");
		String accLen = digx_consulting.get(affCode + "_ACCOUNT_NO_LENGTH", "");
		int oldAcclen = 16;  //Old account length before migration
		int len = Integer.parseInt(accLen);
		if(len == accNo.length())
			return accNo;
		else if(oldAcclen == accNo.length())
			return accNo;
		
		
		if(accNo.startsWith("0"))
			accNo = phonePrefix + accNo.substring(1);
		
		return accNo;
	}

	
	
	
	/*public DemandDepositAccount read(AccountKey key) throws Exception {
	    DemandDepositRepository repository = DemandDepositRepository.getInstance();
	    return repository.read(key);
	  }*/

}
