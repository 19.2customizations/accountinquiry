package com.ecobank.digx.cz.app.domain.accountinquiry.entity;

import com.ofss.fc.framework.domain.AbstractDomainObjectKey;

public class GenericLookupDataKey extends AbstractDomainObjectKey {
	private static final long serialVersionUID = -4865985769527653277L;
	private String refId;
	
	private String determinantValue;

	
	public String getDeterminantValue() {
		return determinantValue;
	}

	public void setDeterminantValue(String determinantValue) {
		this.determinantValue = determinantValue;
	}

	@Override
	public String keyAsString() {
		// TODO Auto-generated method stub
		return refId;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}
	
	
	
	
	

}
