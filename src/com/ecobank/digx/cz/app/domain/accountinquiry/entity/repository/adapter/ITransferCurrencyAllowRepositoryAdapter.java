package com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.adapter;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.TransferCurrencyAllow;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.TransferCurrencyAllowKey;
import com.ofss.digx.framework.domain.repository.IRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public interface ITransferCurrencyAllowRepositoryAdapter extends IRepositoryAdapter<TransferCurrencyAllow, TransferCurrencyAllowKey>
{
	
	 public abstract void create(TransferCurrencyAllow accountTransfer) throws Exception;
	 
	 public TransferCurrencyAllow read(TransferCurrencyAllowKey key) throws Exception;
	 
	 public  boolean validateCurrencyAllow (String affCode, String transactionType, String drCcy, String crCcy) throws Exception;

}


