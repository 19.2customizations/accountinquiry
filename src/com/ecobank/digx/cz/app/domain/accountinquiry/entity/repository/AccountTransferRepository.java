package com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository;

import java.util.List;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.AccountTransfer;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.AccountTransferKey;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.adapter.IAccountTransferRepositoryAdapter;
import com.ofss.digx.framework.domain.repository.AbstractDomainObjectRepository;
import com.ofss.digx.framework.domain.repository.RepositoryAdapterFactory;
import com.ofss.digx.infra.exceptions.Exception;

public class AccountTransferRepository extends AbstractDomainObjectRepository<AccountTransfer, AccountTransferKey> {
	private static AccountTransferRepository singletonInstance;

	public static AccountTransferRepository getInstance() {
		if (singletonInstance == null) {
			synchronized (AccountTransferRepository.class) {
				if (singletonInstance == null) {
					singletonInstance = new AccountTransferRepository();
				}
			}
		}
		return singletonInstance;
	}

	public void create(AccountTransfer object) throws Exception {
		IAccountTransferRepositoryAdapter repositoryAdapter = (IAccountTransferRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("ACCOUNT_TRANSFER_REPOSITORY_ADAPTER");
		repositoryAdapter.create(object);
	}
	
	public List<AccountTransfer> list(String affiliateCode) throws Exception {
		IAccountTransferRepositoryAdapter repositoryAdapter = (IAccountTransferRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("ACCOUNT_TRANSFER_REPOSITORY_ADAPTER");
		return repositoryAdapter.list(affiliateCode);
	}

	@Override
	public void delete(AccountTransfer arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public AccountTransfer read(AccountTransferKey key) throws Exception {
		// TODO Auto-generated method stub
		IAccountTransferRepositoryAdapter repositoryAdapter = (IAccountTransferRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("ACCOUNT_TRANSFER_REPOSITORY_ADAPTER");
		return repositoryAdapter.read(key);
	}

	@Override
	public void update(AccountTransfer accountTransfer) throws Exception {
		IAccountTransferRepositoryAdapter repositoryAdapter = (IAccountTransferRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("ACCOUNT_TRANSFER_UPDATE_REPOSITORY_ADAPTER");
		 repositoryAdapter.update(accountTransfer);
		 
		 return;

	}

}
