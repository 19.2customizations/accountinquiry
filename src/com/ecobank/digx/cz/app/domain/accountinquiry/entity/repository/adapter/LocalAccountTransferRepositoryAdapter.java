package com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.adapter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.AccountTransfer;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.AccountTransferKey;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.digx.framework.domain.repository.adapter.AbstractLocalRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public class LocalAccountTransferRepositoryAdapter extends AbstractLocalRepositoryAdapter<AccountTransfer>
		implements IAccountTransferRepositoryAdapter {

	private static LocalAccountTransferRepositoryAdapter singletonInstance;

	public static LocalAccountTransferRepositoryAdapter getInstance() {
		if (singletonInstance == null) {
			synchronized (LocalAccountTransferRepositoryAdapter.class) {
				if (singletonInstance == null) {
					singletonInstance = new LocalAccountTransferRepositoryAdapter();
				}
			}
		}
		return singletonInstance;
	}

	public void create(AccountTransfer object) throws Exception {
		if (object.getRefKey() != null) {
			/*
			 * object.getPassportNumber().setDeterminantValue(
			 * DeterminantResolver.getInstance().fetchDeterminantValue(AccountTransfer.class
			 * .getName()));
			 */
		}
		super.insert(object);
	}

	public void update(AccountTransfer object) throws Exception {

		super.update(object);
		
		
	}

	@Override
	public AccountTransfer read(AccountTransferKey key) throws Exception {
		// TODO Auto-generated method stub

		key.setDeterminantValue(
				DeterminantResolver.getInstance().fetchDeterminantValue(AccountTransfer.class.getName()));
		return (AccountTransfer) get(AccountTransfer.class, (Serializable) key);

	}

	public List<AccountTransfer> list(String affCode) throws Exception {
		HashMap<String, Object> parameters = null;
		List<AccountTransfer> list = null;
		parameters = new HashMap<>();
		parameters.put("affiliateCode", affCode);
		list = executeNamedQuery("ListTransferByAffiliate", parameters);
		return list;
	}

}
