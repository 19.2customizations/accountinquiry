package com.ecobank.digx.cz.app.domain.accountinquiry.entity.policy;

import com.ofss.digx.app.config.dto.workingwindow.WorkingWindowCheckResponse;
import com.ofss.fc.framework.domain.policy.IBusinessPolicyDTO;

public class AccountTransferBusinessPolicyData implements IBusinessPolicyDTO {
	
	
	private WorkingWindowCheckResponse workingWindowCheckResponse;
	
	  private String senderAccountNo;
	  private String senderName;
	  private String senderAccountCcy;
	  private String affiliateCode;
	  private String beneficiaryAccountNo;
	  private String beneficiaryName;
	  private String beneficiaryAccountCcy;
	  private double amount;
	  private double receiveAmount;
	  private double exchRate;
	  private String tranType;
	  private String transferWhen;
	  private String transferDate;
	  private String senderCustomerId;
	  private String beneficiaryCustomerId;
	  
	  
	  
	public WorkingWindowCheckResponse getWorkingWindowCheckResponse() {
		return workingWindowCheckResponse;
	}
	public void setWorkingWindowCheckResponse(WorkingWindowCheckResponse workingWindowCheckResponse) {
		this.workingWindowCheckResponse = workingWindowCheckResponse;
	}
	public String getSenderAccountNo() {
		return senderAccountNo;
	}
	public void setSenderAccountNo(String senderAccountNo) {
		this.senderAccountNo = senderAccountNo;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getSenderAccountCcy() {
		return senderAccountCcy;
	}
	public void setSenderAccountCcy(String senderAccountCcy) {
		this.senderAccountCcy = senderAccountCcy;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getBeneficiaryAccountNo() {
		return beneficiaryAccountNo;
	}
	public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
		this.beneficiaryAccountNo = beneficiaryAccountNo;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getBeneficiaryAccountCcy() {
		return beneficiaryAccountCcy;
	}
	public void setBeneficiaryAccountCcy(String beneficiaryAccountCcy) {
		this.beneficiaryAccountCcy = beneficiaryAccountCcy;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getReceiveAmount() {
		return receiveAmount;
	}
	public void setReceiveAmount(double receiveAmount) {
		this.receiveAmount = receiveAmount;
	}
	public double getExchRate() {
		return exchRate;
	}
	public void setExchRate(double exchRate) {
		this.exchRate = exchRate;
	}
	public String getTranType() {
		return tranType;
	}
	public void setTranType(String tranType) {
		this.tranType = tranType;
	}
	public String getTransferWhen() {
		return transferWhen;
	}
	public void setTransferWhen(String transferWhen) {
		this.transferWhen = transferWhen;
	}
	public String getTransferDate() {
		return transferDate;
	}
	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}
	public String getSenderCustomerId() {
		return senderCustomerId;
	}
	public void setSenderCustomerId(String senderCustomerId) {
		this.senderCustomerId = senderCustomerId;
	}
	public String getBeneficiaryCustomerId() {
		return beneficiaryCustomerId;
	}
	public void setBeneficiaryCustomerId(String beneficiaryCustomerId) {
		this.beneficiaryCustomerId = beneficiaryCustomerId;
	}
	  
	  
	  

}
