package com.ecobank.digx.cz.app.domain.accountinquiry.entity;


import com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.TransferCurrencyAllowRepository;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.framework.domain.IPersistenceObject;
import com.ofss.fc.infra.das.exception.PersistenceException;

public class TransferCurrencyAllow implements IPersistenceObject {
		private static final long serialVersionUID = -4960596856991625778L;

	
		private String affiliateCode;
		private String drCcy;
		private String crCcy;
		private String transactionType;
		private Date createdDate;
		private Date modifiedDate;
		private String authStatus;
		
		private TransferCurrencyAllowKey refKey;

		
		
		public void create(TransferCurrencyAllow obj) throws Exception {
			TransferCurrencyAllowRepository.getInstance().create(obj);
		}
		
		public void update(TransferCurrencyAllow obj) throws Exception {
			TransferCurrencyAllowRepository.getInstance().update(obj);
		}
		
		
		
		public boolean validateCurrencyAllow(String affCode, String transactionType, String drCcy, String crCcy) throws Exception {
			
			return TransferCurrencyAllowRepository.getInstance().validateCurrencyAllow(affCode, transactionType, drCcy, crCcy);
		}
		
		


		@Override
		public boolean isEntityReadOnly() {
			// TODO Auto-generated method stub
			return false;
		}



		@Override
		public void markReadOnly(boolean arg0) throws PersistenceException {
			// TODO Auto-generated method stub
			
		}



		public String getAffiliateCode() {
			return affiliateCode;
		}



		public void setAffiliateCode(String affiliateCode) {
			this.affiliateCode = affiliateCode;
		}



		public String getDrCcy() {
			return drCcy;
		}



		public void setDrCcy(String drCcy) {
			this.drCcy = drCcy;
		}



		public String getCrCcy() {
			return crCcy;
		}



		public void setCrCcy(String crCcy) {
			this.crCcy = crCcy;
		}



		public String getTransactionType() {
			return transactionType;
		}



		public void setTransactionType(String transactionType) {
			this.transactionType = transactionType;
		}



		public Date getCreatedDate() {
			return createdDate;
		}



		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}



		public Date getModifiedDate() {
			return modifiedDate;
		}



		public void setModifiedDate(Date modifiedDate) {
			this.modifiedDate = modifiedDate;
		}



		public String getAuthStatus() {
			return authStatus;
		}



		public void setAuthStatus(String authStatus) {
			this.authStatus = authStatus;
		}



		public TransferCurrencyAllowKey getRefKey() {
			return refKey;
		}



		public void setRefKey(TransferCurrencyAllowKey refKey) {
			this.refKey = refKey;
		}

}
