package com.ecobank.digx.cz.app.domain.accountinquiry.entity.policy;

import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.TransferCurrencyAllow;
import com.ofss.digx.domain.payment.entity.policy.AbstractPaymentBusinessPolicy;
import com.ofss.fc.framework.domain.policy.IBusinessPolicyDTO;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.infra.validation.error.ValidationError;

public class AccountTransferBusinessPolicy extends AbstractPaymentBusinessPolicy {
	  private static final String THIS_COMPONENT_NAME = AccountTransferBusinessPolicy.class.getName();
	  
	  private static final MultiEntityLogger FORMATTER = MultiEntityLogger.getUniqueInstance();
	  
	  //private static final Logger logger = FORMATTER.getLogger(THIS_COMPONENT_NAME);
	  
	  private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");
	  
	  private AccountTransferBusinessPolicyData policyData;
	  
	  public AccountTransferBusinessPolicy() {}
	  
	  public AccountTransferBusinessPolicy(IBusinessPolicyDTO iBusinessPolicyDTO) {
	    if (iBusinessPolicyDTO instanceof AccountTransferBusinessPolicyData) {
	      this.policyData = (AccountTransferBusinessPolicyData)iBusinessPolicyDTO;
	      
	      
	    } 
	  }
	  
	  public void validatePolicy() {
		  
		  
		  //validateRegEx((PaymentBusinessPolicyData)this.paymentBusinessPolicyData, null);
	     // validatePayment((PaymentBusinessPolicyData)this.paymentBusinessPolicyData);
		  System.out.println("AccountTransferBusinessPolicy222  : " + policyData.getBeneficiaryAccountCcy());
		  System.out.println("AccountTransferBusinessPolicy AffCode: " + policyData.getAffiliateCode());
		  System.out.println("AccountTransferBusinessPolicy TranType: " + policyData.getTranType());
		  System.out.println("AccountTransferBusinessPolicy TranType: " + policyData.getBeneficiaryAccountCcy());
		  
		  //Local Currency to Foreign Currency Transfer is never allowed -  Rounding Tripping
		  String lcyCcy = digx_consulting.get(policyData.getAffiliateCode() + "_LCY", "");
		  System.out.println("AccountTransferBusinessPolicy : " + lcyCcy + " " + policyData.getSenderAccountCcy());
		  
		  if(lcyCcy.equals(policyData.getSenderAccountCcy()) && !lcyCcy.equals(policyData.getBeneficiaryAccountCcy()))
		  {
			  addValidationError(new ValidationError("accountTransferBusinessPolicy", "beneficiaryCcy", null, "DIGX_CZ_0001", new String[] { policyData.getBeneficiaryAccountCcy()  }));
		     return;
		  }
		  //For Self Transfer Sender & Receiver Customer Id must be same
		  if(policyData.getSenderCustomerId().equals("") && policyData.getSenderCustomerId().equals(policyData.getBeneficiaryCustomerId()) && !policyData.getTranType().equals("SELF"))
          {
			  addValidationError(new ValidationError("accountTransferBusinessPolicy", "", null, "DIGX_CZ_0002", new String[] { ""  }));
		      return;
          }
		   
		  try
		  {
			  TransferCurrencyAllow allowTransfer = new TransferCurrencyAllow();
			  if(!allowTransfer.validateCurrencyAllow(policyData.getAffiliateCode(), policyData.getTranType(), 
					  policyData.getSenderAccountCcy(), policyData.getBeneficiaryAccountCcy()))
			  {
				  addValidationError(new ValidationError("accountTransferBusinessPolicy", "", null, "DIGX_CZ_0003", new String[] { ""  }));
			      return;
			  }
		  }
		  catch(Exception ex)
		  {
			  ex.printStackTrace();
		  }
		  
	  }

}
