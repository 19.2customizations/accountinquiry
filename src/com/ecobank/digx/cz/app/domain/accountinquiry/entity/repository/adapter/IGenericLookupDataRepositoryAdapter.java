package com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.adapter;

import java.util.List;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.GenericLookupData;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.GenericLookupDataKey;
import com.ofss.digx.framework.domain.repository.IRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public interface IGenericLookupDataRepositoryAdapter extends IRepositoryAdapter<GenericLookupData, GenericLookupDataKey>
{
	
	 public abstract void create(GenericLookupData GenericLookupData) throws Exception;
	 
	 public GenericLookupData read(GenericLookupDataKey key) throws Exception;
	 
	 public  List<GenericLookupData> list(String affCode, String lookupCode) throws Exception;

}


