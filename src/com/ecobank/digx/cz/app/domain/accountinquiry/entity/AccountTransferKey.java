package com.ecobank.digx.cz.app.domain.accountinquiry.entity;

import com.ofss.fc.framework.domain.AbstractDomainObjectKey;

public class AccountTransferKey extends AbstractDomainObjectKey {
	private static final long serialVersionUID = -4865985769527653277L;
	private String externalRefNo;
	
	private String determinantValue;

	@Override
	public String keyAsString() {
		// TODO Auto-generated method stub
		return getExternalRefNo();
	}

	public String getExternalRefNo() {
		return externalRefNo;
	}

	public void setExternalRefNo(String externalRefNo) {
		this.externalRefNo = externalRefNo;
	}

	public String getDeterminantValue() {
		return determinantValue;
	}

	public void setDeterminantValue(String determinantValue) {
		this.determinantValue = determinantValue;
	}

}
