package com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository;

import java.util.List;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.GenericLookupData;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.GenericLookupDataKey;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.adapter.IGenericLookupDataRepositoryAdapter;
import com.ofss.digx.framework.domain.repository.AbstractDomainObjectRepository;
import com.ofss.digx.framework.domain.repository.RepositoryAdapterFactory;
import com.ofss.digx.infra.exceptions.Exception;

public class GenericLookupDataRepository extends AbstractDomainObjectRepository<GenericLookupData, GenericLookupDataKey> {
	private static GenericLookupDataRepository singletonInstance;

	public static GenericLookupDataRepository getInstance() {
		if (singletonInstance == null) {
			synchronized (GenericLookupDataRepository.class) {
				if (singletonInstance == null) {
					singletonInstance = new GenericLookupDataRepository();
				}
			}
		}
		return singletonInstance;
	}

	public void create(GenericLookupData object) throws Exception {
		IGenericLookupDataRepositoryAdapter repositoryAdapter = (IGenericLookupDataRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("GENERIC_LOOKUP_DATA_REPOSITORY_ADAPTER");
		repositoryAdapter.create(object);
	}
	
	public List<GenericLookupData> list(String affiliateCode,String lookupCode) throws Exception {
		IGenericLookupDataRepositoryAdapter repositoryAdapter = (IGenericLookupDataRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("GENERIC_LOOKUP_DATA_REPOSITORY_ADAPTER");
		return repositoryAdapter.list(affiliateCode,lookupCode);
	}

	@Override
	public void delete(GenericLookupData arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public GenericLookupData read(GenericLookupDataKey key) throws Exception {
		// TODO Auto-generated method stub
		IGenericLookupDataRepositoryAdapter repositoryAdapter = (IGenericLookupDataRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("GENERIC_LOOKUP_DATA_REPOSITORY_ADAPTER");
		return repositoryAdapter.read(key);
	}

	@Override
	public void update(GenericLookupData GenericLookupData) throws Exception {
		IGenericLookupDataRepositoryAdapter repositoryAdapter = (IGenericLookupDataRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("GENERIC_LOOKUP_DATA_REPOSITORY_ADAPTER");
		 repositoryAdapter.update(GenericLookupData);
		 
		 return;

	}

}
