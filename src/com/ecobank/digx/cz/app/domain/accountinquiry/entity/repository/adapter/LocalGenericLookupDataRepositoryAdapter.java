package com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.adapter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.GenericLookupData;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.GenericLookupDataKey;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.digx.framework.domain.repository.adapter.AbstractLocalRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public class LocalGenericLookupDataRepositoryAdapter extends AbstractLocalRepositoryAdapter<GenericLookupData>
		implements IGenericLookupDataRepositoryAdapter {

	private static LocalGenericLookupDataRepositoryAdapter singletonInstance;

	public static LocalGenericLookupDataRepositoryAdapter getInstance() {
		if (singletonInstance == null) {
			synchronized (LocalGenericLookupDataRepositoryAdapter.class) {
				if (singletonInstance == null) {
					singletonInstance = new LocalGenericLookupDataRepositoryAdapter();
				}
			}
		}
		return singletonInstance;
	}

	public void create(GenericLookupData object) throws Exception {
		if (object.getRefKey() != null) {
			/*
			 * object.getPassportNumber().setDeterminantValue(
			 * DeterminantResolver.getInstance().fetchDeterminantValue(GenericLookupData.class
			 * .getName()));
			 */
		}
		super.insert(object);
	}

	public void update(GenericLookupData object) throws Exception {

		super.update(object);
		
		
	}

	@Override
	public GenericLookupData read(GenericLookupDataKey key) throws Exception {
		// TODO Auto-generated method stub

		key.setDeterminantValue(
				DeterminantResolver.getInstance().fetchDeterminantValue(GenericLookupData.class.getName()));
		return (GenericLookupData) get(GenericLookupData.class, (Serializable) key);

	}

	public List<GenericLookupData> list(String affCode, String lookupCode) throws Exception {
		HashMap<String, Object> parameters = null;
		List<GenericLookupData> list = null;
		parameters = new HashMap<>();
		parameters.put("affiliateCode", affCode);
		parameters.put("lookupCode", lookupCode);
		System.out.println("Printing Fetch " + affCode + "  " + lookupCode);
		list = executeNamedQuery("FetchGenericLookupData", parameters);
		return list;
	}

}
