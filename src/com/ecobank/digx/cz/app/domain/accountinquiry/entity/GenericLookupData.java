package com.ecobank.digx.cz.app.domain.accountinquiry.entity;





import java.util.List;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.GenericLookupDataRepository;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.framework.domain.IPersistenceObject;
import com.ofss.fc.infra.das.exception.PersistenceException;

public class GenericLookupData implements IPersistenceObject {
	private static final long serialVersionUID = -4960596856991625778L;

	private String dataCode;
	private String dataName;
	private String dataDesc;
	private String affiliateCode;
	private String lookupCode;
	
	private String authStatus;
	
	
	
	private GenericLookupDataKey refKey;
	
	
	public void create(GenericLookupData transfer) throws Exception {
		GenericLookupDataRepository.getInstance().create(transfer);
	}
	
	public void update(GenericLookupData transfer) throws Exception {
		GenericLookupDataRepository.getInstance().update(transfer);
	}
	
	public List<GenericLookupData> list(String affiliateCode, String lookupCode) throws Exception {
		
		return GenericLookupDataRepository.getInstance().list(affiliateCode,lookupCode);
	}
	
	
	

	@Override
	public boolean isEntityReadOnly() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void markReadOnly(boolean arg0) throws PersistenceException {
		// TODO Auto-generated method stub
		
	}

	public String getDataCode() {
		return dataCode;
	}

	public void setDataCode(String dataCode) {
		this.dataCode = dataCode;
	}

	public String getDataName() {
		return dataName;
	}

	public void setDataName(String dataName) {
		this.dataName = dataName;
	}

	public String getDataDesc() {
		return dataDesc;
	}

	public void setDataDesc(String dataDesc) {
		this.dataDesc = dataDesc;
	}

	public String getAffiliateCode() {
		return affiliateCode;
	}

	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}

	public String getLookupCode() {
		return lookupCode;
	}

	public void setLookupCode(String lookupCode) {
		this.lookupCode = lookupCode;
	}

	public String getAuthStatus() {
		return authStatus;
	}

	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}

	public GenericLookupDataKey getRefKey() {
		return refKey;
	}

	public void setRefKey(GenericLookupDataKey refKey) {
		this.refKey = refKey;
	}

	
	

}
