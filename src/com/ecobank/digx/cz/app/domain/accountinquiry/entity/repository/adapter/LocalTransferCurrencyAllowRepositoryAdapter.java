package com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import com.ecobank.digx.cz.app.domain.accountinquiry.entity.AccountTransfer;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.TransferCurrencyAllow;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.TransferCurrencyAllowKey;
import com.ofss.digx.framework.domain.repository.adapter.AbstractLocalRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public class LocalTransferCurrencyAllowRepositoryAdapter extends AbstractLocalRepositoryAdapter<TransferCurrencyAllow>
implements ITransferCurrencyAllowRepositoryAdapter
{

	  private static LocalTransferCurrencyAllowRepositoryAdapter singletonInstance;
	  
	  public static LocalTransferCurrencyAllowRepositoryAdapter getInstance()
	  {
	    if (singletonInstance == null) {
	      synchronized (LocalTransferCurrencyAllowRepositoryAdapter.class)
	      {
	        if (singletonInstance == null) {
	          singletonInstance = new LocalTransferCurrencyAllowRepositoryAdapter();
	        }
	      }
	    }
	    return singletonInstance;
	  }
	  
	  
	  public void create(TransferCurrencyAllow object)
			    throws Exception
			  {
			    if (object.getRefKey() != null) {
			     /* object.getPassportNumber().setDeterminantValue(
			        DeterminantResolver.getInstance().fetchDeterminantValue(AccountTransfer.class.getName()));*/
			    }
			    super.insert(object);
			  }


	@Override
	public TransferCurrencyAllow read(TransferCurrencyAllowKey key) throws Exception {
		// TODO Auto-generated method stub
		return read(key);
	}
	
	public boolean validateCurrencyAllow(String affCode, String transactionType, String drCcy, String crCcy) throws Exception {
	    HashMap<String, Object> parameters = null;
	    List<TransferCurrencyAllow> list = null;
	    boolean isValid = false;
	    parameters = new HashMap<>();
	    parameters.put("affiliateCode", affCode);
	    parameters.put("transactionType", transactionType);
	    parameters.put("drCcy", drCcy);
	    parameters.put("crCcy", crCcy);
	    list = executeNamedQuery("ValidateTransferCurrencyAllow", parameters);
	    if(list != null && list.size() > 0)
	    	isValid = true;
	    
	    return isValid;
	  }
	
	
	

	
			  
}


