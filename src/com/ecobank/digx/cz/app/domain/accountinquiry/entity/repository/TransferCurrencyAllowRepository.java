package com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository;


import com.ecobank.digx.cz.app.domain.accountinquiry.entity.TransferCurrencyAllow;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.TransferCurrencyAllowKey;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.adapter.ITransferCurrencyAllowRepositoryAdapter;
import com.ofss.digx.framework.domain.repository.AbstractDomainObjectRepository;
import com.ofss.digx.framework.domain.repository.RepositoryAdapterFactory;
import com.ofss.digx.infra.exceptions.Exception;

public class TransferCurrencyAllowRepository extends AbstractDomainObjectRepository<TransferCurrencyAllow, TransferCurrencyAllowKey> {
	private static TransferCurrencyAllowRepository singletonInstance;

	public static TransferCurrencyAllowRepository getInstance() {
		if (singletonInstance == null) {
			synchronized (TransferCurrencyAllowRepository.class) {
				if (singletonInstance == null) {
					singletonInstance = new TransferCurrencyAllowRepository();
				}
			}
		}
		return singletonInstance;
	}

	public void create(TransferCurrencyAllow object) throws Exception {
		ITransferCurrencyAllowRepositoryAdapter repositoryAdapter = (ITransferCurrencyAllowRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("TRANSFER_CURRENCY_ALLOW_REPOSITORY_ADAPTER");
		repositoryAdapter.create(object);
	}
	
	public boolean validateCurrencyAllow(String affCode, String transactionType, String drCcy, String crCcy) throws Exception {
		ITransferCurrencyAllowRepositoryAdapter repositoryAdapter = (ITransferCurrencyAllowRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("TRANSFER_CURRENCY_ALLOW_REPOSITORY_ADAPTER");
		return repositoryAdapter.validateCurrencyAllow(affCode, transactionType, drCcy, crCcy);
	}

	@Override
	public void delete(TransferCurrencyAllow arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public TransferCurrencyAllow read(TransferCurrencyAllowKey key) throws Exception {
		// TODO Auto-generated method stub
		ITransferCurrencyAllowRepositoryAdapter repositoryAdapter = (ITransferCurrencyAllowRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("TRANSFER_CURRENCY_ALLOW_REPOSITORY_ADAPTER");
		return repositoryAdapter.read(key);
	}

	@Override
	public void update(TransferCurrencyAllow arg0) throws Exception {
		// TODO Auto-generated method stub

	}

}
