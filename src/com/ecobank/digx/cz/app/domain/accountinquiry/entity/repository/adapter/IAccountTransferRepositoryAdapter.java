package com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.adapter;

import java.util.List;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.AccountTransfer;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.AccountTransferKey;
import com.ofss.digx.framework.domain.repository.IRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public interface IAccountTransferRepositoryAdapter extends IRepositoryAdapter<AccountTransfer, AccountTransferKey>
{
	
	 public abstract void create(AccountTransfer accountTransfer) throws Exception;
	 
	 public AccountTransfer read(AccountTransferKey key) throws Exception;
	 
	 public  List<AccountTransfer> list(String affCode) throws Exception;

}


