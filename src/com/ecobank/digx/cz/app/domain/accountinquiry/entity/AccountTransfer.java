package com.ecobank.digx.cz.app.domain.accountinquiry.entity;


import java.util.List;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.AccountTransferRepository;
import com.ecobank.digx.cz.app.domain.accountinquiry.entity.repository.adapter.IAccountTransferRepositoryAdapter;
import com.ofss.digx.framework.domain.repository.RepositoryAdapterFactory;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.framework.domain.IPersistenceObject;
import com.ofss.fc.infra.das.exception.PersistenceException;

public class AccountTransfer implements IPersistenceObject {
	private static final long serialVersionUID = -4960596856991625778L;

	private String senderAccountNo;
	private String senderName;
	private String senderAccountCcy;
	private String affiliateCode;
	private String beneficiaryAccountNo;
	private String beneficiaryName;
	private String beneficiaryAccountCcy;
	private double amount;
	private double receiveAmount;
	private double exchRate;
	private String status;

	private String responseCode;
	private String responseMessage;
	private String CBAReferenceNo;
	private Date requestDate;
	private String tranType;
	private String tranCode;
	private String userId;
	
	private String narration;
	private String ccyPair;
	private String transferWhen;
	private Date transferDate;
	

	private AccountTransferKey refKey;

	public void create(AccountTransfer transfer) throws Exception {
		AccountTransferRepository.getInstance().create(transfer);
	}
	
	public void update(AccountTransfer transfer) throws Exception {
		AccountTransferRepository.getInstance().update(transfer);
	}
	
	public List<AccountTransfer> list(String affiliateCode) throws Exception {
		
		return AccountTransferRepository.getInstance().list(affiliateCode);
	}

	
	public AccountTransfer read(AccountTransferKey key) throws Exception {
		return AccountTransferRepository.getInstance().read(key);
	}


	@Override
	public boolean isEntityReadOnly() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void markReadOnly(boolean arg0) throws PersistenceException {
		// TODO Auto-generated method stub

	}

	public String getSenderAccountNo() {
		return senderAccountNo;
	}

	public void setSenderAccountNo(String senderAccountNo) {
		this.senderAccountNo = senderAccountNo;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderAccountCcy() {
		return senderAccountCcy;
	}

	public void setSenderAccountCcy(String senderAccountCcy) {
		this.senderAccountCcy = senderAccountCcy;
	}

	public String getAffiliateCode() {
		return affiliateCode;
	}

	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}

	public String getBeneficiaryAccountNo() {
		return beneficiaryAccountNo;
	}

	public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
		this.beneficiaryAccountNo = beneficiaryAccountNo;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryAccountCcy() {
		return beneficiaryAccountCcy;
	}

	public void setBeneficiaryAccountCcy(String beneficiaryAccountCcy) {
		this.beneficiaryAccountCcy = beneficiaryAccountCcy;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(double receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public double getExchRate() {
		return exchRate;
	}

	public void setExchRate(double exchRate) {
		this.exchRate = exchRate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getCBAReferenceNo() {
		return CBAReferenceNo;
	}

	public void setCBAReferenceNo(String cBAReferenceNo) {
		CBAReferenceNo = cBAReferenceNo;
	}

	public AccountTransferKey getRefKey() {
		return refKey;
	}

	public void setRefKey(AccountTransferKey refKey) {
		this.refKey = refKey;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getTranType() {
		return tranType;
	}

	public void setTranType(String tranType) {
		this.tranType = tranType;
	}

	public String getTranCode() {
		return tranCode;
	}

	public void setTranCode(String tranCode) {
		this.tranCode = tranCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public String getCcyPair() {
		return ccyPair;
	}

	public void setCcyPair(String ccyPair) {
		this.ccyPair = ccyPair;
	}

	public String getTransferWhen() {
		return transferWhen;
	}

	public void setTransferWhen(String transferWhen) {
		this.transferWhen = transferWhen;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}
	
	

}
