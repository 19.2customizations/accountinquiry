package com.ecobank.digx.cz.extsystem.business.extsystems.adapters.ubs;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.util.ArrayList;

import com.ecobank.digx.cz.extsystem.dto.ForexRateRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.ForexRateResponseHostDTO;
import com.ofss.extsystem.business.extsystems.HostAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterConstants;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.dto.ResponseDTO;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCEngine;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCResultSet;


public class ForexRateInquiryHostAdapter implements HostAdapter, HostAdapterConstants {
	private static final String SEL_FETCH_RATE = "SELECT DISTINCT BUY_RATE,SALE_RATE, MID_RATE,RATE_FLAG, CCY1 " +
                " FROM FCC_CYTM_DERIVED_RATES WHERE RATE_TYPE = 'STANDARD' " + 
			    " AND  CCY1 = ? AND CCY2 = ? AND BRANCH_CODE = ? ";

	public HostResponseDTO processRequest(HostRequestDTO p_request) throws Exception {
		HostResponseDTO l_hresp = new HostResponseDTO();
		ForexRateRequestHostDTO request = null;
		ForexRateResponseHostDTO response = null;
		request = (ForexRateRequestHostDTO) p_request.hostRequest;
		request.userContext = p_request.request.userContext;
		response = new ForexRateResponseHostDTO();
		Connection l_con = null;
		l_con = p_request.txnContext.getConnection("A1", p_request.idEntity, true);
		JDBCResultSet l_rs = null;
		ArrayList<Object> l_args = new ArrayList();
		StringBuffer l_query = new StringBuffer(SEL_FETCH_RATE);
		//boolean argumentAdded = true;
		/*if (!AppHelper.isNullOrBlank(l_request.city)) {
			l_query.append(" where branch_addr3 = ? ");
			l_args.add(l_request.city);
			argumentAdded = true;
		}*/
		
		l_args.add(request.baseCcy);
		l_args.add(request.quoteCcy);
		l_args.add(request.branchCode);
		
		String cc1 = request.baseCcy;
		
		response.baseCcy = request.baseCcy;
    	response.quoteCcy = request.quoteCcy;
		
		l_rs = JDBCEngine.executeQuery(l_query.toString(), l_args.size(), l_args, l_con);
		if (l_rs.next()) {
			
		
            String rFlag = l_rs.getString("RATE_FLAG");
            String ccy1 = l_rs.getString("CCY1");
            if (rFlag.equals("D") && ccy1.equals(cc1)) {
            	
            	response.buyRate = l_rs.getDouble("BUY_RATE");
            	response.midRate = l_rs.getDouble("MID_RATE");
            	response.sellRate = l_rs.getDouble("SALE_RATE");
               
            } else {
            	response.buyRate = new BigDecimal(1 / l_rs.getDouble("BUY_RATE")).setScale(6, RoundingMode.CEILING).doubleValue();
            	response.midRate = new BigDecimal(1 / l_rs.getDouble("MID_RATE")).setScale(6, RoundingMode.CEILING).doubleValue();
            	response.sellRate = new BigDecimal(1 / l_rs.getDouble("SALE_RATE")).setScale(6, RoundingMode.CEILING).doubleValue();
                
                
            }
            
			
			
		}
		

		l_hresp.hostResponse = (ResponseDTO) response;
		response.result.returnCode = 0;
		return l_hresp;
	}
	
	
	
}