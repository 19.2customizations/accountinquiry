package com.ecobank.digx.cz.extsystem.business.extsystems.adapters.ubs;


import java.sql.Connection;
import java.util.ArrayList;

import com.ecobank.digx.cz.extsystem.dto.RelationshipManagerInquiryRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.RelationshipManagerInquiryResponseHostDTO;
import com.ofss.extsystem.business.extsystems.HostAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterConstants;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.dto.ResponseDTO;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCEngine;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCResultSet;


public class RelationshipManagerInquiryHostAdapter implements HostAdapter, HostAdapterConstants {
	//private static final String SEL_FETCH_ACCOUNT_NAME = "select acctdesc from fcc_vw_mstaccount a WHERE a.idaccount = ? ";
	
	private static final String SEL_CUSTOMER_DETAIL = "select a.mis_code, code_desc from fcc_gltms_mis_code a,fcc_mitbs_class_mapping b where a.mis_code = b.comp_mis_1 and mis_class='ACC_OFCR' and unit_ref_no = ?";
	
	
	public HostResponseDTO processRequest(HostRequestDTO p_request) throws Exception {
		HostResponseDTO l_hresp = new HostResponseDTO();
		RelationshipManagerInquiryRequestHostDTO request = null;
		RelationshipManagerInquiryResponseHostDTO response = null;
		request = (RelationshipManagerInquiryRequestHostDTO) p_request.hostRequest;
		request.userContext = p_request.request.userContext;
		response = new RelationshipManagerInquiryResponseHostDTO();
		Connection l_con = null;
		l_con = p_request.txnContext.getConnection("A1", p_request.idEntity, true);
		JDBCResultSet l_rs = null;
		ArrayList<Object> l_args = new ArrayList();
		StringBuffer l_query = null;
		l_query = new StringBuffer(SEL_CUSTOMER_DETAIL);
		//boolean argumentAdded = true;
		/*if (!AppHelper.isNullOrBlank(l_request.city)) {
			l_query.append(" where branch_addr3 = ? ");
			l_args.add(l_request.city);
			argumentAdded = true;
		}*/
		
		l_args.add(request.accountNo);
		
		l_rs = JDBCEngine.executeQuery(l_query.toString(), l_args.size(), l_args, l_con);
		if (l_rs.next()) {
			if(l_rs.getString("code_desc") != null)
			{
			   response.officername = l_rs.getString("code_desc");
			   response.officercode =  l_rs.getString("mis_code");
			   response.email = "";
			   response.mobileNo ="";
			  
			}
			else
				response.officername ="";
			
		}
		

		l_hresp.hostResponse = (ResponseDTO) response;
		response.result.returnCode = 0;
		return l_hresp;
	}
	
	
	
}