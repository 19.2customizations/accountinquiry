package com.ecobank.digx.cz.extsystem.business.extsystems.adapters.ubs;


import java.sql.Connection;
import java.util.ArrayList;

import com.ecobank.digx.cz.extsystem.dto.RMDetailInquiryRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.RelationshipManagerInquiryRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.RelationshipManagerInquiryResponseHostDTO;
import com.ofss.extsystem.business.extsystems.HostAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterConstants;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.dto.ResponseDTO;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCEngine;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCResultSet;


public class RelationshipManagerDetailHostAdapter implements HostAdapter, HostAdapterConstants {
	
	private static final String SEL_QUERY = "select CODE, NAME, EMAIL, MOBILE_NO FROM DIGX_CZ_RM_DETAIL " +
	                                  " WHERE CODE = ? AND AFFILIATE_CODE = ? ";
			
	
	
	public HostResponseDTO processRequest(HostRequestDTO p_request) throws Exception {
		HostResponseDTO l_hresp = new HostResponseDTO();
		RMDetailInquiryRequestHostDTO request = null;
		RelationshipManagerInquiryResponseHostDTO response = null;
		request = (RMDetailInquiryRequestHostDTO) p_request.hostRequest;
		request.userContext = p_request.request.userContext;
		response = new RelationshipManagerInquiryResponseHostDTO();
		Connection l_con = null;
		l_con = p_request.txnContext.getConnection("A1", p_request.idEntity, true);
		JDBCResultSet l_rs = null;
		ArrayList<Object> l_args = new ArrayList();
		StringBuffer l_query = null;
		l_query = new StringBuffer(SEL_QUERY);
		//boolean argumentAdded = true;
		/*if (!AppHelper.isNullOrBlank(l_request.city)) {
			l_query.append(" where branch_addr3 = ? ");
			l_args.add(l_request.city);
			argumentAdded = true;
		}*/
		
		l_args.add(request.code);
		l_args.add(request.affiliateCode);
		
		l_rs = JDBCEngine.executeQuery(l_query.toString(), l_args.size(), l_args, l_con);
		if (l_rs.next()) {
			if(l_rs.getString("CODE") != null)
			{
			   response.officername = l_rs.getString("NAME");
			   response.officercode =  l_rs.getString("CODE");
			   response.email = l_rs.getString("EMAIL");
			   response.mobileNo = l_rs.getString("MOBILE_NO");
			  
			}
			else
				response.officername ="";
			
		}
		

		l_hresp.hostResponse = (ResponseDTO) response;
		response.result.returnCode = 0;
		return l_hresp;
	}
	
	
	
}