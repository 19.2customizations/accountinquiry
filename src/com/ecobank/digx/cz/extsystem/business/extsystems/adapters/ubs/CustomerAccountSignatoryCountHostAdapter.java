package com.ecobank.digx.cz.extsystem.business.extsystems.adapters.ubs;


import java.sql.Connection;
import java.util.ArrayList;

import com.ecobank.digx.cz.extsystem.dto.AccountInquiryRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.AccountInquiryResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.RelationshipManagerInquiryResponseHostDTO;
import com.ofss.extsystem.business.extsystems.HostAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterConstants;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.dto.ResponseDTO;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCEngine;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCResultSet;


public class CustomerAccountSignatoryCountHostAdapter implements HostAdapter, HostAdapterConstants {
	private static final String SEL_QUERY = "SELECT COUNT(*) VOLX FROM fcc_svtm_acc_sig_det WHERE ACC_NO = ? ";
	
	public HostResponseDTO processRequest(HostRequestDTO p_request) throws Exception {
		HostResponseDTO l_hresp = new HostResponseDTO();
		AccountInquiryRequestHostDTO request = null;
		AccountInquiryResponseHostDTO response = null;
		request = (AccountInquiryRequestHostDTO) p_request.hostRequest;
		request.userContext = p_request.request.userContext;
		response = new AccountInquiryResponseHostDTO();
		Connection l_con = null;
		l_con = p_request.txnContext.getConnection("A1", p_request.idEntity, true);
		JDBCResultSet l_rs = null;
		ArrayList<Object> l_args = new ArrayList();
		StringBuffer l_query = null;
		l_query = new StringBuffer(SEL_QUERY);
		//boolean argumentAdded = true;
		/*if (!AppHelper.isNullOrBlank(l_request.city)) {
			l_query.append(" where branch_addr3 = ? ");
			l_args.add(l_request.city);
			argumentAdded = true;
		}*/
		
		l_args.add(request.accNo);
		
		l_rs = JDBCEngine.executeQuery(l_query.toString(), l_args.size(), l_args, l_con);
		if (l_rs.next()) {
			if(l_rs.getString("VOLX") != null)
			{
			   response.signatoryCount = l_rs.getInt("VOLX");
			  
			  
			}
			else
				response.signatoryCount = 0;
			
		}
		

		l_hresp.hostResponse = (ResponseDTO) response;
		response.result.returnCode = 0;
		return l_hresp;
	}
	
	
	
}