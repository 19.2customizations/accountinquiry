package com.ecobank.digx.cz.extsystem.business.extsystems.adapters.ubs;


import java.sql.Connection;
import java.util.ArrayList;

import com.ecobank.digx.cz.extsystem.dto.AccountInquiryRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.AccountInquiryResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.CustomerInfoRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.CustomerInfoResponseHostDTO;
import com.ofss.extsystem.business.extsystems.HostAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterConstants;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.dto.ResponseDTO;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCEngine;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCResultSet;


public class CustomerInquiryHostAdapter implements HostAdapter, HostAdapterConstants {
	//private static final String SEL_FETCH_ACCOUNT_NAME = "select acctdesc from fcc_vw_mstaccount a WHERE a.idaccount = ? ";
	
	private static final String SEL_CUSTOMER_DETAIL = "SELECT CUSTOMER_NO, CUSTOMER_TYPE, CUSTOMER_NAME1, UDF_5 PHONE_NO, UDF_2 FROM FCC_STTMS_CUSTOMER WHERE CUSTOMER_NO =? ";
	
	
	public HostResponseDTO processRequest(HostRequestDTO p_request) throws Exception {
		HostResponseDTO l_hresp = new HostResponseDTO();
		CustomerInfoRequestHostDTO request = null;
		CustomerInfoResponseHostDTO response = null;
		request = (CustomerInfoRequestHostDTO) p_request.hostRequest;
		request.userContext = p_request.request.userContext;
		response = new CustomerInfoResponseHostDTO();
		Connection l_con = null;
		l_con = p_request.txnContext.getConnection("A1", p_request.idEntity, true);
		JDBCResultSet l_rs = null;
		ArrayList<Object> l_args = new ArrayList();
		StringBuffer l_query = null;
		l_query = new StringBuffer(SEL_CUSTOMER_DETAIL);
		//boolean argumentAdded = true;
		/*if (!AppHelper.isNullOrBlank(l_request.city)) {
			l_query.append(" where branch_addr3 = ? ");
			l_args.add(l_request.city);
			argumentAdded = true;
		}*/
		
		l_args.add(request.customerId);
		
		l_rs = JDBCEngine.executeQuery(l_query.toString(), l_args.size(), l_args, l_con);
		if (l_rs.next()) {
			if(l_rs.getString("AC_DESC") != null)
			{
			   response.customerName = l_rs.getString("CUSTOMER_NAME1");
			   response.customerType = l_rs.getString("CUSTOMER_TYPE");
			   response.udf2 = l_rs.getString("UDF_2");
			  
			}
			
			
		}
		

		l_hresp.hostResponse = (ResponseDTO) response;
		response.result.returnCode = 0;
		return l_hresp;
	}
	
	
	
}