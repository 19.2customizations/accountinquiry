package com.ecobank.digx.cz.extsystem.business.extsystems.adapters.ubs;


import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.ecobank.digx.cz.extsystem.dto.AccountDetailResponseHost;
import com.ecobank.digx.cz.extsystem.dto.AccountInquiryRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.AccountListResponseHostDTO;
import com.ofss.extsystem.business.extsystems.HostAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterConstants;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.dto.ResponseDTO;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCEngine;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCResultSet;


public class AccountInquiryListByCustIdHostAdapter implements HostAdapter, HostAdapterConstants {
	//private static final String SEL_FETCH_ACCOUNT_NAME = "select acctdesc from fcc_vw_mstaccount a WHERE a.idaccount = ? ";
	
	private static final String SEL_FETCH_ACCOUNT_NAME = "SELECT BRANCH_CODE, CUST_AC_NO, AC_DESC, CUST_NO,CCY,ACCOUNT_CLASS, " +
	   " ALT_AC_NO FROM FCC_STTMS_CUST_ACCOUNT WHERE CUST_NO = ? AND RECORD_STAT != 'C' ";
	
	

	public HostResponseDTO processRequest(HostRequestDTO p_request) throws Exception {
		HostResponseDTO l_hresp = new HostResponseDTO();
		AccountInquiryRequestHostDTO request = null;
		AccountListResponseHostDTO response = new AccountListResponseHostDTO();
		List<AccountDetailResponseHost> list = new ArrayList<AccountDetailResponseHost>();
		request = (AccountInquiryRequestHostDTO) p_request.hostRequest;
		request.userContext = p_request.request.userContext;
		response = new AccountListResponseHostDTO();
		Connection l_con = null;
		l_con = p_request.txnContext.getConnection("A1", p_request.idEntity, true);
		JDBCResultSet l_rs = null;
		ArrayList<Object> l_args = new ArrayList();
		StringBuffer l_query = null;
		l_query = new StringBuffer(SEL_FETCH_ACCOUNT_NAME);
		//boolean argumentAdded = true;
		/*if (!AppHelper.isNullOrBlank(l_request.city)) {
			l_query.append(" where branch_addr3 = ? ");
			l_args.add(l_request.city);
			argumentAdded = true;
		}*/
		
		l_args.add(request.customerNo);
		
		l_rs = JDBCEngine.executeQuery(l_query.toString(), l_args.size(), l_args, l_con);
		while (l_rs.next()) {
			if(l_rs.getString("AC_DESC") != null)
			{
				AccountDetailResponseHost response1 = new AccountDetailResponseHost();
				response1.accountName = l_rs.getString("AC_DESC");
			   response1.ccyCode = l_rs.getString("CCY");
			   response1.branchCode = l_rs.getString("BRANCH_CODE");
			   response1.accountNo = l_rs.getString("CUST_AC_NO");
			   response1.customerId = l_rs.getString("CUST_NO");
			   response1.accountClass = l_rs.getString("ACCOUNT_CLASS");
			   list.add(response1);
			   
			}
			
			
		}
		
		response.accounts = list;
		

		l_hresp.hostResponse = (ResponseDTO) response;
		response.result.returnCode = 0;
		return l_hresp;
	}
	
	
	
}