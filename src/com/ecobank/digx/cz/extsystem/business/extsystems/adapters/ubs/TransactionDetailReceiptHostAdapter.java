package com.ecobank.digx.cz.extsystem.business.extsystems.adapters.ubs;


import java.sql.Connection;
import java.util.ArrayList;

import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.TransactionDetailRequestHostDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.TransactionDetailResponseHostDTO;
import com.ofss.extsystem.business.extsystems.HostAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterConstants;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.dto.ResponseDTO;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCEngine;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCResultSet;


public class TransactionDetailReceiptHostAdapter implements HostAdapter, HostAdapterConstants {
	
	private static final String SEL_QUERY = "SELECT TRN_REF_NO,GRP_REF_NO FROM FCC_CAVW_ENTRIES WHERE AC_NO = ? AND EXTERNAL_REF_NO = ? AND SOURCE_CODE = ? ";
	
	
	public HostResponseDTO processRequest(HostRequestDTO p_request) throws Exception {
		HostResponseDTO l_hresp = new HostResponseDTO();
		TransactionDetailRequestHostDTO request = null;
		TransactionDetailResponseHostDTO response = null;
		request = (TransactionDetailRequestHostDTO) p_request.hostRequest;
		request.userContext = p_request.request.userContext;
		response = new TransactionDetailResponseHostDTO();
		Connection l_con = null;
		l_con = p_request.txnContext.getConnection("A1", p_request.idEntity, true);
		JDBCResultSet l_rs = null;
		ArrayList<Object> l_args = new ArrayList();
		StringBuffer l_query = null;
		l_query = new StringBuffer(SEL_QUERY);
		//boolean argumentAdded = true;
		/*if (!AppHelper.isNullOrBlank(l_request.city)) {
			l_query.append(" where branch_addr3 = ? ");
			l_args.add(l_request.city);
			argumentAdded = true;
		}*/
		
		l_args.add(request.accountNo);
		l_args.add(request.externalRefNo);
		l_args.add(request.sourceCode);
		
		l_rs = JDBCEngine.executeQuery(l_query.toString(), l_args.size(), l_args, l_con);
		if (l_rs.next()) {
			if(l_rs.getString("TRN_REF_NO") != null)
			{
			   response.cbaReferenceNo = l_rs.getString("TRN_REF_NO");
			   response.grpReferenceNo = l_rs.getString("GRP_REF_NO");
			  
			}
			else
				response.cbaReferenceNo ="";
			
		}
		

		l_hresp.hostResponse = (ResponseDTO) response;
		response.result.returnCode = 0;
		return l_hresp;
	}
	
	
	
}