package com.ecobank.digx.cz.extsystem.business.extsystems.adapters.ubs;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;

import com.ecobank.digx.cz.extsystem.dto.AccountDetailResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.AccountInquiryRequestHostDTO;
import com.ofss.extsystem.business.extsystems.HostAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterConstants;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.dto.ResponseDTO;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCEngine;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCResultSet;


public class AccountDetailHostAdapter implements HostAdapter, HostAdapterConstants {
	//private static final String SEL_FETCH_ACCOUNT_NAME = "select acctdesc from fcc_vw_mstaccount a WHERE a.idaccount = ? ";
	
	private static final String SEL_FETCH_ACCOUNT_NAME = "SELECT AC_NO, ACCOUNT_NAME, BRANCH_CODE, CUST_NO, CCY, ACCOUNT_CLASS, ALT_AC_NO, PND, PNC," +
			" BLOCKED_STATUS, DORMANT, FROZEN, CUST_FROZEN, AUTH_STAT1, AUTH_STAT2, RECORD_STAT, AVL_BAL, CUR_BAL, ACCOUNT_TYPE, " +
			" CUSTOMER_TYPE, UNIQUE_ID_NAME, UNIQUE_ID_VALUE, EMAIL1, PHONE1, " +
			" BVN, BBAN, TELEPHONE, EMAIL, ADDRESS, CITY, COUNTRY, OPEN_BAL, ACCOUNT_OFFICER, AFFILIATE_CODE, " +
			" TO_CHAR(DOB,'DD-MM-YYYY') DOB, CUSTOMER_NAME " +
			" FROM FCAT_VW_CZ_ACCOUNT_DETAIL WHERE AC_NO = ?  AND AFFILIATE_CODE = ? AND RECORD_STAT = 'O' ";
	
	public HostResponseDTO processRequest(HostRequestDTO p_request) throws Exception {
		HostResponseDTO l_hresp = new HostResponseDTO();
		AccountInquiryRequestHostDTO request = null;
		AccountDetailResponseHostDTO response = null;
		
		 
		 
		request = (AccountInquiryRequestHostDTO) p_request.hostRequest;
		request.userContext = p_request.request.userContext;
		response = new AccountDetailResponseHostDTO();
		
		System.out.println("Account-Fetch-Detail-1 " + request.accNo);
		
		Connection l_con = null;
		l_con = p_request.txnContext.getConnection("A1", p_request.idEntity, true);
		JDBCResultSet l_rs = null;
		ArrayList<Object> l_args = new ArrayList();
		StringBuffer l_query = null;
		l_query = new StringBuffer(SEL_FETCH_ACCOUNT_NAME);
		//boolean argumentAdded = true;
		/*if (!AppHelper.isNullOrBlank(l_request.city)) {
			l_query.append(" where branch_addr3 = ? ");
			l_args.add(l_request.city);
			argumentAdded = true;
		}*/
		
		l_args.add(request.accNo);
		l_args.add(request.affiliateCode);
		
		 System.out.println("Account-Fetch-Detail " + request.affiliateCode + " " + request.accNo);
		   
		
		l_rs = JDBCEngine.executeQuery(l_query.toString(), l_args.size(), l_args, l_con);
		if (l_rs.next()) {
			if(l_rs.getString("ACCOUNT_NAME") != null)
			{
			   response.accountName = l_rs.getString("ACCOUNT_NAME");
			   response.ccyCode = l_rs.getString("CCY");
			   response.branchCode = l_rs.getString("BRANCH_CODE");
			   response.accountNo = l_rs.getString("AC_NO");
			   response.customerId = l_rs.getString("CUST_NO");
			   response.accountClass = l_rs.getString("ACCOUNT_CLASS");
			   response.altAcoountNo = l_rs.getString("ALT_AC_NO");
			   response.pnd = l_rs.getString("PND");
			   response.pnc = l_rs.getString("PNC");
			   response.blockedStatus = l_rs.getString("BLOCKED_STATUS");
			   response.dormant = l_rs.getString("DORMANT");
			   response.frozen = l_rs.getString("FROZEN");
			   
			   response.availableBalance = new BigDecimal(l_rs.getDouble("AVL_BAL"));
			   response.currentBalance = new BigDecimal(l_rs.getDouble("CUR_BAL"));
			   
			   System.out.println("Account Balance 222 " + response.availableBalance + " " + request.accNo);
			   System.out.println("Account-Fetch-Detail-frozen " + response.frozen);
			   
			   response.bban =  l_rs.getString("BBAN");
			   response.identityNo =  l_rs.getString("UNIQUE_ID_VALUE");
			   response.identityType =  l_rs.getString("UNIQUE_ID_NAME");
			   response.customerType =  l_rs.getString("CUSTOMER_TYPE");
			   
			   if(l_rs.getString("DOB") != null)
				   response.dateOfBirth = l_rs.getString("DOB");
			   
			   if(l_rs.getString("CUSTOMER_NAME") != null)
			   {
				   response.customerName = l_rs.getString("CUSTOMER_NAME");
				   
				   response.firstname = "";
				   response.lastname = "";
				   response.middlename = "";
			   
			        String[] names = tokenize(response.customerName,",");
			        if(names != null && names.length > 0)
			        {
			        	response.firstname = names[0];
			        	if(names.length >= 2)
			        		response.lastname = names[1];
			        	
			        	if(names.length >= 3)
			        		response.middlename = names[2];
			        }
			   }
			   
			   
			   String email = l_rs.getString("EMAIL");
			   if(email == null || email.equals(""))
			   {
				  email = l_rs.getString("EMAIL1");
			   }
			   
			   String mobile = l_rs.getString("TELEPHONE");
			   if(mobile == null || mobile.equals(""))
			   {
				  mobile = l_rs.getString("PHONE1");
			   }
			   
			   response.telephone = mobile;
			   response.email = email;
			   
			}
			else
				response.accountName ="NAP";
			
		}
		

		l_hresp.hostResponse = (ResponseDTO) response;
		response.result.returnCode = 0;
		return l_hresp;
	}
	
	
	public  String[] tokenize(String input, String delim) {
        Vector v = new Vector();
        StringTokenizer t;
        //System.out.println("...TOKENIZE::" + input + "    " + delim);
        if (delim.equals("default")) {
            t = new StringTokenizer(input);
        } else {
            t = new StringTokenizer(input, delim);
        }
        for (; t.hasMoreTokens(); v.addElement(t.nextToken()));
        String cmd[] = new String[v.size()];
        for (int i = 0; i < cmd.length; i++) {
            cmd[i] = (String) v.elementAt(i);
            //System.out.println("...TOKENIZE CMD::" + cmd[i]);
        }

        return cmd;
    }
	
	
	
}