package com.ecobank.digx.cz.extsystem.dto;

import java.util.List;

import com.ofss.extsystem.dto.ResponseDTO;

public class BatchPostingResponseDTO extends ResponseDTO {
	
	
	private static final long serialVersionUID = 1L;
	
	
    private List<TransactionEntry> transactionEntries = null;
  
    private String batchNo;
    
    private String cbaReferenceNo;
    
    private String responseCode;
    
    private String responseMessage;

    public List<TransactionEntry> getTransactionEntries() {
        return transactionEntries;
    }

    public void setTransactionEntries(List<TransactionEntry> transactionEntries) {
        this.transactionEntries = transactionEntries;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getCbaReferenceNo() {
        return cbaReferenceNo;
    }

    public void setCbaReferenceNo(String cbaReferenceNo) {
        this.cbaReferenceNo = cbaReferenceNo;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

}
