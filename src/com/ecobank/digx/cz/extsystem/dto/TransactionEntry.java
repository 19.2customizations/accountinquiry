package com.ecobank.digx.cz.extsystem.dto;

import java.math.BigDecimal;

public class TransactionEntry {
	
	
	    private Integer serialNo;
	 
	    private String instrumentNo;
	    
	    private String transactionCode;
	   
	    private String cbaReferenceNo;
	   
	    private String externalRefNo;
	    
	    private String valueDate;
	    
	    private String tranDate;
	    
	    private String creditOrDebit;
	    
	    private BigDecimal amount;
	    
	    private BigDecimal lcyAmount;
	    
	    private String currencyCode;
	  
	    private String narration;
		
		
	    
	    private BigDecimal exchangeRate;
		
		
	    
	    private String costCenterCode;
	    
	    private String accountBranchCode;
	   
	    private String accountNo;
	    
	    private String customerId;
	   
	 

	    public Integer getSerialNo() {
	        return serialNo;
	    }

	    public void setSerialNo(Integer serialNo) {
	        this.serialNo = serialNo;
	    }

	    public String getInstrumentNo() {
	        return instrumentNo;
	    }

	    public void setInstrumentNo(String instrumentNo) {
	        this.instrumentNo = instrumentNo;
	    }

	    public String getTransactionCode() {
	        return transactionCode;
	    }

	    public void setTransactionCode(String transactionCode) {
	        this.transactionCode = transactionCode;
	    }

	    public String getCbaReferenceNo() {
	        return cbaReferenceNo;
	    }

	    public void setCbaReferenceNo(String cbaReferenceNo) {
	        this.cbaReferenceNo = cbaReferenceNo;
	    }

	    public String getExternalRefNo() {
	        return externalRefNo;
	    }

	    public void setExternalRefNo(String externalRefNo) {
	        this.externalRefNo = externalRefNo;
	    }

	    public String getValueDate() {
	        return valueDate;
	    }

	    public void setValueDate(String valueDate) {
	        this.valueDate = valueDate;
	    }

	    public String getTranDate() {
	        return tranDate;
	    }

	    public void setTranDate(String tranDate) {
	        this.tranDate = tranDate;
	    }

	    public String getCreditOrDebit() {
	        return creditOrDebit;
	    }

	    public void setCreditOrDebit(String creditOrDebit) {
	        this.creditOrDebit = creditOrDebit;
	    }

	    public BigDecimal getAmount() {
	        return amount;
	    }

	    public void setAmount(BigDecimal amount) {
	        this.amount = amount;
	    }

	    public BigDecimal getLcyAmount() {
	        return lcyAmount;
	    }

	    public void setLcyAmount(BigDecimal lcyAmount) {
	        this.lcyAmount = lcyAmount;
	    }

	    public String getCurrencyCode() {
	        return currencyCode;
	    }

	    public void setCurrencyCode(String currencyCode) {
	        this.currencyCode = currencyCode;
	    }

	    public String getNarration() {
	        return narration;
	    }

	    public void setNarration(String narration) {
	        this.narration = narration;
	    }

	    public BigDecimal getExchangeRate() {
	        return exchangeRate;
	    }

	    public void setExchangeRate(BigDecimal exchangeRate) {
	        this.exchangeRate = exchangeRate;
	    }

	    public String getCostCenterCode() {
	        return costCenterCode;
	    }

	    public void setCostCenterCode(String costCenterCode) {
	        this.costCenterCode = costCenterCode;
	    }

	    public String getAccountBranchCode() {
	        return accountBranchCode;
	    }

	    public void setAccountBranchCode(String accountBranchCode) {
	        this.accountBranchCode = accountBranchCode;
	    }

	    public String getAccountNo() {
	        return accountNo;
	    }

	    public void setAccountNo(String accountNo) {
	        this.accountNo = accountNo;
	    }

	    public String getCustomerId() {
	        return customerId;
	    }

	    public void setCustomerId(String customerId) {
	        this.customerId = customerId;
	    }

	  

}
