package com.ecobank.digx.cz.extsystem.dto;

import com.ofss.extsystem.dto.ResponseDTO;

public final class RelationshipManagerInquiryResponseHostDTO extends ResponseDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public String officername;
	public String mobileNo;
	public String email;
	public String branch;
	public String officercode;
	public String accountNo;
	public String accountName;
	
	
	

}
