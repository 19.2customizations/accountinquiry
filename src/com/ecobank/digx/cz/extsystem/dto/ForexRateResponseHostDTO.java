package com.ecobank.digx.cz.extsystem.dto;

import java.math.BigDecimal;

import com.ofss.extsystem.dto.ResponseDTO;

public final class ForexRateResponseHostDTO extends ResponseDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String baseCcy;
	public String quoteCcy;
	public double midRate;
	public double buyRate;
	public double sellRate;

}
