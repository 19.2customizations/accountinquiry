package com.ecobank.digx.cz.extsystem.dto;

import com.ofss.extsystem.dto.ResponseDTO;

public final class CustomerInfoResponseHostDTO extends ResponseDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public String customerName;
	public String customerType;
	public String udf2;
	

}
