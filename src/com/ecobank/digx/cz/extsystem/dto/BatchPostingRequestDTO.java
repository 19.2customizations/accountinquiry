package com.ecobank.digx.cz.extsystem.dto;

import java.util.List;

import com.ofss.extsystem.dto.RequestDTO;

public class BatchPostingRequestDTO extends RequestDTO {
	
	
	private static final long serialVersionUID = 1L;
	
    private String batchRefNo;
    
    private String productCode;
    
    private String sourceCode;
    
    private String affiliateCode;
    
    private String postingBranchCode;
    
    private List<TransactionEntry> transactionEntries = null;

    public String getBatchRefNo() {
        return batchRefNo;
    }

    public void setBatchRefNo(String batchRefNo) {
        this.batchRefNo = batchRefNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getAffiliateCode() {
        return affiliateCode;
    }

    public void setAffiliateCode(String affiliateCode) {
        this.affiliateCode = affiliateCode;
    }

    public String getPostingBranchCode() {
        return postingBranchCode;
    }

    public void setPostingBranchCode(String postingBranchCode) {
        this.postingBranchCode = postingBranchCode;
    }

    public List<TransactionEntry> getTransactionEntries() {
        return transactionEntries;
    }

    public void setTransactionEntries(List<TransactionEntry> transactionEntries) {
        this.transactionEntries = transactionEntries;
    }

}
