package com.ecobank.digx.cz.extsystem.dto;

import com.ofss.extsystem.dto.ResponseDTO;

public final class AccountDetailResponseHost  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public String accountName;
	public String accountNo;
	public String ccyCode;
	public String branchCode;
	public String accountClass;

	public String customerId;
	

}
