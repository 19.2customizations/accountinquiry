package com.ecobank.digx.cz.extsystem.dto;

import com.ofss.extsystem.dto.RequestDTO;

public final class ForexRateRequestHostDTO extends RequestDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String baseCcy;
	public String quoteCcy;
	public String branchCode;
	public String affiliateCode;
	
	
	

}
