package com.ecobank.digx.cz.extsystem.dto;

import java.math.BigDecimal;

import com.ofss.extsystem.dto.ResponseDTO;

public final class AccountDetailResponseHostDTO extends ResponseDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String accountName;
	public String accountNo;
	public String ccyCode;
	public String branchCode;
	public String accountClass;

	public String customerId;
	public String altAcoountNo;
	public String bban;
	public String pnd;
	public String pnc;
	public String blockedStatus;
	public String dormant;
	public String frozen;
	public String custFrozen;
	public String authStat1;
	public String authStat2;
	public BigDecimal availableBalance;
	public BigDecimal currentBalance;
	public BigDecimal openBalance;
	public String customerType;
	public String bvn;
	public String telephone;
	public String email;
	public String identityNo;
	public String identityType;
	public String address;
	public String affiliateCode;
	public String city;
	public String country;
	public String customerName;
	public String dateOfBirth;
	
	public String firstname;
	public String lastname;
	public String middlename;
	
	
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getCcyCode() {
		return ccyCode;
	}
	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getAccountClass() {
		return accountClass;
	}
	public void setAccountClass(String accountClass) {
		this.accountClass = accountClass;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getAltAcoountNo() {
		return altAcoountNo;
	}
	public void setAltAcoountNo(String altAcoountNo) {
		this.altAcoountNo = altAcoountNo;
	}
	public String getBban() {
		return bban;
	}
	public void setBban(String bban) {
		this.bban = bban;
	}
	public String getPnd() {
		return pnd;
	}
	public void setPnd(String pnd) {
		this.pnd = pnd;
	}
	public String getPnc() {
		return pnc;
	}
	public void setPnc(String pnc) {
		this.pnc = pnc;
	}
	public String getBlockedStatus() {
		return blockedStatus;
	}
	public void setBlockedStatus(String blockedStatus) {
		this.blockedStatus = blockedStatus;
	}
	public String getDormant() {
		return dormant;
	}
	public void setDormant(String dormant) {
		this.dormant = dormant;
	}
	public String getFrozen() {
		return frozen;
	}
	public void setFrozen(String frozen) {
		this.frozen = frozen;
	}
	public String getCustFrozen() {
		return custFrozen;
	}
	public void setCustFrozen(String custFrozen) {
		this.custFrozen = custFrozen;
	}
	public String getAuthStat1() {
		return authStat1;
	}
	public void setAuthStat1(String authStat1) {
		this.authStat1 = authStat1;
	}
	public String getAuthStat2() {
		return authStat2;
	}
	public void setAuthStat2(String authStat2) {
		this.authStat2 = authStat2;
	}
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}
	public BigDecimal getOpenBalance() {
		return openBalance;
	}
	public void setOpenBalance(BigDecimal openBalance) {
		this.openBalance = openBalance;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getBvn() {
		return bvn;
	}
	public void setBvn(String bvn) {
		this.bvn = bvn;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	

}
