package com.ecobank.digx.cz.extsystem.dto;

import com.ofss.extsystem.dto.RequestDTO;

public final class AccountInquiryRequestHostDTO extends RequestDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String accNo;
	public String customerNo;
	public boolean isExpress = false;
	public String affiliateCode;
	

}
