package com.ecobank.digx.cz.extsystem.dto;

import com.ofss.extsystem.dto.RequestDTO;

public final class CustomerInfoRequestHostDTO extends RequestDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String customerId;
	public String customerType;
	

}
