package com.ecobank.digx.cz.extsystem.dto;

import java.util.List;

import com.ofss.extsystem.dto.ResponseDTO;

public final class AccountListResponseHostDTO extends ResponseDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public List<AccountDetailResponseHost> accounts;
	

}
