package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.ecobank.digx.cz.extxface.mule.dto.AlertSMSRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.AlertSMSResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBResponseDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.domain.config.entity.ConfigVarBDomain;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.digx.framework.rest.client.RESTClientFactory;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.framework.rest.client.impl.IRESTClient;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.thoughtworks.xstream.XStream;

public class RemoteSMSAlertRepositoryAdapter {
	private static RemoteSMSAlertRepositoryAdapter singletonInstance;

	// private Preferences esbInterface =
	// ConfigurationFactory.getInstance().getConfigurations("ESBInterfaceConfig");
	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	private static final String THIS_COMPONENT_NAME = RemoteSMSAlertRepositoryAdapter.class.getName();

	private final transient MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();

	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);

	public static RemoteSMSAlertRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (RemoteSMSAlertRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new RemoteSMSAlertRepositoryAdapter();
			}
		return singletonInstance;
	}

	public ESBResponseDTO sendMessage(String mobileNo, String affCode, String message) throws Exception {
		ESBResponseDTO response = new ESBResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		// RESTClientFactory restClient = RESTClientFactory.getInstance();

		String reqID = "1";
		String requestType = "SEND";
		
		
		
		String affCode2 = digx_consulting.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
		System.out.println("SMS-Affiliate- " + affCode2);
		System.out.println("SMS-Affiliate- " + affCode + " " + mobileNo);
		
		if(mobileNo.startsWith("+"))
			mobileNo = mobileNo.substring(1);
		
		String phonePrefix = digx_consulting.get(affCode2 + "_PHONE_PREFIX", "");
		String phoneNoLen = digx_consulting.get(affCode2 + "_PHONE_NO_LENGTH", "");
		System.out.println("SMS-Affiliate- " + phonePrefix + " " + phoneNoLen);
		int len = 0;
		if(phoneNoLen != null && !phoneNoLen.equals(""))
		{
			len = Integer.parseInt(phoneNoLen);
		}
		
		
		
		if(len > 0 && len != mobileNo.length() && phonePrefix != null && !phonePrefix.equals(""))
		{
			System.out.println("SMS-Add-Prefix- " + len + " " + mobileNo);
			if(!mobileNo.startsWith(phonePrefix) && mobileNo.startsWith("0") )
			   mobileNo = phonePrefix + mobileNo.substring(1);
			else if(!mobileNo.startsWith(phonePrefix) && !mobileNo.startsWith("0") )
			   mobileNo = phonePrefix + mobileNo;
		}
		
		//

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String url = digx_consulting.get("ESB_SMS_URL", ""); //ESB_VIRTUAL_CARD_BALANCE_URL
		String clientId = digx_consulting.get("ESB_OBDX_CLIENT_ID", "");
		String secret = digx_consulting.get("ESB_OBDX_CLIENT_SECRET", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		String sourceChannel = digx_consulting.get("ESB_OBDX_SOURCE_CHANNEL", "");

		// Date startDateTime = new Date();
		// RequestHostHeaderInfo requestHostHeaderDTO =
		// this.getRequestHostHeader(serviceName);
		prop.getHeaders().put("X-sourceApplication", sourceCode);
		prop.getHeaders().put("X-affiliateCode", affCode);
		prop.getHeaders().put("X-requestId", reqID);
		prop.getHeaders().put("X-requestType", requestType);
		prop.getHeaders().put("X-ipAddress", ipx);
		prop.getHeaders().put("X-sourceChannelId", sourceChannel);
		prop.getHeaders().put("X-Client-Id", clientId);
		prop.getHeaders().put("X-Client-Secret", secret);

		// 'Content-Type: application/json
		// prop.getQueryParams().put("affiliateCode",
		// requestHostHeaderDTO.getAffiliateCode());

		AlertSMSRequestDTO requestDTO = new AlertSMSRequestDTO();
		requestDTO.setAffCode(affCode);
		requestDTO.setHeader(sourceCode);
		requestDTO.setSourceCode(sourceCode);
		requestDTO.setMessage(message);
		requestDTO.setPhonenumber(mobileNo);

		AlertSMSResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			//AlertSMSRequestDTO requestDTO2 = requestDTO;
			//if(message.length() > 10)
			//	requestDTO2.setMessage(message.substring(0,10));
			
			//System.out.println("SMS-Request : " + xs.toXML(requestDTO2));
			System.out.println("SMS-Request-URL : " + url);
			System.out.println("SMS-Request-Source Code : " + sourceCode);
			System.out.println("SMS-Request-Client-id : " + clientId + " " + secret);

			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(requestDTO);
			//System.out.println(":: send sms jsonRequestBody : " + jsonRequestBody);

			Gson gson = new Gson();

			String genResponse = HttpPost(url, requestDTO, affCode, clientId,
					secret, sourceCode);
			resp = gson.fromJson(genResponse, AlertSMSResponseDTO.class);

			// resp = gson.fromJson(MockServcie.fetchCreditCardBalanceDetails(),
			// CreditCardBalanceDetailResponseDTO.class);

			String jsonResponseBody = mapper.writeValueAsString(resp);
			System.out.println(":: sms jsonResponseBody : " + jsonResponseBody);

			// resp = (AlertSMSResponseDTO) restClient.post(prop, AlertSMSResponseDTO.class,
			// requestDTO);
			System.out.println("SMS-Response : " + xs.toXML(resp));
			if (resp != null && resp.getStatus().equals("SUCCESS")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getStatus());
				response.setTransactionRefNo(resp.getMessageid());
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			/*
			 * if
			 * ("Y".equalsIgnoreCase(this.digx_consulting.get("INTERFACE_LOGGER_REQUIRED",
			 * ""))) { serviceLogger.dbLogger(requestDTO, response, "WU_FEE_INQ", "", "",
			 * ThreadAttribute.get("SUBJECT_NAME").toString(),
			 * ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime); }
			 */

		}

		/*
		 * try {
		 * 
		 * 
		 * } catch (Exception e) { e.printStackTrace(); logger.log(Level.SEVERE,
		 * this.formatter.formatMessage(
		 * " Exception has occured while getting response object of %s inside the validatePaymentDetails method of %s for %s. Exception details are %s"
		 * , new Object[] { ESBResponseDTO.class.getName(), THIS_COMPONENT_NAME,
		 * response, e })); } finally { AdapterInteraction.close(); }
		 */

		AdapterInteraction.close();

		return response;
	}
	
	public ESBResponseDTO sendMessageWeblogic(String mobileNo, String affCode, String message) throws Exception {
		ESBResponseDTO response = new ESBResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		// RESTClientFactory restClient = RESTClientFactory.getInstance();

		String reqID = "1";
		String requestType = "SEND";

		//RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		//RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB");
		//RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		//IRESTClient restClient = clientFactory.getRESTClientInstance();

		String url = digx_consulting.get("ESB_SMS_URL", ""); //ESB_VIRTUAL_CARD_BALANCE_URL
		String clientId = digx_consulting.get("ESB_OBDX_CLIENT_ID", "");
		String secret = digx_consulting.get("ESB_OBDX_CLIENT_SECRET", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		String sourceChannel = digx_consulting.get("ESB_OBDX_SOURCE_CHANNEL", "");

		// Date startDateTime = new Date();
		// RequestHostHeaderInfo requestHostHeaderDTO =
		// this.getRequestHostHeader(serviceName);
		

		// 'Content-Type: application/json
		// prop.getQueryParams().put("affiliateCode",
		// requestHostHeaderDTO.getAffiliateCode());

		AlertSMSRequestDTO requestDTO = new AlertSMSRequestDTO();
		requestDTO.setAffCode(affCode);
		requestDTO.setHeader(sourceCode);
		requestDTO.setSourceCode(sourceCode);
		requestDTO.setMessage(message);
		requestDTO.setPhonenumber(mobileNo);

		AlertSMSResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			System.out.println("SMS-Request : " + xs.toXML(requestDTO));
			System.out.println("SMS-Request-URL : " + url);
			System.out.println("SMS-Request-Source Code : " + sourceCode);
			System.out.println("SMS-Request-Client-id : " + clientId + " " + secret);

			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(requestDTO);
			System.out.println(":: send sms jsonRequestBody : " + jsonRequestBody);

			Gson gson = new Gson();

			String genResponse = HttpPost(url, requestDTO, affCode, clientId,
					secret, sourceCode);
			resp = gson.fromJson(genResponse, AlertSMSResponseDTO.class);

			// resp = gson.fromJson(MockServcie.fetchCreditCardBalanceDetails(),
			// CreditCardBalanceDetailResponseDTO.class);

			String jsonResponseBody = mapper.writeValueAsString(resp);
			System.out.println(":: sms jsonResponseBody : " + jsonResponseBody);

			// resp = (AlertSMSResponseDTO) restClient.post(prop, AlertSMSResponseDTO.class,
			// requestDTO);
			System.out.println("SMS-Response : " + xs.toXML(resp));
			if (resp != null && resp.getStatus().equals("SUCCESS")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getStatus());
				response.setTransactionRefNo(resp.getMessageid());
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			/*
			 * if
			 * ("Y".equalsIgnoreCase(this.digx_consulting.get("INTERFACE_LOGGER_REQUIRED",
			 * ""))) { serviceLogger.dbLogger(requestDTO, response, "WU_FEE_INQ", "", "",
			 * ThreadAttribute.get("SUBJECT_NAME").toString(),
			 * ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime); }
			 */

		}

		/*
		 * try {
		 * 
		 * 
		 * } catch (Exception e) { e.printStackTrace(); logger.log(Level.SEVERE,
		 * this.formatter.formatMessage(
		 * " Exception has occured while getting response object of %s inside the validatePaymentDetails method of %s for %s. Exception details are %s"
		 * , new Object[] { ESBResponseDTO.class.getName(), THIS_COMPONENT_NAME,
		 * response, e })); } finally { AdapterInteraction.close(); }
		 */

		AdapterInteraction.close();

		return response;
	}

	public static String HttpPostOnly(String url, Object payloadRequest, String affCode,
			String clientId, String secret, String sourceCode) {

		StringBuffer resp = new StringBuffer();
		URL obj = null;
		Gson gson = new Gson();
		try {
			obj = new URL(url); //contextUrl + "/" + serviceUrl);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");

			con.setRequestProperty("X-sourceApplication", sourceCode);
			con.setRequestProperty("X-affiliateCode", affCode);
			con.setRequestProperty("X-requestId", "1");
			con.setRequestProperty("X-requestType", "SEND");
			con.setRequestProperty("X-ipAddress", "1.1.1.1");
			con.setRequestProperty("X-sourceChannelId", "OBDX");
			con.setRequestProperty("X-Client-Id", clientId);
			con.setRequestProperty("X-Client-Secret", secret);

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(payloadRequest));

			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			System.out.println("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				// System.out.println("response:" + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				// System.out.println("response 2" + resp.toString());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return resp.toString();

	}

	public static String HttpPost(String url, Object payloadRequest, String affCode,
			String clientId, String secret, String sourceCode) {

		StringBuffer resp = new StringBuffer();
		URL obj = null;
		Gson gson = new Gson();
		try {
			obj = new URL(url);

			if (url.startsWith("https")) {
				enableSSL();

			} else
				return HttpPostOnly(url, payloadRequest, affCode, clientId, secret, sourceCode);

			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("X-sourceApplication", sourceCode);
			con.setRequestProperty("X-affiliateCode", affCode);
			con.setRequestProperty("X-requestId", "1");
			con.setRequestProperty("X-requestType", "SEND");
			con.setRequestProperty("X-ipAddress", "1.1.1.1");
			con.setRequestProperty("X-sourceChannelId", "OBDX");
			con.setRequestProperty("X-Client-Id", clientId);
			con.setRequestProperty("X-Client-Secret", secret);

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(payloadRequest));

			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			System.out.println("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return resp.toString();

	}

	public static void enableSSL() {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
