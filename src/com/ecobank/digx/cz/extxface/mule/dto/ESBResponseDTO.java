package com.ecobank.digx.cz.extxface.mule.dto;

import com.ofss.digx.service.response.BaseResponseObject;

public class ESBResponseDTO extends BaseResponseObject {
	
	
	private static final long serialVersionUID = 1L;
	
	private String cbaReferenceNo;
	private String responseCode;
	private String responseMessage;
	private String transactionRefNo;
	
	
	public String getCbaReferenceNo() {
		return cbaReferenceNo;
	}
	public void setCbaReferenceNo(String cbaReferenceNo) {
		this.cbaReferenceNo = cbaReferenceNo;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	
	

}
