package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"application", "mobileno", "message", "senderid", "token", "affcode"})
public class UnifiedAlertSMSRequestDTO {
	
	
	@JsonProperty("affCode")
	private String affCode;
	
	@JsonProperty("mobileno")
	private String mobileno;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("senderid")
	private String senderid;
	
	@JsonProperty("token")
	private String token;
	
	@JsonProperty("application")
	private String application;
	
	

	public String getAffCode() {
		return affCode;
	}

	public void setAffCode(String affCode) {
		this.affCode = affCode;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSenderid() {
		return senderid;
	}

	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}
	
	
	
	

}
