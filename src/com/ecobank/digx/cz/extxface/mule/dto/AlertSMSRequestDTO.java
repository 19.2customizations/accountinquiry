package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"affCode", "phonenumber", "message", "sourceCode", "header"})
public class AlertSMSRequestDTO {
	
	@JsonProperty("affCode")
	private String affCode;
	
	@JsonProperty("phonenumber")
	private String phonenumber;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("sourceCode")
	private String sourceCode;
	
	@JsonProperty("header")
	private String header;

	public String getAffCode() {
		return affCode;
	}

	public void setAffCode(String affCode) {
		this.affCode = affCode;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}
	
	
	
	

}
