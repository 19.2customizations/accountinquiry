package com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto;

import com.ofss.digx.app.common.dto.DataTransferObject;

public class AccountDetailRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	
	private String accountNo;
	private String affiliateCode;
	private String bankCode;
	private boolean isExpressAccount;
	
	private String customerId;
	
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public boolean isExpressAccount() {
		return isExpressAccount;
	}
	public void setExpressAccount(boolean isExpressAccount) {
		this.isExpressAccount = isExpressAccount;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	

}
