package com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto;

import java.math.BigDecimal;

import com.ofss.extsystem.dto.ResponseDTO;

public final class TransactionDetailResponseHostDTO extends ResponseDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String cbaReferenceNo;
	public String grpReferenceNo;
	public String externalRefNo;
	public String sourceCode;
	public String sourceAccountNo;
	public String beneficiaryAccountNo;
	public String purpose;
	public String narration;
	public String tranDate;
	public String valueDate;
	public String beneficiaryName;
	public String bankCode;
	public String transactionType;
	public String tranCode;
	public BigDecimal amount;
	
	

}
