package com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto;

import com.ofss.digx.app.common.dto.DataTransferObject;

public class CustomerInfoRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	
	private String customerId;
	private String affiliateCode;
	private String customerType;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	
	

}
