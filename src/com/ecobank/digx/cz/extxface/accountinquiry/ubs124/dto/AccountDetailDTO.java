package com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto;

import java.math.BigDecimal;

import com.ofss.digx.service.response.BaseResponseObject;

public class AccountDetailDTO extends BaseResponseObject {
	
	private static final long serialVersionUID = 1L;
	private String accountName;
	private String accountNo;
	private String ccyCode;
	private String branchCode;
	private String accountClass;

	private String customerId;
	private String altAcoountNo;
	private String bban;
	private String pnd;
	private String pnc;
	private String blockedStatus;
	private String dormant;
	private String frozen;
	private String custFrozen;
	private String authStat1;
	private String authStat2;
	private BigDecimal availableBalance;
	private BigDecimal currentBalance;
	private BigDecimal openBalance;
	private String customerType;
	private String bvn;
	private String telephone;
	private String email;
	private String identityNo;
	private String identityType;
	private String address;
	private String affiliateCode;
	private String city;
	private String country;
	
	private String customerName;
	private String dateOfBirth;
	
	private String firstname;
	private String lastname;
	private String middlename;
	
	
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getCcyCode() {
		return ccyCode;
	}
	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getAccountClass() {
		return accountClass;
	}
	public void setAccountClass(String accountClass) {
		this.accountClass = accountClass;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getAltAcoountNo() {
		return altAcoountNo;
	}
	public void setAltAcoountNo(String altAcoountNo) {
		this.altAcoountNo = altAcoountNo;
	}
	public String getBban() {
		return bban;
	}
	public void setBban(String bban) {
		this.bban = bban;
	}
	public String getPnd() {
		return pnd;
	}
	public void setPnd(String pnd) {
		this.pnd = pnd;
	}
	public String getPnc() {
		return pnc;
	}
	public void setPnc(String pnc) {
		this.pnc = pnc;
	}
	public String getBlockedStatus() {
		return blockedStatus;
	}
	public void setBlockedStatus(String blockedStatus) {
		this.blockedStatus = blockedStatus;
	}
	public String getDormant() {
		return dormant;
	}
	public void setDormant(String dormant) {
		this.dormant = dormant;
	}
	public String getFrozen() {
		return frozen;
	}
	public void setFrozen(String frozen) {
		this.frozen = frozen;
	}
	public String getCustFrozen() {
		return custFrozen;
	}
	public void setCustFrozen(String custFrozen) {
		this.custFrozen = custFrozen;
	}
	public String getAuthStat1() {
		return authStat1;
	}
	public void setAuthStat1(String authStat1) {
		this.authStat1 = authStat1;
	}
	public String getAuthStat2() {
		return authStat2;
	}
	public void setAuthStat2(String authStat2) {
		this.authStat2 = authStat2;
	}
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}
	public BigDecimal getOpenBalance() {
		return openBalance;
	}
	public void setOpenBalance(BigDecimal openBalance) {
		this.openBalance = openBalance;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getBvn() {
		return bvn;
	}
	public void setBvn(String bvn) {
		this.bvn = bvn;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getIdentityType() {
		return identityType;
	}
	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	

}
