package com.ecobank.digx.cz.extxface.accountinquiry.ubs124.impl.assembler;

import java.util.ArrayList;
import java.util.List;

import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryListResponse;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.RMDetailInfoResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.RMDetailListResponse;
import com.ecobank.digx.cz.extsystem.dto.AccountDetailResponseHost;
import com.ecobank.digx.cz.extsystem.dto.AccountDetailResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.AccountInquiryRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.AccountInquiryResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.AccountListResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.CustomerInfoRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.CustomerInfoResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.ForexRateRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.ForexRateResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.RMDetailInquiryRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.RelationshipManagerInquiryRequestHostDTO;
import com.ecobank.digx.cz.extsystem.dto.RelationshipManagerInquiryResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.RelationshipManagerListResponseHostDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.CustomerInfoDTO;
import com.ofss.extsystem.business.extsystems.HostAdapterHelper;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.RequestDTO;
import com.ofss.extsystem.dto.UserContextDTO;

public class AccountInquiryAssembler {
	
	 public HostRequestDTO fromAdaptertoHostRequestAccountInquiry(AccountInquiryRequestDTO requestDTO) throws Exception {
		 AccountInquiryRequestHostDTO hostAccInquiryRequestDTO = new AccountInquiryRequestHostDTO();
		   
		 hostAccInquiryRequestDTO.userContext = new UserContextDTO();
		 hostAccInquiryRequestDTO.userContext.idEntity = "B001";
		 hostAccInquiryRequestDTO.userContext.refIdEntity = "B001";
		 hostAccInquiryRequestDTO.userContext.idRequest = "DIGX_CZ_ACCOUNT_INQUIRY";
		 hostAccInquiryRequestDTO.accNo = requestDTO.getAccountNo();
		 hostAccInquiryRequestDTO.isExpress = requestDTO.isExpressAccount();
		 
		 
		 return HostAdapterHelper.buildHostRequest((RequestDTO)hostAccInquiryRequestDTO);
	 }
		  
		public AccountInquiryResponseDTO  fromUBSObjectToAccountInquiryResponse(AccountInquiryResponseHostDTO response) {
			AccountInquiryResponseDTO responseDTO = new AccountInquiryResponseDTO();
			
			responseDTO.setAccountName(response.accountName);
			responseDTO.setAccountClass(response.accountClass);
			responseDTO.setAccountNo(response.accountNo);
			responseDTO.setBranchId(response.branchCode);
			responseDTO.setCcy(response.ccyCode);
			responseDTO.setCustomerId(response.customerId);
		    
		    return responseDTO;
		  }
		
		
		
		
		
		
		public HostRequestDTO fromAdaptertoHostRequestForexRate(ForexRateRequestDTO requestDTO) throws Exception {
			ForexRateRequestHostDTO hostRequestDTO = new ForexRateRequestHostDTO();
			   
			hostRequestDTO.userContext = new UserContextDTO();
			hostRequestDTO.userContext.idEntity = "B001";
			hostRequestDTO.userContext.refIdEntity = "B001";
			hostRequestDTO.userContext.idRequest = "DIGX_CZ_FOREX_RATE_INQUIRY";
			hostRequestDTO.affiliateCode = requestDTO.getAffiliateCode();
			hostRequestDTO.baseCcy = requestDTO.getBaseCcy();
			hostRequestDTO.quoteCcy = requestDTO.getQuoteCcy();
			hostRequestDTO.branchCode = requestDTO.getBranchCode();
			    return HostAdapterHelper.buildHostRequest((RequestDTO)hostRequestDTO);
			
		}
		
		/*public HostRequestDTO fromAdaptertoHostRequestTransactionDetailReceipt(ForexRateRequestDTO requestDTO) throws Exception {
			TransactionDetailRequestHostDTO hostRequestDTO = new TransactionDetailRequestHostDTO();
			   
			hostRequestDTO.userContext = new UserContextDTO();
			hostRequestDTO.userContext.idEntity = "B001";
			hostRequestDTO.userContext.refIdEntity = "B001";
			hostRequestDTO.userContext.idRequest = "DIGX_CZ_FETCH_TRANSACTION_DETAIL";
			hostRequestDTO.affiliateCode = requestDTO.getAffiliateCode();
			hostRequestDTO.baseCcy = requestDTO.getBaseCcy();
			hostRequestDTO.quoteCcy = requestDTO.getQuoteCcy();
			hostRequestDTO.branchCode = requestDTO.getBranchCode();
			    return HostAdapterHelper.buildHostRequest((RequestDTO)hostRequestDTO);
			
		}*/
		
		
		
		public HostRequestDTO fromAdaptertoHostRequestFetchRMetail(String accountNo, String affCode) throws Exception {
			RelationshipManagerInquiryRequestHostDTO hostRequestDTO = new RelationshipManagerInquiryRequestHostDTO();

			hostRequestDTO.userContext = new UserContextDTO();
			hostRequestDTO.userContext.idEntity = "B001";
			hostRequestDTO.userContext.refIdEntity = "B001";
			hostRequestDTO.userContext.idRequest = "DIGX_CZ_FETCH_RM_DETAIL";
			hostRequestDTO.accountNo = accountNo;
			
			return HostAdapterHelper.buildHostRequest((RequestDTO) hostRequestDTO);
		}
		
		public HostRequestDTO fromAdaptertoHostRequestFetchRMetailListByParty(String customerId, String affCode) throws Exception {
			RelationshipManagerInquiryRequestHostDTO hostRequestDTO = new RelationshipManagerInquiryRequestHostDTO();

			hostRequestDTO.userContext = new UserContextDTO();
			hostRequestDTO.userContext.idEntity = "B001";
			hostRequestDTO.userContext.refIdEntity = "B001";
			hostRequestDTO.userContext.idRequest = "DIGX_CZ_FETCH_RM_DETAIL_LIST_BY_PARTY";
			hostRequestDTO.customerNo = customerId;
			
			return HostAdapterHelper.buildHostRequest((RequestDTO) hostRequestDTO);
		}
		
		public HostRequestDTO fromAdaptertoHostAccountlListByParty(String customerId, String affCode) throws Exception {
			AccountInquiryRequestHostDTO hostRequestDTO = new AccountInquiryRequestHostDTO();

			hostRequestDTO.userContext = new UserContextDTO();
			hostRequestDTO.userContext.idEntity = "B001";
			hostRequestDTO.userContext.refIdEntity = "B001";
			hostRequestDTO.userContext.idRequest = "DIGX_CZ_FETCH_ACCOUNTS_LIST_BY_PARTY";
			hostRequestDTO.customerNo = customerId;
			
			return HostAdapterHelper.buildHostRequest((RequestDTO) hostRequestDTO);
		}
		
		
		public HostRequestDTO fromAdaptertoHostRequestRMetailInquiryByCode(String code, String affCode) throws Exception {
			RMDetailInquiryRequestHostDTO hostRequestDTO = new RMDetailInquiryRequestHostDTO();

			hostRequestDTO.userContext = new UserContextDTO();
			hostRequestDTO.userContext.idEntity = "B001";
			hostRequestDTO.userContext.refIdEntity = "B001";
			hostRequestDTO.userContext.idRequest = "DIGX_CZ_FETCH_RM_DETAIL_BY_CODE";
			hostRequestDTO.code = code;
			hostRequestDTO.affiliateCode = affCode;
			
			return HostAdapterHelper.buildHostRequest((RequestDTO) hostRequestDTO);
		}
		
		public HostRequestDTO fromAdaptertoHostRequestFetchAccountDetail(String accountNo, String affCode) throws Exception {
			AccountInquiryRequestHostDTO hostAccDetailRequestDTO = new AccountInquiryRequestHostDTO();

			hostAccDetailRequestDTO.userContext = new UserContextDTO();
			hostAccDetailRequestDTO.userContext.idEntity = "B001";
			hostAccDetailRequestDTO.userContext.refIdEntity = "B001";
			hostAccDetailRequestDTO.userContext.idRequest = "DIGX_CZ_FETCH_ACCOUNT_DETAIL";
			hostAccDetailRequestDTO.accNo = accountNo;
			hostAccDetailRequestDTO.affiliateCode = affCode;
			
			return HostAdapterHelper.buildHostRequest((RequestDTO) hostAccDetailRequestDTO);
			
		}
		
		public HostRequestDTO fromAdaptertoHostRequestFetchCustomerMISBusinessCode(String customerId, String affCode) throws Exception {
			AccountInquiryRequestHostDTO hostAccDetailRequestDTO = new AccountInquiryRequestHostDTO();

			hostAccDetailRequestDTO.userContext = new UserContextDTO();
			hostAccDetailRequestDTO.userContext.idEntity = "B001";
			hostAccDetailRequestDTO.userContext.refIdEntity = "B001";
			hostAccDetailRequestDTO.userContext.idRequest = "DIGX_CZ_FETCH_CUSTOMER_MIS_BUSINESS_CODE";
			hostAccDetailRequestDTO.customerNo = customerId;
			hostAccDetailRequestDTO.affiliateCode = affCode;
			
			return HostAdapterHelper.buildHostRequest((RequestDTO) hostAccDetailRequestDTO);
			
		}
		
		public HostRequestDTO fromAdaptertoHostRequestFetchAccountSignatoryCount(String accountNo, String affCode) throws Exception {
			AccountInquiryRequestHostDTO hostAccDetailRequestDTO = new AccountInquiryRequestHostDTO();

			hostAccDetailRequestDTO.userContext = new UserContextDTO();
			hostAccDetailRequestDTO.userContext.idEntity = "B001";
			hostAccDetailRequestDTO.userContext.refIdEntity = "B001";
			hostAccDetailRequestDTO.userContext.idRequest = "DIGX_CZ_FETCH_ACC_SIGNATORY_COUNT";
			hostAccDetailRequestDTO.accNo = accountNo;
			hostAccDetailRequestDTO.affiliateCode = affCode;
			
			return HostAdapterHelper.buildHostRequest((RequestDTO) hostAccDetailRequestDTO);
			
		}
		
		public HostRequestDTO fromAdaptertoHostRequestCustomerInfoHost(String customerId, String affCode) throws Exception {
			CustomerInfoRequestHostDTO hostRequestDTO = new CustomerInfoRequestHostDTO();

			hostRequestDTO.userContext = new UserContextDTO();
			hostRequestDTO.userContext.idEntity = "B001";
			hostRequestDTO.userContext.refIdEntity = "B001";
			hostRequestDTO.userContext.idRequest = "DIGX_CZ_FETCH_CUST_INFO";
			hostRequestDTO.customerId = customerId;
			
			return HostAdapterHelper.buildHostRequest((RequestDTO) hostRequestDTO);
			
		}
		
		public RMDetailInfoResponseDTO  fromUBSObjectToRMDetailResponse(RelationshipManagerInquiryResponseHostDTO response) {
			RMDetailInfoResponseDTO responseDTO = new RMDetailInfoResponseDTO();
			
			responseDTO.setName(response.officername);
			responseDTO.setCode(response.officercode);
			responseDTO.setEmail(response.email);
			responseDTO.setPhoneNo(response.mobileNo);
			
		    
		    return responseDTO;
		}
		
		public RMDetailListResponse  fromUBSObjectToRMDetailListResponse(RelationshipManagerListResponseHostDTO response) {
			RMDetailListResponse responseDTO = new RMDetailListResponse();
			
			List<RMDetailInfoResponseDTO> listRMs = new ArrayList<RMDetailInfoResponseDTO>();
			//RMDetailInfoResponseDTO response2 = new RMDetailInfoResponseDTO();
			
			if(response != null && response.listRMs.size() > 0)
			{
				for(RelationshipManagerInquiryResponseHostDTO rm : response.listRMs)
				{
					RMDetailInfoResponseDTO rmDTO = new RMDetailInfoResponseDTO();
					rmDTO.setAccountNo(rm.accountNo);
					rmDTO.setCode(rm.officercode);
					rmDTO.setName(rm.officername);
					rmDTO.setEmail(rm.email);
					rmDTO.setPhoneNo(rm.mobileNo);
					rmDTO.setAccountName(rm.accountName);
					listRMs.add(rmDTO);
					
				}
			}
			
			responseDTO.setListRMs(listRMs);
			
		    
		    return responseDTO;
		}
		
		public AccountInquiryListResponse  fromUBSObjectToAccountsListResponse( AccountListResponseHostDTO response) {
			AccountInquiryListResponse responseDTO = new AccountInquiryListResponse();
			
			List<AccountInquiryResponseDTO> listRMs = new ArrayList<AccountInquiryResponseDTO>();
			//RMDetailInfoResponseDTO response2 = new RMDetailInfoResponseDTO();
			
			if(response != null && response.accounts.size() > 0)
			{
				for(AccountDetailResponseHost rm : response.accounts)
				{
					AccountInquiryResponseDTO rmDTO = new AccountInquiryResponseDTO();
					rmDTO.setAccountNo(rm.accountNo);
					rmDTO.setAccountName(rm.accountName);
					rmDTO.setCcy(rm.ccyCode);
					rmDTO.setBranchId(rm.branchCode);
					rmDTO.setAccountClass(rm.accountClass);
					
					listRMs.add(rmDTO);
					
				}
			}
			
			responseDTO.setAccounts(listRMs);
			
		    
		    return responseDTO;
		}
	  
			  
			public ForexRateResponseDTO  fromUBSObjectToForexRateResponse(ForexRateResponseHostDTO response) {
				ForexRateResponseDTO responseDTO = new ForexRateResponseDTO();
				
				responseDTO.setBaseCcy(response.baseCcy);
				responseDTO.setQuoteCcy(response.quoteCcy);
				responseDTO.setBuyRate(response.buyRate);
				responseDTO.setSellRate(response.sellRate);
				responseDTO.setMidRate(response.midRate);
			    
			    return responseDTO;
			}
			
			public CustomerInfoDTO  fromUBSObjectToCustomerInfoResponse(CustomerInfoResponseHostDTO response) {
				CustomerInfoDTO responseDTO = new CustomerInfoDTO();
				
				responseDTO.setCustomerName(response.customerName);
				responseDTO.setUdf2(response.udf2);
				responseDTO.setCustomerType(response.customerType);
				
			    
			    return responseDTO;
			}
		  
			
			public AccountDetailDTO  fromUBSObjectToAccountDetailDTO(AccountDetailResponseHostDTO response) {
				AccountDetailDTO aDTO = new AccountDetailDTO();
				
				aDTO.setAccountClass(response.accountClass);
				aDTO.setAccountName(response.accountName);
				aDTO.setAccountNo(response.accountNo);
				aDTO.setAddress(response.address);
				aDTO.setAffiliateCode(response.affiliateCode);
				aDTO.setAltAcoountNo(aDTO.getAltAcoountNo());
				aDTO.setAvailableBalance(response.availableBalance);
				aDTO.setBban(response.bban);
				aDTO.setCurrentBalance(response.currentBalance);
				aDTO.setBvn(response.bvn);
				aDTO.setCustomerId(response.customerId);
				aDTO.setCustomerType(response.customerType);
				aDTO.setDormant(response.dormant);
				aDTO.setFrozen(response.frozen);
				aDTO.setCurrentBalance(response.currentBalance);
				aDTO.setBlockedStatus(response.blockedStatus);
				aDTO.setEmail(response.email);
				aDTO.setTelephone(response.telephone);
				aDTO.setIdentityNo(response.identityNo);
				aDTO.setIdentityType(response.identityType);
				aDTO.setPnc(response.pnc);
				aDTO.setPnd(response.pnd);
				aDTO.setCcyCode(response.ccyCode);
				aDTO.setDateOfBirth(response.dateOfBirth);
				aDTO.setCustomerName(response.customerName);
				aDTO.setFirstname(response.firstname);
				aDTO.setLastname(response.lastname);
				aDTO.setMiddlename(response.middlename);
				//aDTO.setOpenBalance(openBalance);
				
				
			    
			    return aDTO;
			}
		  

}
