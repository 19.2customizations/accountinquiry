package com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto;

import com.ofss.extsystem.dto.RequestDTO;

public final class TransactionDetailRequestHostDTO extends RequestDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String externalRefNo;
	public String accountNo;
	public String sourceCode;
	public String cbaReferenceNo;
	public String transactionType;
	
	public String fetchType;  // CBAREF or EXTREF
	
	

}
