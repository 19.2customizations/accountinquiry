package com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto;

import java.math.BigDecimal;

import com.ofss.digx.service.response.BaseResponseObject;

public class AccountMISAndSignatoryDetailDTO extends BaseResponseObject {
	
	private static final long serialVersionUID = 1L;
	private String accountName;
	private String accountNo;
	
	private int signatoryCount;
	private String misBusinessCode;
	private String customerId;
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public int getSignatoryCount() {
		return signatoryCount;
	}
	public void setSignatoryCount(int signatoryCount) {
		this.signatoryCount = signatoryCount;
	}
	public String getMisBusinessCode() {
		return misBusinessCode;
	}
	public void setMisBusinessCode(String misBusinessCode) {
		this.misBusinessCode = misBusinessCode;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	
	

}
