package com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto;

import com.ofss.digx.service.response.BaseResponseObject;

public class CustomerInfoDTO extends BaseResponseObject {
	
	
	private static final long serialVersionUID = 1L;
	private String customerId;
	private String customerName;
	private String customerType;
	private String udf2;
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getUdf2() {
		return udf2;
	}
	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}
	
	

}
