package com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryListResponse;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.RMDetailInfoRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.RMDetailInfoResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.RMDetailListResponse;
import com.ecobank.digx.cz.extsystem.dto.AccountDetailResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.AccountInquiryResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.AccountListResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.CustomerInfoResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.ForexRateResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.RelationshipManagerInquiryResponseHostDTO;
import com.ecobank.digx.cz.extsystem.dto.RelationshipManagerListResponseHostDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailRequestDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountMISAndSignatoryDetailDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.CustomerInfoDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.CustomerInfoRequestDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.impl.assembler.AccountInquiryAssembler;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.extxface.adapter.AbstractAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterManager;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.fc.app.context.SessionContext;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class AccountInquiryAdapter extends AbstractAdapter implements IAccountInquiryAdapter {
	  private static final String THIS_COMPONENT_NAME = AccountInquiryAdapter.class.getName();
	  
	  private final transient MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	  
	  private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);

	  private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");


	public AccountInquiryResponseDTO fetchAccountName(SessionContext sessionContext,AccountInquiryRequestDTO requestDTO) throws Exception {
	    
		//AccountInquiryRequestDTO requestDTO = new AccountInquiryRequestDTO();
		
		
		if (logger.isLoggable(Level.FINE))
	      logger.log(Level.FINE, "Entered into method fetch account name AccountInquiryAdapter "); 
	    checkRequest("com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.AccountInquiryAdapter.fetchAccountName", new Object[] { requestDTO });
	    AdapterInteraction.begin();
	    AccountInquiryResponseDTO responseDTO = null;
	    HostResponseDTO hostResponse = null;
	    HostRequestDTO hostRequest = null;
	    try {
	    	AccountInquiryAssembler assembler = new AccountInquiryAssembler();
	      hostRequest = assembler.fromAdaptertoHostRequestAccountInquiry(requestDTO);
	      hostResponse = HostAdapterManager.processRequest(hostRequest);
	      responseDTO = assembler.fromUBSObjectToAccountInquiryResponse((AccountInquiryResponseHostDTO)hostResponse.response);
	    } catch (Exception e) {
	      if (logger.isLoggable(Level.SEVERE))
	        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch account name method of AccountInquiryHostAdapter", new Object[0]), e); 
	    } finally {
	      AdapterInteraction.close();
	    } 
	    checkResponse(responseDTO);
	    return responseDTO;
	  }
	
		public RMDetailInfoResponseDTO fetchRMDetail(SessionContext sessionContext,RMDetailInfoRequestDTO requestDTO) throws Exception {
	    
		
			if (logger.isLoggable(Level.FINE))
		      logger.log(Level.FINE, "Entered into method fetch RM Detail "); 
		    checkRequest("com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.AccountInquiryAdapter.fetchRMDetail", new Object[] { requestDTO });
		    AdapterInteraction.begin();
		    RMDetailInfoResponseDTO responseDTO = null;
		    HostResponseDTO hostResponse = null;
		    HostRequestDTO hostRequest = null;
		    try {
		    	
		    	System.out.println("Fecth RM Detail: " + requestDTO.getAccountNo() + " " + requestDTO.getAffiliateCode() + " " );
		    	
		    	AccountInquiryAssembler assembler = new AccountInquiryAssembler();
		      hostRequest = assembler.fromAdaptertoHostRequestFetchRMetail(requestDTO.getAccountNo(), requestDTO.getAffiliateCode());
		      hostResponse = HostAdapterManager.processRequest(hostRequest);
		      responseDTO = assembler.fromUBSObjectToRMDetailResponse((RelationshipManagerInquiryResponseHostDTO)hostResponse.response);
		    } catch (Exception e) {
		      if (logger.isLoggable(Level.SEVERE))
		        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch forex rate method of ForexRateInquiryAdapter", new Object[0]), e); 
		    } finally {
		      AdapterInteraction.close();
		    } 
		    checkResponse(responseDTO);
		    return responseDTO;
	  }
		
		public RMDetailInfoResponseDTO fetchRMDetailByCode(SessionContext sessionContext,String rmCode, String affiliateCode) throws Exception {
		    
			
			if (logger.isLoggable(Level.FINE))
		      logger.log(Level.FINE, "Entered into method fetch RM Detail "); 
		    checkRequest("com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.AccountInquiryAdapter.fetchRMDetailByCode", new Object[] { rmCode });
		    AdapterInteraction.begin();
		    RMDetailInfoResponseDTO responseDTO = null;
		    HostResponseDTO hostResponse = null;
		    HostRequestDTO hostRequest = null;
		    try {
		    	
		    	System.out.println("Fecth RM Detail: " + rmCode + " " + affiliateCode + " " );
		    	
		    	AccountInquiryAssembler assembler = new AccountInquiryAssembler();
		      hostRequest = assembler.fromAdaptertoHostRequestRMetailInquiryByCode(rmCode, affiliateCode);
		      hostResponse = HostAdapterManager.processRequest(hostRequest);
		      responseDTO = assembler.fromUBSObjectToRMDetailResponse((RelationshipManagerInquiryResponseHostDTO)hostResponse.response);
		    } catch (Exception e) {
		      if (logger.isLoggable(Level.SEVERE))
		        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch forex rate method of ForexRateInquiryAdapter", new Object[0]), e); 
		    } finally {
		      AdapterInteraction.close();
		    } 
		    checkResponse(responseDTO);
		    return responseDTO;
	  }
		
		public RMDetailListResponse fetchRMDListByParty(SessionContext sessionContext,RMDetailInfoRequestDTO requestDTO) throws Exception {
		    
			
			if (logger.isLoggable(Level.FINE))
		      logger.log(Level.FINE, "Entered into method fetchRMDListByParty "); 
		    checkRequest("com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.AccountInquiryAdapter.fetchRMDListByParty", new Object[] { requestDTO });
		    AdapterInteraction.begin();
		    RMDetailListResponse responseDTO = null;
		    HostResponseDTO hostResponse = null;
		    HostRequestDTO hostRequest = null;
		    
		    
		    try {
		    	
		    	System.out.println("Fecth RM Detail: " + requestDTO.getAccountNo() + " " + requestDTO.getAffiliateCode() + " " );
		    	
		    	AccountInquiryAssembler assembler = new AccountInquiryAssembler();
		      hostRequest = assembler.fromAdaptertoHostRequestFetchRMetailListByParty(requestDTO.getAccountNo(), requestDTO.getAffiliateCode());
		      hostResponse = HostAdapterManager.processRequest(hostRequest);
		     
		      responseDTO = assembler.fromUBSObjectToRMDetailListResponse((RelationshipManagerListResponseHostDTO)hostResponse.response);
		    } catch (Exception e) {
		      if (logger.isLoggable(Level.SEVERE))
		        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch forex rate method of fetchRMDListByParty", new Object[0]), e); 
		    } finally {
		      AdapterInteraction.close();
		    } 
		    checkResponse(responseDTO);
		    return responseDTO;
	  }
		
		public AccountInquiryListResponse fetchAccountListByCustId(SessionContext sessionContext,String customerId,String affCode) throws Exception {
		    
			
			if (logger.isLoggable(Level.FINE))
		      logger.log(Level.FINE, "Entered into method fetchRMDListByParty "); 
		    checkRequest("com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.AccountInquiryAdapter.fetchAccountListByCustId", new Object[] { customerId });
		    AdapterInteraction.begin();
		    AccountInquiryListResponse responseDTO = null;
		    HostResponseDTO hostResponse = null;
		    HostRequestDTO hostRequest = null;
		    
		    
		    try {
		    	
		    	System.out.println("Fecth Account List: " + customerId + " " );
		    	
		    	AccountInquiryAssembler assembler = new AccountInquiryAssembler();
		      hostRequest = assembler.fromAdaptertoHostAccountlListByParty(customerId, affCode);
		      hostResponse = HostAdapterManager.processRequest(hostRequest);
		     
		      responseDTO = assembler.fromUBSObjectToAccountsListResponse((AccountListResponseHostDTO)hostResponse.response);
		    } catch (Exception e) {
		      if (logger.isLoggable(Level.SEVERE))
		        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch forex rate method of fetchRMDListByParty", new Object[0]), e); 
		    } finally {
		      AdapterInteraction.close();
		    } 
		    checkResponse(responseDTO);
		    return responseDTO;
	  }
		
		public AccountDetailDTO  fetchAccountDetail(SessionContext sessionContext,AccountDetailRequestDTO requestDTO) throws Exception {
		    
			
			if (logger.isLoggable(Level.FINE))
		      logger.log(Level.FINE, "Entered into method fetch Account Detail "); 
		    checkRequest("com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.AccountInquiryAdapter.fetchAccountDetail", new Object[] { requestDTO });
		    AdapterInteraction.begin();
		    AccountDetailDTO responseDTO = null;
		    HostResponseDTO hostResponse = null;
		    HostRequestDTO hostRequest = null;
		    try {
		    	
		    	System.out.println("Fecth Account Detail: " + requestDTO.getAccountNo() + " " + requestDTO.getAffiliateCode() + " " );
		    	
		    	AccountInquiryAssembler assembler = new AccountInquiryAssembler();
		      hostRequest = assembler.fromAdaptertoHostRequestFetchAccountDetail(requestDTO.getAccountNo(), requestDTO.getAffiliateCode());
		      hostResponse = HostAdapterManager.processRequest(hostRequest);
		      responseDTO = assembler.fromUBSObjectToAccountDetailDTO((AccountDetailResponseHostDTO)hostResponse.response);
		    } catch (Exception e) {
		      if (logger.isLoggable(Level.SEVERE))
		        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch Account Detail method of AccountInquiryAdapter", new Object[0]), e); 
		    } finally {
		      AdapterInteraction.close();
		    } 
		    checkResponse(responseDTO);
		    return responseDTO;
	  }
		
    public AccountMISAndSignatoryDetailDTO fetchAccountAndCustomerMISAndSignatoryCount(SessionContext sessionContext,AccountDetailRequestDTO requestDTO) throws Exception {
		    
			
			if (logger.isLoggable(Level.FINE))
		      logger.log(Level.FINE, "Entered into method fetchAccountAndCustomerMISAndSignatoryCount "); 
		    checkRequest("com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.AccountInquiryAdapter.fetchAccountAndCustomerMISAndSignatoryCount", new Object[] { requestDTO });
		    AdapterInteraction.begin();
		    AccountMISAndSignatoryDetailDTO responseDTO = new AccountMISAndSignatoryDetailDTO() ;
		    HostResponseDTO hostResponse = null;
		    HostRequestDTO hostRequest = null;
		    try {
		    	
		    	System.out.println("Fecth Account Detail: " + requestDTO.getAccountNo() + " " + requestDTO.getAffiliateCode() + " " );
		    	
		    	AccountInquiryAssembler assembler = new AccountInquiryAssembler();
		      hostRequest = assembler.fromAdaptertoHostRequestFetchAccountSignatoryCount(requestDTO.getAccountNo(), requestDTO.getAffiliateCode());
		      hostResponse = HostAdapterManager.processRequest(hostRequest);
		      
		      AccountInquiryResponseHostDTO aresp = (AccountInquiryResponseHostDTO)hostResponse.response;
		      if(aresp != null)
		      {
		    	  responseDTO.setSignatoryCount(aresp.signatoryCount);
		      }
		      
		      hostRequest = assembler.fromAdaptertoHostRequestFetchCustomerMISBusinessCode(requestDTO.getCustomerId(), requestDTO.getAffiliateCode());
		      hostResponse = HostAdapterManager.processRequest(hostRequest);
		      
		       aresp = (AccountInquiryResponseHostDTO)hostResponse.response;
		       if(aresp != null)
			      {
			    	  responseDTO.setMisBusinessCode(aresp.misBusinessCode);
			      }
		      
		    
		    } catch (Exception e) {
		      if (logger.isLoggable(Level.SEVERE))
		        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch Account Detail method of AccountInquiryAdapter", new Object[0]), e); 
		    } finally {
		      AdapterInteraction.close();
		    } 
		    checkResponse(responseDTO);
		    return responseDTO;
	  }
		
		public CustomerInfoDTO fetchCustomerDetail(SessionContext sessionContext,CustomerInfoRequestDTO requestDTO) throws Exception {
		    
			
			if (logger.isLoggable(Level.FINE))
		      logger.log(Level.FINE, "Entered into method fetch Customer Detail "); 
		    checkRequest("com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.AccountInquiryAdapter.fetchCustomerDetail", new Object[] { requestDTO });
		    AdapterInteraction.begin();
		    CustomerInfoDTO responseDTO = null;
		    HostResponseDTO hostResponse = null;
		    HostRequestDTO hostRequest = null;
		    try {
		    	
		    	System.out.println("Fecth Customer Detail: " + requestDTO.getCustomerId() + " " + requestDTO.getAffiliateCode() + " " );
		    	
		    	AccountInquiryAssembler assembler = new AccountInquiryAssembler();
		      hostRequest = assembler.fromAdaptertoHostRequestCustomerInfoHost(requestDTO.getCustomerId(), requestDTO.getAffiliateCode());
		      hostResponse = HostAdapterManager.processRequest(hostRequest);
		      responseDTO = assembler.fromUBSObjectToCustomerInfoResponse((CustomerInfoResponseHostDTO)hostResponse.response);
		    } catch (Exception e) {
		      if (logger.isLoggable(Level.SEVERE))
		        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch Customer Detail method of AccountInquiryAdapter", new Object[0]), e); 
		    } finally {
		      AdapterInteraction.close();
		    } 
		    checkResponse(responseDTO);
		    return responseDTO;
	  }
	
       public ForexRateResponseDTO fetchForexRate(SessionContext sessionContext,ForexRateRequestDTO requestDTO) throws Exception {
	    
		
		if (logger.isLoggable(Level.FINE))
	      logger.log(Level.FINE, "Entered into method fetch forex rate ForexRateInquiryAdapter "); 
	    checkRequest("com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl.ForexRateInquiryAdapter.fetchForexRate", new Object[] { requestDTO });
	    AdapterInteraction.begin();
	    ForexRateResponseDTO responseDTO = null;
	    HostResponseDTO hostResponse = null;
	    HostRequestDTO hostRequest = null;
	    try {
	    	
	    	System.out.println("Fecth Rate: " + requestDTO.getBaseCcy() + " " + requestDTO.getQuoteCcy() + " " + requestDTO.getAffiliateCode() + " " + requestDTO.getBranchCode());
	    	
	    	AccountInquiryAssembler assembler = new AccountInquiryAssembler();
	      hostRequest = assembler.fromAdaptertoHostRequestForexRate(requestDTO);
	      hostResponse = HostAdapterManager.processRequest(hostRequest);
	      responseDTO = assembler.fromUBSObjectToForexRateResponse((ForexRateResponseHostDTO)hostResponse.response);
	    } catch (Exception e) {
	      if (logger.isLoggable(Level.SEVERE))
	        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch forex rate method of ForexRateInquiryAdapter", new Object[0]), e); 
	    } finally {
	      AdapterInteraction.close();
	    } 
	    checkResponse(responseDTO);
	    return responseDTO;
	  }
       
       public String fetchLocalCurrencyCode(SessionContext sessionContext,String affCode) throws Exception {
   	    
    	   String ccyCode ="";
   		
    	   ccyCode = digx_consulting.get(affCode + "_LCY", "");
    	   System.out.println("CCY-CODE-LCY: " + affCode + "  " + ccyCode);
   		
   	    try {
   	    	
   	    
   	    } catch (Exception e) {
   	    	e.printStackTrace();
   	    }
   	    return ccyCode;
   	  }
	  
	  

}
