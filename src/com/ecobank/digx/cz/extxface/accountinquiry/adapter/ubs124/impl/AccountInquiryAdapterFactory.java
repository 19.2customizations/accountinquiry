package com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl;

import com.ofss.digx.app.adapter.AdapterFactory;

import com.ofss.digx.datatype.NameValuePair;


public class AccountInquiryAdapterFactory extends AdapterFactory  {
	
	
	private static AccountInquiryAdapterFactory adapterFactory = null;
	  
	  //private boolean isAccountInquiryAdaterMocked = AdapterMockingConfigurator.getInstance().isAdapterMocked("ACCOUNT_INQUIRY_ADAPTER_MOCKED").booleanValue();
	  
	  public static AccountInquiryAdapterFactory getInstance() {
	    if (adapterFactory == null)
	      synchronized (AccountInquiryAdapterFactory.class) {
	        adapterFactory = new AccountInquiryAdapterFactory();
	      }  
	    return adapterFactory;
	  }
	  
	  public Object getAdapter(String adapterClass) {
	    IAccountInquiryAdapter adapter = new AccountInquiryAdapter();
	     
	    return adapter;
	  }
	  
	  public Object getAdapter(String adapter, NameValuePair[] nameValues) {
	    return null;
	  }

}
