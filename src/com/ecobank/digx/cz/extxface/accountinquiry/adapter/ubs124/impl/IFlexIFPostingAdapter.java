package com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl;

import com.ecobank.digx.cz.extsystem.dto.BatchPostingRequestDTO;
import com.ecobank.digx.cz.extsystem.dto.BatchPostingResponseDTO;

public interface IFlexIFPostingAdapter {
	
	public  BatchPostingResponseDTO postBatchTran(BatchPostingRequestDTO batchReq, boolean reversal);

}
