package com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl;

import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryListResponse;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.RMDetailInfoRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.RMDetailInfoResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.RMDetailListResponse;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailRequestDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountMISAndSignatoryDetailDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.CustomerInfoDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.CustomerInfoRequestDTO;
import com.ofss.fc.app.context.SessionContext;

public interface IAccountInquiryAdapter {
	
	public AccountInquiryResponseDTO fetchAccountName (SessionContext sessionContext,AccountInquiryRequestDTO requestDTO) throws Exception;
	public ForexRateResponseDTO fetchForexRate(SessionContext sessionContext,ForexRateRequestDTO requestDTO) throws Exception ;
	public CustomerInfoDTO fetchCustomerDetail(SessionContext sessionContext,CustomerInfoRequestDTO requestDTO) throws Exception ;
	public String fetchLocalCurrencyCode(SessionContext sessionContext,String affCode) throws Exception ;
	public AccountDetailDTO fetchAccountDetail(SessionContext sessionContext,AccountDetailRequestDTO requestDTO) throws Exception ;  	    
	public RMDetailInfoResponseDTO fetchRMDetail(SessionContext sessionContext,RMDetailInfoRequestDTO requestDTO) throws Exception ;
	public RMDetailListResponse fetchRMDListByParty(SessionContext sessionContext,RMDetailInfoRequestDTO requestDTO) throws Exception ;
	public AccountMISAndSignatoryDetailDTO fetchAccountAndCustomerMISAndSignatoryCount(SessionContext sessionContext,AccountDetailRequestDTO requestDTO) throws Exception;
	public RMDetailInfoResponseDTO fetchRMDetailByCode(SessionContext sessionContext,String rmCode, String affiliateCode) throws Exception ;
	public AccountInquiryListResponse fetchAccountListByCustId(SessionContext sessionContext,String customerId,String affCode) throws Exception ;
		   
}
