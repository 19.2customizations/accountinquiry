package com.ecobank.digx.cz.extxface.accountinquiry.adapter.ubs124.impl;

import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import com.ecobank.digx.cz.extsystem.dto.BatchPostingRequestDTO;
import com.ecobank.digx.cz.extsystem.dto.BatchPostingResponseDTO;
import com.ecobank.digx.cz.extsystem.dto.TransactionEntry;

import com.ofss.digx.extxface.adapter.AbstractAdapter;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.infra.thread.ThreadAttribute;
import com.ofss.fcubs.gw.ws.types.fcubsifservice.FCUBSIFService;
import com.ofss.fcubs.gw.ws.types.fcubsifservice.FCUBSIFServiceSEI;
import com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSREQ;
import com.ofss.fcubs.service.fcubsifservice.ExtAccEcaEntriesFullType;
import com.ofss.fcubs.service.fcubsifservice.FCUBSHEADERType;
import com.ofss.fcubs.service.fcubsifservice.UBSCOMPType;
import com.thoughtworks.xstream.XStream;

public class FlexIFPostingAdapter  extends AbstractAdapter implements IFlexIFPostingAdapter {
	
	private static final String THIS_COMPONENT_NAME = FlexIFPostingAdapter.class.getName();
	  
	  private final transient MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	  
	  private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);

	  //private Preferences esbInterface = ConfigurationFactory.getInstance().getConfigurations("ESBInterfaceConfig");
		private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");


	public  BatchPostingResponseDTO postBatchTran(BatchPostingRequestDTO batchReq, boolean reversal) {

		BatchPostingResponseDTO bresp = new BatchPostingResponseDTO();
		String[] resp2 = new String[3];
		resp2[0] = "E99";
		resp2[1] = "CBA Error";
		resp2[2] = "";

		try {

			
			
			String affCode = batchReq.getAffiliateCode();
			String lcyCcy = digx_consulting.get(affCode + "_LCY", "");
			System.out.println("LCY-CCY-PARAM:: " + lcyCcy);
			System.out.println("LCY-CCY-PARAM:: " + affCode);
			
			String urlxIF = digx_consulting.get(affCode + "_IF_GATEWAY_URL", "");
			String urlxIF2 = digx_consulting.get(affCode + "_IF_GATEWAY_URL", "CZ_PARAMS");
			
			System.out.println("IF-URL-PARAM:: " + urlxIF);
			System.out.println("IF-URL-PARAM:: " + urlxIF2);
			
			String entity = "";;
			if(ThreadAttribute.get("TARGET_UNIT_CODE") != null)
			     entity = ThreadAttribute.get("TARGET_UNIT_CODE").toString();
			
			System.out.println("ENTITY-PARAM:: " + entity);
			
			//String url = "http://10.4.139.86:7004/FCUBSIFService/FCUBSIFService?wsdl";  // digx_consulting.get(affCode + "_GATEWAY_URL", "");
			String url = "http://10.4.139.87:7004/FCUBSIFService/FCUBSIFService?wsdl"; 
			
			//String digx_consulting.get(affCode + "_GATEWAY_URL", "");
			
			System.out.println("Flexcube Post Url: " + url + " " + lcyCcy);
			
			String sourceCode = batchReq.getSourceCode();

			String event = "INIT";
			if (reversal) {
				event = "REVR";
			}

			String refNo2 = batchReq.getBatchRefNo();

			URL wsdlURL = new URL(url);

// SetTrustStore2();
// System.setProperty("https.protocols", "TLSv1,SSLv3");
//  FCUBSIFService service = new FCUBSIFService(wsdlURL);
//   FCUBSIFServiceSEI port = service.getFCUBSIFServiceSEI();

			//final SSLSocketFactory sc = ignoreSSL();
			//HttpsURLConnection.setDefaultSSLSocketFactory(sc);
			FCUBSIFService service = new FCUBSIFService(wsdlURL);
			FCUBSIFServiceSEI port = service.getFCUBSIFServiceSEI();

			BindingProvider bindp = ((BindingProvider) port);
			Map<String, Object> requestContext = bindp.getRequestContext();
			//requestContext.put("com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", sc);
			//requestContext.put("com.sun.xml.ws.transport.https.client.SSLSocketFactory", sc);
			//requestContext.put("weblogic.wsee.jaxws.JAXWSProperties.SSL_SOCKET_FACTORY", sc);

			com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSREQ breq = new com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSREQ();

//ExtAccEcaEntriesFullType ecaFull = breq.getFCUBSBODY().getAccEntryMasterFull()
// com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSRE breq = new com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSRES();
			port.createExtAccEcaEntriesFS(breq);

//ExtAccEcaEntriesFullType accEntryMasterFull breq.getFCUBSBODY().getAccEntryMasterFull();
//FCUBSHEADERType fcubsheader = null;
			CREATEEXTACCECAENTRIESFSFSREQ.FCUBSBODY fcubsbody = new CREATEEXTACCECAENTRIESFSFSREQ.FCUBSBODY();
			FCUBSHEADERType fcubsheader = new FCUBSHEADERType();

			fcubsheader.setBRANCH(batchReq.getPostingBranchCode());
//  fcubsheader.setSOURCE(batchReq.getSourceCode());
			fcubsheader.setSOURCE(sourceCode);
			fcubsheader.setUBSCOMP(UBSCOMPType.FCUBS);
			fcubsheader.setOPERATION("CreateExtAccEcaEntries");
			fcubsheader.setSERVICE("FCUBSIFService");
			fcubsheader.setMSGID(java.util.UUID.randomUUID().toString());
			fcubsheader.setCORRELID(batchReq.getBatchRefNo());
			
			
			String userId = batchReq.getSourceCode();
			if (!affCode.equals("ENG")) {
				userId = userId + "_" + affCode.substring(1);
			}
			if (affCode.equalsIgnoreCase("ESD")) {
				affCode = "ESS";
				userId = userId + "_" + affCode.substring(1);
			}

			fcubsheader.setUSERID(userId);
			fcubsheader.setSOURCEOPERATION("CreateExtAccEcaEntries");

			ExtAccEcaEntriesFullType accEntryMasterFull = new ExtAccEcaEntriesFullType();
			accEntryMasterFull.setEVENT(event);
            //accEntryMasterFull.setEVNTSRNO(BigDecimal.ONE);
			accEntryMasterFull.setGRPREFNO(batchReq.getBatchRefNo());
//accEntryMasterFull.setSOURCECODE(batchReq.getSourceCode());
			accEntryMasterFull.setSOURCECODE(sourceCode);
			accEntryMasterFull.setUNIQUEEXTREFNO(batchReq.getBatchRefNo());
			accEntryMasterFull.setTXNBRN(batchReq.getPostingBranchCode());

			List<ExtAccEcaEntriesFullType.AccEntryDetails> list = new ArrayList<ExtAccEcaEntriesFullType.AccEntryDetails>();
			ExtAccEcaEntriesFullType.AccEntryDetails t = null;
			for (TransactionEntry te : batchReq.getTransactionEntries()) {
				t = new ExtAccEcaEntriesFullType.AccEntryDetails();
				t.setACBRANCH(te.getAccountBranchCode());
				t.setACCCY(te.getCurrencyCode());
				t.setACNO(te.getAccountNo());
				t.setEVENT(event);
				t.setINSTRUMENTCODE(te.getInstrumentNo());
				t.setDRCRIND(te.getCreditOrDebit());
				t.setEXCHRATE(te.getExchangeRate());
				t.setEVENTSRNO(new BigDecimal(te.getSerialNo()));

				t.setRELATEDCUSTOMER(te.getCustomerId());
				t.setTRNREFNO(refNo2);
				try {
					XMLGregorianCalendar gregFmt = DatatypeFactory.newInstance()
							.newXMLGregorianCalendar(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					t.setVALUEDT(gregFmt);
//t.setVALUEDT(otaStringUtil.GetXMLGregDate(new java.util.Date()));
				} catch (Exception ec) {
				}
				t.setMODULE("IF");
				
				if(event.equals("REVR"))
				{
					t.setLCYAMT(te.getLcyAmount().negate());
					if (!lcyCcy.equals(te.getCurrencyCode())) {
						t.setFCYAMT(te.getAmount().negate());
					}
				}
				else
				{
					t.setLCYAMT(te.getLcyAmount());
					if (!lcyCcy.equals(te.getCurrencyCode())) {
						t.setFCYAMT(te.getAmount());
					}
				}
				

				t.setTXNNARRATIVE(te.getNarration());
				t.setTRNCODE(te.getTransactionCode());
				t.setDONTSHOWINSTMT("N");
				if (!reversal) {
					t.setAVAILBALREQD("Y");
				}

				list.add(t);

			} // for

			accEntryMasterFull.getAccEntryDetails().addAll(list);
			fcubsbody.setAccEntryMasterFull(accEntryMasterFull);
			breq.setFCUBSBODY(fcubsbody);
			breq.setFCUBSHEADER(fcubsheader);

			XStream xs = new XStream();

			System.out.println("Post Request:\n-------------------------------------\n " + xs.toXML(breq));

			com.ofss.fcubs.service.fcubsifservice.CREATEEXTACCECAENTRIESFSFSRES resp = port.createExtAccEcaEntriesFS(breq);
              System.out.println("Response: " + xs.toXML(resp));
			FCUBSHEADERType rspHead = resp.getFCUBSHEADER();
			String rspCode = rspHead.getMSGSTAT().value().toString();

			System.out.println("Response: " + rspHead.getMSGSTAT() ); //+ "  " + xs.toXML(resp));
			System.out.println("Respone2 " + resp.getFCUBSBODY().getAccEntryMasterFull().getTRNREFNO());

			if (rspCode.equals("SUCCESS")) {
				resp2[0] = "000";
				resp2[1] = "SUCCESS";

				String refNo = resp.getFCUBSBODY().getAccEntryMasterFull().getGRPREFNO().toString();
				resp2[2] = refNo;
				System.out.println("Respone24444 " + refNo);

			} else {
//resp.getFCUBSBODY().getFCUBSERRORRESP().get(0).getERROR().get(0).getECODE();
//String msg = resp.getFCUBSBODY().getFCUBSWARNINGRESP().get(0).getWARNING().get(0).getWDESC();
				String msg = resp.getFCUBSBODY().getFCUBSERRORRESP().get(0).getERROR().get(0).getEDESC();
				String msgc = resp.getFCUBSBODY().getFCUBSERRORRESP().get(0).getERROR().get(0).getECODE();
				resp2[0] = "E04";
				resp2[1] = "CBA Error:" + msg + " " + msgc;
				resp2[2] = "";
			}
			
			bresp.setResponseCode(resp2[0]);
			bresp.setResponseMessage(resp2[1]);
			bresp.setCbaReferenceNo(resp2[2]);

		} catch (Throwable e) {
			e.printStackTrace();
			//System.out.println("Exception Occurred >> " + e.getMessage());
			System.out.println("Throwable " + Arrays.toString(e.getStackTrace()).replaceAll(", ", "\n"));
		}

		return bresp;
	}

}
