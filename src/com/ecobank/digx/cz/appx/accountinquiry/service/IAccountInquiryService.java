package com.ecobank.digx.cz.appx.accountinquiry.service;


import javax.ws.rs.core.Response;

import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AlertMessageRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.FetchAccountDetailRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateRequestDTO;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

public interface IAccountInquiryService {
	
	Response read(String accNo );
	
	Response create(AccountTransferRequestDTO accountTransfer );
    Response fetchAccountDetail(String accNo);
    Response fetchForexRate(String baseCcy, String quoteCcy);
    Response computeReceiveAmount(String sendCcy, String receiveCcy, String amount);
    
    //Response updateStatus(String paymentId);
    
    Response sendMessage(AlertMessageRequestDTO request);
    
    Response fetchTransferList(String affiliateCode, String tranType);
    
    Response fetchAccountDetailFullInfo(String accNo, String affCode) ;
    
    Response computeTransferRate(ForexRateRequestDTO request)  ;
    
    Response getAccountInfo(FetchAccountDetailRequestDTO request)  ;



    

    
  

}
