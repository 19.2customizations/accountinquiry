package com.ecobank.digx.cz.appx.accountinquiry.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountInquiryResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AccountTransferResponseList;
import com.ecobank.digx.cz.app.accountinquiry.dto.AlertMessageRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.AlertMessageResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.FetchAccountDetailRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateRequestDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.ForexRateResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.dto.GenericLookupDataResponseList;
import com.ecobank.digx.cz.app.accountinquiry.dto.TransferReceiveAmountResponseDTO;
import com.ecobank.digx.cz.app.accountinquiry.service.AccountInquiry;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailDTO;
import com.ecobank.digx.cz.extxface.accountinquiry.ubs124.dto.AccountDetailRequestDTO;
import com.ofss.digx.app.context.ChannelContext;
import com.ofss.digx.app.core.ChannelInteraction;
import com.ofss.digx.app.messages.Status;
import com.ofss.digx.appx.AbstractRESTApplication;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.digx.service.response.BaseResponseObject;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@Path("/accountInquiry")
public class AccountInquiryService extends AbstractRESTApplication implements IAccountInquiryService {

	private static final String THIS_COMPONENT_NAME = AccountInquiryService.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	private static transient MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();

	// @Parameter(in = ParameterIn.PATH, description = "account no", required =
	// true, name = "accNo", schema = @Schema(type = "String"))

	@Path("/{accNo}")
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "fetch account name", description = "fetch account name", tags = {
			"Fetch account name" }, operationId = "com.ecobank.digx.cz.appx.accountinquiry.service.AccountInquiryService.read")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = AccountInquiryResponseDTO.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response read(
			@Parameter(in = ParameterIn.PATH, description = "account no", required = true, name = "accNo", schema = @Schema(type = "String")) @PathParam("accNo") String accNo) {

		System.out.println("Account-Inquiry-REST: " + accNo);

		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, this.formatter.formatMessage(" Receive account no " + accNo));

		AccountInquiryResponseDTO responseDTO = new AccountInquiryResponseDTO();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			AccountInquiryRequestDTO request = new AccountInquiryRequestDTO();
			request.setAccountNo(accNo);

			String bankCode = channelContext.getSessionContext().getBankCode();

			String affCode = channelContext.getSessionContext().getTargetUnit();
			String userId = channelContext.getSessionContext().getUserId();
			request.setAffiliateCode(affCode);
			logger.log(Level.FINE, " Receive aff code " + affCode + " " + bankCode);
			logger.log(Level.FINE, "User Id " + userId);

			System.out.println("Account-Inquiry-REST: " + userId + " " + bankCode);

			AccountInquiry inquiry = new AccountInquiry();
			responseDTO = inquiry.read(channelContext.getSessionContext(), request);
			logger.log(Level.FINE, "Account name " + responseDTO.getAccountName());

			System.out.println("Account-Inquiry-RESPONSE: " + responseDTO.getAccountName());

			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}
	
	
	@Path("/fetchforexrate")  //?baseCcy=&quoteCcy
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "fetch forex rate", description = "fetch forex rate", tags = {
			"Fetch forex rate" }, operationId = "com.ecobank.digx.cz.appx.accountinquiry.service.AccountInquiryService.fetchForexRate")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = ForexRateResponseDTO.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response fetchForexRate(
			@Parameter(in = ParameterIn.QUERY, description = "base ccy", required = true, name = "baseCcy", schema = @Schema(type = "String")) @QueryParam("baseCcy") String baseCcy,
			@Parameter(in = ParameterIn.QUERY, description = "quote ccy", required = true, name = "quoteCcy", schema = @Schema(type = "String")) @QueryParam("quoteCcy") String quoteCcy) {

		System.out.println("Account-Inquiry-REST: " + baseCcy + " " + quoteCcy);

		//if (logger.isLoggable(Level.FINE))
		//	logger.log(Level.FINE, this.formatter.formatMessage(" Receive account no " + accNo));

		ForexRateResponseDTO responseDTO = new ForexRateResponseDTO();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			ForexRateRequestDTO request = new ForexRateRequestDTO();
			request.setAffiliateCode("ENG");
			request.setBaseCcy(baseCcy);
			request.setBranchCode("ENG");
			request.setQuoteCcy(quoteCcy);

			String bankCode = channelContext.getSessionContext().getBankCode();

			String affCode = channelContext.getSessionContext().getTargetUnit();
			String userId = channelContext.getSessionContext().getUserId();
			request.setAffiliateCode(affCode);
			logger.log(Level.FINE, " Receive aff code " + affCode + " " + bankCode);
			logger.log(Level.FINE, "User Id " + userId);

			System.out.println("Forex Rate-REST: " + userId + " " + bankCode);

			AccountInquiry inquiry = new AccountInquiry();
			responseDTO = inquiry.fetchForexRate(channelContext.getSessionContext(), request);
			logger.log(Level.FINE, "Forex Rate " + responseDTO.getBuyRate());

			System.out.println("Account-Inquiry-RESPONSE: " + responseDTO.getMidRate());

			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}
	
	@Path("/computereceiveamount")  //?baseCcy=&quoteCcy
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "Compute Receive amount", description = "Compute Receive amount", tags = {
			"Compute Receive amount" }, operationId = "com.ecobank.digx.cz.appx.accountinquiry.service.AccountInquiryService.computeReceiveAmount")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = ForexRateResponseDTO.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response computeReceiveAmount(
			@Parameter(in = ParameterIn.QUERY, description = "send ccy", required = true, name = "sendCcy", schema = @Schema(type = "String")) @QueryParam("sendCcy") String sendCcy,
			@Parameter(in = ParameterIn.QUERY, description = "receive Ccy", required = true, name = "receiveCcy", schema = @Schema(type = "String")) @QueryParam("receiveCcy") String receiveCcy,
			@Parameter(in = ParameterIn.QUERY, description = "amount", required = true, name = "amount", schema = @Schema(type = "String")) @QueryParam("amount") String amount) {

		System.out.println("Forex Amount-Compute-REST: " + sendCcy + " " + receiveCcy);
		System.out.println("Forex Amount-: " + amount);

		
		TransferReceiveAmountResponseDTO responseDTO = new TransferReceiveAmountResponseDTO();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			ForexRateRequestDTO request = new ForexRateRequestDTO();
			request.setAffiliateCode("ENG");
			request.setBaseCcy(sendCcy);
			request.setBranchCode("ENG");
			request.setQuoteCcy(receiveCcy);
			request.setAmount(Double.parseDouble(amount));

			String bankCode = channelContext.getSessionContext().getBankCode();

			String affCodex = channelContext.getSessionContext().getTargetUnit();
			String userId = channelContext.getSessionContext().getUserId();
			//OFSSUser
			//request.setAffiliateCode(affCode);
			logger.log(Level.FINE, " Receive aff code " + affCodex + " " + bankCode);
			logger.log(Level.FINE, "User Id " + userId);

			System.out.println("Forex Rate-REST: " + userId + " " + bankCode);

			AccountInquiry inquiry = new AccountInquiry();
			responseDTO = inquiry.computeReceiveAmount(channelContext.getSessionContext(), request); 
			//fetchForexRate(channelContext.getSessionContext(), request);
			
			logger.log(Level.FINE, "Forex Rate " + responseDTO.getBuyRate());
			
			System.out.println("Account-Inquiry-RESPONSE: " + responseDTO.getMidRate());

			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}
	
	@Path("/computetransferrate")  //?baseCcy=&quoteCcy
	@POST
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@Operation(summary = "Compute Receive amount", description = "Compute Receive amount", tags = {
			"Compute Receive amount" }, operationId = "com.ecobank.digx.cz.appx.accountinquiry.service.AccountInquiryService.computeTransferRate")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = TransferReceiveAmountResponseDTO.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response computeTransferRate(
			@RequestBody(description = "compute transfer exchange rate.", required = true, content = {
					@io.swagger.v3.oas.annotations.media.Content(schema = @Schema(implementation = ForexRateRequestDTO.class)) }) ForexRateRequestDTO request)  {

		System.out.println("Forex-update-Amount-Compute-REST: " + request.getAmount());
		//System.out.println("Forex Amount-: " + amount);

		
		TransferReceiveAmountResponseDTO responseDTO = new TransferReceiveAmountResponseDTO();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			

			String bankCode = channelContext.getSessionContext().getBankCode();

			//String affCodex = channelContext.getSessionContext().getTargetUnit();
			String userId = channelContext.getSessionContext().getUserId();
			//OFSSUser
			//request.setAffiliateCode(affCode);
			//logger.log(Level.FINE, " Receive aff code " + affCodex + " " + bankCode);
			logger.log(Level.FINE, "User Id " + userId);

			System.out.println("Forex Rate-REST: " + userId + " " + bankCode);

			AccountInquiry inquiry = new AccountInquiry();
			responseDTO = inquiry.computeTransferForexRate(channelContext.getSessionContext(), request); 
			
			
			logger.log(Level.FINE, "Forex Rate " + responseDTO.getBuyRate());
			
			System.out.println("Account-Inquiry-RESPONSE: " + responseDTO.getMidRate());

			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}
	
	@Path("/getaccountinfo")  //?baseCcy=&quoteCcy
	@POST
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@Operation(summary = "Get account info", description = "Get account info", tags = {
			"Get account info" }, operationId = "com.ecobank.digx.cz.appx.accountinquiry.service.AccountInquiryService.getAccountInfo")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = TransferReceiveAmountResponseDTO.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response getAccountInfo(
			@RequestBody(description = "get account info", required = true, content = {
					@io.swagger.v3.oas.annotations.media.Content(schema = @Schema(implementation = FetchAccountDetailRequestDTO.class)) }) FetchAccountDetailRequestDTO request)  {

		System.out.println("Fetch-account-detailInfo: " + request.getAccountNo());
		
		
		AccountDetailDTO responseDTO = new AccountDetailDTO();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			AccountDetailRequestDTO requestDTO = new AccountDetailRequestDTO();
			
            if(request.getAccountId() != null && request.getAccountId().getValue() != null)
            {
				String sAcc = request.getAccountId().getValue();
				System.out.println("Get-Account: " + sAcc);
				int x = 0;
				String sourceAccount = sAcc;
				if(sAcc.contains("~"))
				{
					x = sAcc.indexOf("~");
					sourceAccount = sAcc.substring(x + 1);
					requestDTO.setAccountNo(sourceAccount);
				}
            }
            else
              requestDTO.setAccountNo(request.getAccountNo());
            
            requestDTO.setAffiliateCode(request.getAffiliateCode());
			
			
			AccountInquiry inquiry = new AccountInquiry();
			responseDTO = inquiry.fetchAccountDetailFullInfo(channelContext.getSessionContext(), requestDTO);
			

			System.out.println("Account-info-Detail-RESPONSE: " + responseDTO.getAccountName());

			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}
	
	
	@Path("/fetchgenericlookupdata")  //?baseCcy=&quoteCcy
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "Fetch Generic Lookup data", description = "Fetch Generic Lookup data", tags = {
			"Fetch Generic Lookup data" }, operationId = "com.ecobank.digx.cz.appx.accountinquiry.service.AccountInquiryService.fetchGenericLookupDataList")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = GenericLookupDataResponseList.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response fetchGenericLookupDataList(
			@Parameter(in = ParameterIn.QUERY, description = "affiliateCode", required = true, name = "affiliateCode", schema = @Schema(type = "String")) @QueryParam("affiliateCode") String affiliateCode,
			@Parameter(in = ParameterIn.QUERY, description = "lookupCode", required = true, name = "lookupCode", schema = @Schema(type = "String")) @QueryParam("lookupCode") String lookupCode) {

		System.out.println("Fetch Generic Lookuo data: " + affiliateCode + " " + lookupCode);


		
		GenericLookupDataResponseList responseDTO = new GenericLookupDataResponseList();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			
			AccountInquiry inquiry = new AccountInquiry();
			responseDTO = inquiry.listGenericLookupData(channelContext.getSessionContext(), affiliateCode, lookupCode);
			
			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}
	
	@Path("/fetchtransferlist")  //?baseCcy=&quoteCcy
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "Fetch Transfer List", description = "Fetch Transfer List", tags = {
			"Fetch Transfer List" }, operationId = "com.ecobank.digx.cz.appx.accountinquiry.service.AccountInquiryService.fetchGenericLookupDataList")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = AccountTransferResponseList.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response fetchTransferList(
			@Parameter(in = ParameterIn.QUERY, description = "affiliateCode", required = true, name = "affiliateCode", schema = @Schema(type = "String")) @QueryParam("affiliateCode") String affiliateCode,
			@Parameter(in = ParameterIn.QUERY, description = "tranType", required = true, name = "tranType", schema = @Schema(type = "String")) @QueryParam("tranType") String tranType) {

		System.out.println("Fetch Transfer List: " + affiliateCode + " " + tranType);


		
		AccountTransferResponseList responseDTO = new AccountTransferResponseList();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			
			AccountInquiry inquiry = new AccountInquiry();
			responseDTO = inquiry.listTransfers(channelContext.getSessionContext());
			
			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}
	
	
	@Path("/fetchaccountdetail/{accNo}")
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "fetch account name", description = "fetch account detail", tags = {
			"Fetch account detail" }, operationId = "com.ecobank.digx.cz.appx.accountinquiry.service.AccountInquiryService.fetchAccountDetail")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = AccountInquiryResponseDTO.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response fetchAccountDetail(
			@Parameter(in = ParameterIn.PATH, description = "account no", required = true, name = "accNo", schema = @Schema(type = "String")) @PathParam("accNo") String accNo) {

		System.out.println("Account-Inquiry-Detail-REST: " + accNo);

		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, this.formatter.formatMessage(" Receive account no " + accNo));

		AccountInquiryResponseDTO responseDTO = new AccountInquiryResponseDTO();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			AccountInquiryRequestDTO request = new AccountInquiryRequestDTO();
			request.setAccountNo(accNo);

			String bankCode = channelContext.getSessionContext().getBankCode();

			String affCode = channelContext.getSessionContext().getTargetUnit();
			String userId = channelContext.getSessionContext().getUserId();
			String partyId = channelContext.getSessionContext().getTransactingPartyCode();
			request.setAffiliateCode(affCode);
			logger.log(Level.FINE, " Receive aff code " + affCode + " " + bankCode);
			logger.log(Level.FINE, "User Id " + userId);

			System.out.println("Account-Inquiry-REST: " + userId + " " + bankCode + " " + partyId);

			AccountInquiry inquiry = new AccountInquiry();
			responseDTO = inquiry.fetchAccountDetail(channelContext.getSessionContext(), request);
			logger.log(Level.FINE, "Account name " + responseDTO.getAccountName());

			System.out.println("Account-Inquiry-RESPONSE: " + responseDTO.getAccountName());
			
			
			//Test Fetch RM Detail
			

			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}
	
	@Path("/fetchaccountdetailfullinfo/{accNo}/{affCode}")
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "fetch account name", description = "fetch account detail", tags = {
			"Fetch account detail" }, operationId = "com.ecobank.digx.cz.appx.accountinquiry.service.AccountInquiryService.fetchAccountDetail")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = AccountInquiryResponseDTO.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response fetchAccountDetailFullInfo(
			@Parameter(in = ParameterIn.PATH, description = "account no", required = true, name = "accNo", schema = @Schema(type = "String")) @PathParam("accNo") String accNo,
			@Parameter(in = ParameterIn.PATH, description = "affiliate code", required = true, name = "affCode", schema = @Schema(type = "String")) @PathParam("affCode") String affCode) {

		System.out.println("Account-Inquiry-FullInfo-REST: " + accNo + " " + affCode);

		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, this.formatter.formatMessage(" Receive account no " + accNo));

		AccountDetailDTO responseDTO = new AccountDetailDTO();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			AccountDetailRequestDTO request = new AccountDetailRequestDTO();
			request.setAccountNo(accNo);
			request.setAffiliateCode(affCode);
			

			//System.out.println("Account-Inquiry-REST: " + userId + " " + bankCode + " " + partyId);

			AccountInquiry inquiry = new AccountInquiry();
			responseDTO = inquiry.fetchAccountDetailFullInfo(channelContext.getSessionContext(), request);
			logger.log(Level.FINE, "Account name " + responseDTO.getAccountName());

			System.out.println("Account-Inquiry-Detail-RESPONSE: " + responseDTO.getAccountName());
			
			
			//Test Fetch RM Detail
			

			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}

	@POST
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@Operation(summary = "Initiates Account transfer", description = "This API allows to initiate "
			+ "a transfer within mapped accounts of the logged in user within the same bank. "
			+ "The transfer is processed with current value date. On submitting the request a "
			+ "reference number is generated by OBDX indicating successful initiation of the payment. "
			+ "To post the initiated transfer to the core banking system API needs to be invoked.", tags = {
			"AccountTransfer" }, operationId = "com.ofss.digx.appx.payment.service.transfer.SelfTransfer.create")
	@ApiResponses({
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "201", description = "New Self-Transfer Transaction Created", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = AccountTransferResponseDTO.class)) }),
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }),
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }) })
	public Response create(
			@RequestBody(description = "Holds mandatory details required to perform the self transfer.", required = true, content = {
					@io.swagger.v3.oas.annotations.media.Content(schema = @Schema(implementation = AccountTransferRequestDTO.class)) }) AccountTransferRequestDTO accountTransfer) {
		
		System.out.println("Inncoming 22 Account Transfer: " + accountTransfer.getSenderAccountNo() );
		System.out.println("Inncoming 22 Account Transfer: " + accountTransfer.getSourceAccountId().getValue());
		System.out.println("Inncoming 22 Account Transfer: " + accountTransfer.getBeneficiaryAccountId().getValue());
		
		String sAcc = accountTransfer.getSourceAccountId().getValue();
		//C35@~0022069204
		if(sAcc != null &&  sAcc.indexOf("~") > 0)
		{
			int x = sAcc.indexOf("~");
			String sourceAccount = sAcc.substring(x + 1);
			accountTransfer.setSenderAccountNo(sourceAccount);
		}
		
		System.out.println("Inn-Send-Acc: " + accountTransfer.getSenderAccountNo());
		
		sAcc = accountTransfer.getBeneficiaryAccountId().getValue();
		if(sAcc != null &&  sAcc.indexOf("~") > 0)
        {
        	accountTransfer.setBeneficiaryAccountNo(sAcc.substring(sAcc.indexOf("~") + 1));
        }
		
		System.out.println("Inn-Ben-Acc: " + accountTransfer.getBeneficiaryAccountNo());
		
		
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					formatter.formatMessage(
							"Entered into create of Account Transfer REST Service  Input: AccountTransferRequestDTO: %s",
							new Object[] { accountTransfer }));
		}
		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		AccountTransferResponseDTO createResponse = null;
		try {
			channelContext = super.getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			//AccountTransferRequestDTO passCreateRequestDTO = new AccountTransferRequestDTO();

			AccountInquiry inquiryService = new AccountInquiry();
			createResponse = inquiryService.create(channelContext.getSessionContext(), accountTransfer);

			//response = buildResponse(createResponse, Response.Status.CREATED);
			response = buildResponse((BaseResponseObject) createResponse, Response.Status.CREATED);

		} catch (Exception e) {
			logger.log(Level.SEVERE,
					formatter.formatMessage(
							"Exception encountered while invoking the create service for AccountTransferRequestDTO=%s",
							new Object[] { accountTransfer }),
					e);

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while closing channelContext %s",
						new Object[] { channelContext }), e);

				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		logger.log(Level.FINE, formatter.formatMessage("Exiting from create() : AccountTransferCreateResponse=%s",
				new Object[] { createResponse }));

		return response;
	}
	
	
	 /* @POST
	  @Path("/{paymentId}")
	  @Consumes({"application/json"})
	  @Produces({"application/json"})
	  @Operation(summary = "Updates status of internal transfer initiated.", description = "Updates the status of Internal Transfer payment.", tags = {"Payments"}, operationId = "com.ofss.digx.appx.payment.service.transfer.InternalTransfer.updateStatus")
	  @ApiResponses({@ApiResponse(responseCode = "200", description = "Transaction Successful", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = AccountTransferResponseDTO.class))}), @ApiResponse(responseCode = "400", description = "Validation Failure", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class))}), @ApiResponse(responseCode = "500", description = "Internal Server Error", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class))})})
	  public Response updateStatus(@Parameter(in = ParameterIn.PATH, required = true, name = "paymentId", description = "System generated reference number for payment", schema = @Schema(type = "String")) @PathParam("paymentId") String paymentId) {
	    if (logger.isLoggable(Level.FINE))
	      logger.log(Level.FINE, formatter.formatMessage("Entering updateStatus of InternalTransfer rest service, payment Id: %s", new Object[] { paymentId })); 
	    System.out.println("Payment Id: " + paymentId);
	    Response response = null;
	    ChannelContext channelContext = null;
	    ChannelInteraction channelInteraction = null;
	   
	    AccountTransferResponseDTO responseDTO = new AccountTransferResponseDTO();
	    try {
	      channelContext = getChannelContext();
	      channelInteraction = ChannelInteraction.getInstance();
	      channelInteraction.begin(channelContext);
	      
	      AccountInquiry inquiryService = new AccountInquiry();
	      responseDTO = inquiryService.update(channelContext.getSessionContext(), paymentId);

			//response = buildResponse(createResponse, Response.Status.CREATED);
		  response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
	      
	     
	      } catch (Exception e) {
	      logger.log(Level.SEVERE, formatter.formatMessage("Exception encountered while invoking the updateStatus service for payment Id: %s", new Object[] { paymentId }), (Throwable)e);
	      response = buildResponse(e, Response.Status.BAD_REQUEST);
	    } finally {
	      try {
	        channelInteraction.close(channelContext);
	      } catch (Exception e) {
	        logger.log(Level.SEVERE, formatter
	            .formatMessage("Error encountered while closing channelContext %s", new Object[] { channelContext }), (Throwable)e);
	        response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
	      } 
	    } 
	    if (logger.isLoggable(Level.FINE))
	      logger.log(Level.FINE, formatter.formatMessage("Exiting from updateStatus() : InternalTransferResponse=%s", new Object[] { responseDTO })); 
	    return response;
	  }
	  */
	  
	  
	    @POST
	    @Path("/sendsms")
		@Produces({ "application/json" })
		@Consumes({ "application/json" })
		@Operation(summary = "Send SMS", description = "This API allows to send SMS.", tags = {
				"AlertMessageRequestDTO" }, operationId = "com.ecobank.digx.cz.appx.accountinquiry.service.AccountInquiryService.sendMessage")
		@ApiResponses({
				@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "201", description = "Send SMS", content = {
						@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = AlertMessageResponseDTO.class)) }),
				@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Validation Failure", content = {
						@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }),
				@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
						@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }) })
		public Response sendMessage(
				@RequestBody(description = "Holds mandatory details required to perform the self transfer.", required = true, content = {
						@io.swagger.v3.oas.annotations.media.Content(schema = @Schema(implementation = AlertMessageRequestDTO.class)) }) AlertMessageRequestDTO request) {
			
			System.out.println("Inncoming Mobile No SMS: " + request.getMobileNo() );
			
			
			if (logger.isLoggable(Level.FINE)) {
				logger.log(Level.FINE,
						formatter.formatMessage(
								"Entered into create of Send SMS  Input: AccountTransferRequestDTO: %s",
								new Object[] { request }));
			}
			Response response = null;
			ChannelInteraction channelInteraction = null;
			ChannelContext channelContext = null;
			AlertMessageResponseDTO createResponse = null;
			try {
				channelContext = super.getChannelContext();
				channelInteraction = ChannelInteraction.getInstance();
				channelInteraction.begin(channelContext);
				//AccountTransferRequestDTO passCreateRequestDTO = new AccountTransferRequestDTO();

				AccountInquiry inquiryService = new AccountInquiry();
				createResponse = inquiryService.sendMessage(channelContext.getSessionContext(), request);

				//response = buildResponse(createResponse, Response.Status.CREATED);
				response = buildResponse((BaseResponseObject) createResponse, Response.Status.CREATED);

			} catch (Exception e) {
				logger.log(Level.SEVERE,
						formatter.formatMessage(
								"Exception encountered while invoking the create service for AccountTransferRequestDTO=%s",
								new Object[] { request }),
						e);

				response = buildResponse(e, Response.Status.BAD_REQUEST);
			} finally {
				try {
					channelInteraction.close(channelContext);
				} catch (Exception e) {
					logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while closing channelContext %s",
							new Object[] { channelContext }), e);

					response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
				}
			}
			logger.log(Level.FINE, formatter.formatMessage("Exiting from create() : AccountTransferCreateResponse=%s",
					new Object[] { createResponse }));

			return response;
		}

}
