
package com.ofss.fcubs.service.fcubsifservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ExtAccEcaEntries-Create-IO-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtAccEcaEntries-Create-IO-Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SOURCECODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TXNBRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GRPREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EVNTSRNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="UNIQUEEXTREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EVENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TRNREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Acc-Entry-Details" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="AVAILBALREQD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="INTRADAYRELEASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="AVLINFO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RELATEDREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DRCRIND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RELATEDCUSTOMER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PRODUCT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TRNREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SWEEPREQD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EVENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="INSTRUMENTCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EXCHRATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="ESCROW" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EVENTSRNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="VALUEDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                   &lt;element name="CHEQUEMANDATORY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CONSFORTRNOVER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MODULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="AMTTAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ACCCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="BLOCKRELEASESTATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="FCYAMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="GRPREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CONSFORACCACTIVITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ECAREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ACBRANCH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ACNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RELATEDACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="LCYAMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="TXNNARRATIVE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SWIFT_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TRNCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEBIT_OVERRIDE_TRACKING" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CREDIT_OVERRIDE_TRACKING" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DONTSHOWINSTMT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EVENTSRNOTOBEREVD" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtAccEcaEntries-Create-IO-Type", propOrder = {
    "sourcecode",
    "txnbrn",
    "grprefno",
    "evntsrno",
    "uniqueextrefno",
    "event",
    "trnrefno",
    "accEntryDetails"
})
public class ExtAccEcaEntriesCreateIOType {

    @XmlElement(name = "SOURCECODE")
    protected String sourcecode;
    @XmlElement(name = "TXNBRN")
    protected String txnbrn;
    @XmlElement(name = "GRPREFNO")
    protected String grprefno;
    @XmlElement(name = "EVNTSRNO")
    protected BigDecimal evntsrno;
    @XmlElement(name = "UNIQUEEXTREFNO")
    protected String uniqueextrefno;
    @XmlElement(name = "EVENT")
    protected String event;
    @XmlElement(name = "TRNREFNO")
    protected String trnrefno;
    @XmlElement(name = "Acc-Entry-Details")
    protected List<ExtAccEcaEntriesCreateIOType.AccEntryDetails> accEntryDetails;

    /**
     * Gets the value of the sourcecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOURCECODE() {
        return sourcecode;
    }

    /**
     * Sets the value of the sourcecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOURCECODE(String value) {
        this.sourcecode = value;
    }

    /**
     * Gets the value of the txnbrn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTXNBRN() {
        return txnbrn;
    }

    /**
     * Sets the value of the txnbrn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTXNBRN(String value) {
        this.txnbrn = value;
    }

    /**
     * Gets the value of the grprefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRPREFNO() {
        return grprefno;
    }

    /**
     * Sets the value of the grprefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRPREFNO(String value) {
        this.grprefno = value;
    }

    /**
     * Gets the value of the evntsrno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEVNTSRNO() {
        return evntsrno;
    }

    /**
     * Sets the value of the evntsrno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEVNTSRNO(BigDecimal value) {
        this.evntsrno = value;
    }

    /**
     * Gets the value of the uniqueextrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNIQUEEXTREFNO() {
        return uniqueextrefno;
    }

    /**
     * Sets the value of the uniqueextrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNIQUEEXTREFNO(String value) {
        this.uniqueextrefno = value;
    }

    /**
     * Gets the value of the event property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEVENT() {
        return event;
    }

    /**
     * Sets the value of the event property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEVENT(String value) {
        this.event = value;
    }

    /**
     * Gets the value of the trnrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRNREFNO() {
        return trnrefno;
    }

    /**
     * Sets the value of the trnrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRNREFNO(String value) {
        this.trnrefno = value;
    }

    /**
     * Gets the value of the accEntryDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accEntryDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccEntryDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtAccEcaEntriesCreateIOType.AccEntryDetails }
     * 
     * 
     */
    public List<ExtAccEcaEntriesCreateIOType.AccEntryDetails> getAccEntryDetails() {
        if (accEntryDetails == null) {
            accEntryDetails = new ArrayList<ExtAccEcaEntriesCreateIOType.AccEntryDetails>();
        }
        return this.accEntryDetails;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="AVAILBALREQD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="INTRADAYRELEASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="AVLINFO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RELATEDREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DRCRIND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RELATEDCUSTOMER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PRODUCT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TRNREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SWEEPREQD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EVENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="INSTRUMENTCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EXCHRATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="ESCROW" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EVENTSRNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="VALUEDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *         &lt;element name="CHEQUEMANDATORY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CONSFORTRNOVER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MODULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="AMTTAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ACCCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="BLOCKRELEASESTATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="FCYAMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="GRPREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CONSFORACCACTIVITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ECAREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ACBRANCH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ACNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RELATEDACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LCYAMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="TXNNARRATIVE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SWIFT_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TRNCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEBIT_OVERRIDE_TRACKING" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CREDIT_OVERRIDE_TRACKING" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DONTSHOWINSTMT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EVENTSRNOTOBEREVD" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "availbalreqd",
        "intradayrelease",
        "avlinfo",
        "relatedref",
        "drcrind",
        "relatedcustomer",
        "product",
        "trnrefno",
        "sweepreqd",
        "event",
        "instrumentcode",
        "exchrate",
        "escrow",
        "eventsrno",
        "valuedt",
        "chequemandatory",
        "consfortrnover",
        "module",
        "amttag",
        "acccy",
        "blockreleasestatus",
        "fcyamt",
        "grprefno",
        "consforaccactivity",
        "ecarefno",
        "acbranch",
        "acno",
        "relatedaccount",
        "lcyamt",
        "txnnarrative",
        "swiftcode",
        "trncode",
        "debitoverridetracking",
        "creditoverridetracking",
        "dontshowinstmt",
        "eventsrnotoberevd"
    })
    public static class AccEntryDetails {

        @XmlElement(name = "AVAILBALREQD")
        protected String availbalreqd;
        @XmlElement(name = "INTRADAYRELEASE")
        protected String intradayrelease;
        @XmlElement(name = "AVLINFO")
        protected String avlinfo;
        @XmlElement(name = "RELATEDREF")
        protected String relatedref;
        @XmlElement(name = "DRCRIND")
        protected String drcrind;
        @XmlElement(name = "RELATEDCUSTOMER")
        protected String relatedcustomer;
        @XmlElement(name = "PRODUCT")
        protected String product;
        @XmlElement(name = "TRNREFNO")
        protected String trnrefno;
        @XmlElement(name = "SWEEPREQD")
        protected String sweepreqd;
        @XmlElement(name = "EVENT")
        protected String event;
        @XmlElement(name = "INSTRUMENTCODE")
        protected String instrumentcode;
        @XmlElement(name = "EXCHRATE")
        protected BigDecimal exchrate;
        @XmlElement(name = "ESCROW")
        protected String escrow;
        @XmlElement(name = "EVENTSRNO")
        protected BigDecimal eventsrno;
        @XmlElement(name = "VALUEDT")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar valuedt;
        @XmlElement(name = "CHEQUEMANDATORY")
        protected String chequemandatory;
        @XmlElement(name = "CONSFORTRNOVER")
        protected String consfortrnover;
        @XmlElement(name = "MODULE")
        protected String module;
        @XmlElement(name = "AMTTAG")
        protected String amttag;
        @XmlElement(name = "ACCCY")
        protected String acccy;
        @XmlElement(name = "BLOCKRELEASESTATUS")
        protected String blockreleasestatus;
        @XmlElement(name = "FCYAMT")
        protected BigDecimal fcyamt;
        @XmlElement(name = "GRPREFNO")
        protected String grprefno;
        @XmlElement(name = "CONSFORACCACTIVITY")
        protected String consforaccactivity;
        @XmlElement(name = "ECAREFNO")
        protected String ecarefno;
        @XmlElement(name = "ACBRANCH")
        protected String acbranch;
        @XmlElement(name = "ACNO")
        protected String acno;
        @XmlElement(name = "RELATEDACCOUNT")
        protected String relatedaccount;
        @XmlElement(name = "LCYAMT")
        protected BigDecimal lcyamt;
        @XmlElement(name = "TXNNARRATIVE")
        protected String txnnarrative;
        @XmlElement(name = "SWIFT_CODE")
        protected String swiftcode;
        @XmlElement(name = "TRNCODE")
        protected String trncode;
        @XmlElement(name = "DEBIT_OVERRIDE_TRACKING")
        protected String debitoverridetracking;
        @XmlElement(name = "CREDIT_OVERRIDE_TRACKING")
        protected String creditoverridetracking;
        @XmlElement(name = "DONTSHOWINSTMT")
        protected String dontshowinstmt;
        @XmlElement(name = "EVENTSRNOTOBEREVD")
        protected BigDecimal eventsrnotoberevd;

        /**
         * Gets the value of the availbalreqd property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAVAILBALREQD() {
            return availbalreqd;
        }

        /**
         * Sets the value of the availbalreqd property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAVAILBALREQD(String value) {
            this.availbalreqd = value;
        }

        /**
         * Gets the value of the intradayrelease property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINTRADAYRELEASE() {
            return intradayrelease;
        }

        /**
         * Sets the value of the intradayrelease property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINTRADAYRELEASE(String value) {
            this.intradayrelease = value;
        }

        /**
         * Gets the value of the avlinfo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAVLINFO() {
            return avlinfo;
        }

        /**
         * Sets the value of the avlinfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAVLINFO(String value) {
            this.avlinfo = value;
        }

        /**
         * Gets the value of the relatedref property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRELATEDREF() {
            return relatedref;
        }

        /**
         * Sets the value of the relatedref property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRELATEDREF(String value) {
            this.relatedref = value;
        }

        /**
         * Gets the value of the drcrind property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDRCRIND() {
            return drcrind;
        }

        /**
         * Sets the value of the drcrind property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDRCRIND(String value) {
            this.drcrind = value;
        }

        /**
         * Gets the value of the relatedcustomer property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRELATEDCUSTOMER() {
            return relatedcustomer;
        }

        /**
         * Sets the value of the relatedcustomer property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRELATEDCUSTOMER(String value) {
            this.relatedcustomer = value;
        }

        /**
         * Gets the value of the product property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPRODUCT() {
            return product;
        }

        /**
         * Sets the value of the product property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPRODUCT(String value) {
            this.product = value;
        }

        /**
         * Gets the value of the trnrefno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTRNREFNO() {
            return trnrefno;
        }

        /**
         * Sets the value of the trnrefno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTRNREFNO(String value) {
            this.trnrefno = value;
        }

        /**
         * Gets the value of the sweepreqd property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSWEEPREQD() {
            return sweepreqd;
        }

        /**
         * Sets the value of the sweepreqd property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSWEEPREQD(String value) {
            this.sweepreqd = value;
        }

        /**
         * Gets the value of the event property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEVENT() {
            return event;
        }

        /**
         * Sets the value of the event property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEVENT(String value) {
            this.event = value;
        }

        /**
         * Gets the value of the instrumentcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSTRUMENTCODE() {
            return instrumentcode;
        }

        /**
         * Sets the value of the instrumentcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSTRUMENTCODE(String value) {
            this.instrumentcode = value;
        }

        /**
         * Gets the value of the exchrate property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getEXCHRATE() {
            return exchrate;
        }

        /**
         * Sets the value of the exchrate property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setEXCHRATE(BigDecimal value) {
            this.exchrate = value;
        }

        /**
         * Gets the value of the escrow property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getESCROW() {
            return escrow;
        }

        /**
         * Sets the value of the escrow property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setESCROW(String value) {
            this.escrow = value;
        }

        /**
         * Gets the value of the eventsrno property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getEVENTSRNO() {
            return eventsrno;
        }

        /**
         * Sets the value of the eventsrno property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setEVENTSRNO(BigDecimal value) {
            this.eventsrno = value;
        }

        /**
         * Gets the value of the valuedt property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getVALUEDT() {
            return valuedt;
        }

        /**
         * Sets the value of the valuedt property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setVALUEDT(XMLGregorianCalendar value) {
            this.valuedt = value;
        }

        /**
         * Gets the value of the chequemandatory property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHEQUEMANDATORY() {
            return chequemandatory;
        }

        /**
         * Sets the value of the chequemandatory property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHEQUEMANDATORY(String value) {
            this.chequemandatory = value;
        }

        /**
         * Gets the value of the consfortrnover property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCONSFORTRNOVER() {
            return consfortrnover;
        }

        /**
         * Sets the value of the consfortrnover property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCONSFORTRNOVER(String value) {
            this.consfortrnover = value;
        }

        /**
         * Gets the value of the module property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMODULE() {
            return module;
        }

        /**
         * Sets the value of the module property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMODULE(String value) {
            this.module = value;
        }

        /**
         * Gets the value of the amttag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAMTTAG() {
            return amttag;
        }

        /**
         * Sets the value of the amttag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAMTTAG(String value) {
            this.amttag = value;
        }

        /**
         * Gets the value of the acccy property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACCCY() {
            return acccy;
        }

        /**
         * Sets the value of the acccy property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACCCY(String value) {
            this.acccy = value;
        }

        /**
         * Gets the value of the blockreleasestatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBLOCKRELEASESTATUS() {
            return blockreleasestatus;
        }

        /**
         * Sets the value of the blockreleasestatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBLOCKRELEASESTATUS(String value) {
            this.blockreleasestatus = value;
        }

        /**
         * Gets the value of the fcyamt property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getFCYAMT() {
            return fcyamt;
        }

        /**
         * Sets the value of the fcyamt property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setFCYAMT(BigDecimal value) {
            this.fcyamt = value;
        }

        /**
         * Gets the value of the grprefno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGRPREFNO() {
            return grprefno;
        }

        /**
         * Sets the value of the grprefno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGRPREFNO(String value) {
            this.grprefno = value;
        }

        /**
         * Gets the value of the consforaccactivity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCONSFORACCACTIVITY() {
            return consforaccactivity;
        }

        /**
         * Sets the value of the consforaccactivity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCONSFORACCACTIVITY(String value) {
            this.consforaccactivity = value;
        }

        /**
         * Gets the value of the ecarefno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getECAREFNO() {
            return ecarefno;
        }

        /**
         * Sets the value of the ecarefno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setECAREFNO(String value) {
            this.ecarefno = value;
        }

        /**
         * Gets the value of the acbranch property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACBRANCH() {
            return acbranch;
        }

        /**
         * Sets the value of the acbranch property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACBRANCH(String value) {
            this.acbranch = value;
        }

        /**
         * Gets the value of the acno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACNO() {
            return acno;
        }

        /**
         * Sets the value of the acno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACNO(String value) {
            this.acno = value;
        }

        /**
         * Gets the value of the relatedaccount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRELATEDACCOUNT() {
            return relatedaccount;
        }

        /**
         * Sets the value of the relatedaccount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRELATEDACCOUNT(String value) {
            this.relatedaccount = value;
        }

        /**
         * Gets the value of the lcyamt property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLCYAMT() {
            return lcyamt;
        }

        /**
         * Sets the value of the lcyamt property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLCYAMT(BigDecimal value) {
            this.lcyamt = value;
        }

        /**
         * Gets the value of the txnnarrative property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTXNNARRATIVE() {
            return txnnarrative;
        }

        /**
         * Sets the value of the txnnarrative property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTXNNARRATIVE(String value) {
            this.txnnarrative = value;
        }

        /**
         * Gets the value of the swiftcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSWIFTCODE() {
            return swiftcode;
        }

        /**
         * Sets the value of the swiftcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSWIFTCODE(String value) {
            this.swiftcode = value;
        }

        /**
         * Gets the value of the trncode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTRNCODE() {
            return trncode;
        }

        /**
         * Sets the value of the trncode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTRNCODE(String value) {
            this.trncode = value;
        }

        /**
         * Gets the value of the debitoverridetracking property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDEBITOVERRIDETRACKING() {
            return debitoverridetracking;
        }

        /**
         * Sets the value of the debitoverridetracking property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDEBITOVERRIDETRACKING(String value) {
            this.debitoverridetracking = value;
        }

        /**
         * Gets the value of the creditoverridetracking property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCREDITOVERRIDETRACKING() {
            return creditoverridetracking;
        }

        /**
         * Sets the value of the creditoverridetracking property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCREDITOVERRIDETRACKING(String value) {
            this.creditoverridetracking = value;
        }

        /**
         * Gets the value of the dontshowinstmt property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDONTSHOWINSTMT() {
            return dontshowinstmt;
        }

        /**
         * Sets the value of the dontshowinstmt property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDONTSHOWINSTMT(String value) {
            this.dontshowinstmt = value;
        }

        /**
         * Gets the value of the eventsrnotoberevd property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getEVENTSRNOTOBEREVD() {
            return eventsrnotoberevd;
        }

        /**
         * Sets the value of the eventsrnotoberevd property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setEVENTSRNOTOBEREVD(BigDecimal value) {
            this.eventsrnotoberevd = value;
        }

    }

}
