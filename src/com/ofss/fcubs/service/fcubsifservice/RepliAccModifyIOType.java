
package com.ofss.fcubs.service.fcubsifservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RepliAcc-Modify-IO-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliAcc-Modify-IO-Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BRNCDE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="REP_ACC_NUM" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ACC_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ACCCCY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ECASYSCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NO_CR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NO_DR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BLOCK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DORMANT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FROZEN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MAKER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MAKERSTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliAcc-Modify-IO-Type", propOrder = {
    "brncde",
    "repaccnum",
    "accdesc",
    "accccy",
    "ecasyscode",
    "nocr",
    "nodr",
    "block",
    "dormant",
    "frozen",
    "maker",
    "makerstamp"
})
public class RepliAccModifyIOType {

    @XmlElement(name = "BRNCDE", required = true)
    protected String brncde;
    @XmlElement(name = "REP_ACC_NUM", required = true)
    protected String repaccnum;
    @XmlElement(name = "ACC_DESC")
    protected String accdesc;
    @XmlElement(name = "ACCCCY", required = true)
    protected String accccy;
    @XmlElement(name = "ECASYSCODE")
    protected String ecasyscode;
    @XmlElement(name = "NO_CR")
    protected String nocr;
    @XmlElement(name = "NO_DR")
    protected String nodr;
    @XmlElement(name = "BLOCK")
    protected String block;
    @XmlElement(name = "DORMANT")
    protected String dormant;
    @XmlElement(name = "FROZEN")
    protected String frozen;
    @XmlElement(name = "MAKER")
    protected String maker;
    @XmlElement(name = "MAKERSTAMP")
    protected String makerstamp;

    /**
     * Gets the value of the brncde property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRNCDE() {
        return brncde;
    }

    /**
     * Sets the value of the brncde property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRNCDE(String value) {
        this.brncde = value;
    }

    /**
     * Gets the value of the repaccnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREPACCNUM() {
        return repaccnum;
    }

    /**
     * Sets the value of the repaccnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREPACCNUM(String value) {
        this.repaccnum = value;
    }

    /**
     * Gets the value of the accdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCDESC() {
        return accdesc;
    }

    /**
     * Sets the value of the accdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCDESC(String value) {
        this.accdesc = value;
    }

    /**
     * Gets the value of the accccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCCCY() {
        return accccy;
    }

    /**
     * Sets the value of the accccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCCCY(String value) {
        this.accccy = value;
    }

    /**
     * Gets the value of the ecasyscode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getECASYSCODE() {
        return ecasyscode;
    }

    /**
     * Sets the value of the ecasyscode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setECASYSCODE(String value) {
        this.ecasyscode = value;
    }

    /**
     * Gets the value of the nocr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOCR() {
        return nocr;
    }

    /**
     * Sets the value of the nocr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOCR(String value) {
        this.nocr = value;
    }

    /**
     * Gets the value of the nodr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNODR() {
        return nodr;
    }

    /**
     * Sets the value of the nodr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNODR(String value) {
        this.nodr = value;
    }

    /**
     * Gets the value of the block property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBLOCK() {
        return block;
    }

    /**
     * Sets the value of the block property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBLOCK(String value) {
        this.block = value;
    }

    /**
     * Gets the value of the dormant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDORMANT() {
        return dormant;
    }

    /**
     * Sets the value of the dormant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDORMANT(String value) {
        this.dormant = value;
    }

    /**
     * Gets the value of the frozen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFROZEN() {
        return frozen;
    }

    /**
     * Sets the value of the frozen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFROZEN(String value) {
        this.frozen = value;
    }

    /**
     * Gets the value of the maker property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAKER() {
        return maker;
    }

    /**
     * Sets the value of the maker property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAKER(String value) {
        this.maker = value;
    }

    /**
     * Gets the value of the makerstamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAKERSTAMP() {
        return makerstamp;
    }

    /**
     * Sets the value of the makerstamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAKERSTAMP(String value) {
        this.makerstamp = value;
    }

}
