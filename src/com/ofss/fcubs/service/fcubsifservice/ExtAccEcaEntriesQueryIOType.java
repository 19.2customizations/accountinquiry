
package com.ofss.fcubs.service.fcubsifservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExtAccEcaEntries-Query-IO-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtAccEcaEntries-Query-IO-Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GRPREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNIQUEEXTREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TRNREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtAccEcaEntries-Query-IO-Type", propOrder = {
    "grprefno",
    "uniqueextrefno",
    "trnrefno"
})
public class ExtAccEcaEntriesQueryIOType {

    @XmlElement(name = "GRPREFNO")
    protected String grprefno;
    @XmlElement(name = "UNIQUEEXTREFNO")
    protected String uniqueextrefno;
    @XmlElement(name = "TRNREFNO")
    protected String trnrefno;

    /**
     * Gets the value of the grprefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRPREFNO() {
        return grprefno;
    }

    /**
     * Sets the value of the grprefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRPREFNO(String value) {
        this.grprefno = value;
    }

    /**
     * Gets the value of the uniqueextrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNIQUEEXTREFNO() {
        return uniqueextrefno;
    }

    /**
     * Sets the value of the uniqueextrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNIQUEEXTREFNO(String value) {
        this.uniqueextrefno = value;
    }

    /**
     * Gets the value of the trnrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRNREFNO() {
        return trnrefno;
    }

    /**
     * Sets the value of the trnrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRNREFNO(String value) {
        this.trnrefno = value;
    }

}
