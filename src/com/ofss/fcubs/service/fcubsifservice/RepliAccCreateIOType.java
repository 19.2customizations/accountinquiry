
package com.ofss.fcubs.service.fcubsifservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RepliAcc-Create-IO-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliAcc-Create-IO-Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BRNCDE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="REP_ACC_NUM" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ACC_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUSNO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ACCCCY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ACCOUNT_CLASS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CRDLIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AC_OPEN_DATE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="NATIVE_ACC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IBANACNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ALTACC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ECASYSCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CLRACCNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NO_CR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NO_DR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BLOCK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DORMANT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FROZEN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliAcc-Create-IO-Type", propOrder = {
    "brncde",
    "repaccnum",
    "accdesc",
    "cusno",
    "accccy",
    "accountclass",
    "crdlin",
    "acopendate",
    "nativeacc",
    "ibanacno",
    "altacc",
    "ecasyscode",
    "clraccno",
    "nocr",
    "nodr",
    "block",
    "dormant",
    "frozen"
})
public class RepliAccCreateIOType {

    @XmlElement(name = "BRNCDE", required = true)
    protected String brncde;
    @XmlElement(name = "REP_ACC_NUM", required = true)
    protected String repaccnum;
    @XmlElement(name = "ACC_DESC")
    protected String accdesc;
    @XmlElement(name = "CUSNO", required = true)
    protected String cusno;
    @XmlElement(name = "ACCCCY", required = true)
    protected String accccy;
    @XmlElement(name = "ACCOUNT_CLASS", required = true)
    protected String accountclass;
    @XmlElement(name = "CRDLIN")
    protected String crdlin;
    @XmlElement(name = "AC_OPEN_DATE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar acopendate;
    @XmlElement(name = "NATIVE_ACC", required = true)
    protected String nativeacc;
    @XmlElement(name = "IBANACNO")
    protected String ibanacno;
    @XmlElement(name = "ALTACC")
    protected String altacc;
    @XmlElement(name = "ECASYSCODE")
    protected String ecasyscode;
    @XmlElement(name = "CLRACCNO")
    protected String clraccno;
    @XmlElement(name = "NO_CR")
    protected String nocr;
    @XmlElement(name = "NO_DR")
    protected String nodr;
    @XmlElement(name = "BLOCK")
    protected String block;
    @XmlElement(name = "DORMANT")
    protected String dormant;
    @XmlElement(name = "FROZEN")
    protected String frozen;

    /**
     * Gets the value of the brncde property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRNCDE() {
        return brncde;
    }

    /**
     * Sets the value of the brncde property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRNCDE(String value) {
        this.brncde = value;
    }

    /**
     * Gets the value of the repaccnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREPACCNUM() {
        return repaccnum;
    }

    /**
     * Sets the value of the repaccnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREPACCNUM(String value) {
        this.repaccnum = value;
    }

    /**
     * Gets the value of the accdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCDESC() {
        return accdesc;
    }

    /**
     * Sets the value of the accdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCDESC(String value) {
        this.accdesc = value;
    }

    /**
     * Gets the value of the cusno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSNO() {
        return cusno;
    }

    /**
     * Sets the value of the cusno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSNO(String value) {
        this.cusno = value;
    }

    /**
     * Gets the value of the accccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCCCY() {
        return accccy;
    }

    /**
     * Sets the value of the accccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCCCY(String value) {
        this.accccy = value;
    }

    /**
     * Gets the value of the accountclass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCOUNTCLASS() {
        return accountclass;
    }

    /**
     * Sets the value of the accountclass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCOUNTCLASS(String value) {
        this.accountclass = value;
    }

    /**
     * Gets the value of the crdlin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRDLIN() {
        return crdlin;
    }

    /**
     * Sets the value of the crdlin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRDLIN(String value) {
        this.crdlin = value;
    }

    /**
     * Gets the value of the acopendate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getACOPENDATE() {
        return acopendate;
    }

    /**
     * Sets the value of the acopendate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setACOPENDATE(XMLGregorianCalendar value) {
        this.acopendate = value;
    }

    /**
     * Gets the value of the nativeacc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNATIVEACC() {
        return nativeacc;
    }

    /**
     * Sets the value of the nativeacc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNATIVEACC(String value) {
        this.nativeacc = value;
    }

    /**
     * Gets the value of the ibanacno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIBANACNO() {
        return ibanacno;
    }

    /**
     * Sets the value of the ibanacno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIBANACNO(String value) {
        this.ibanacno = value;
    }

    /**
     * Gets the value of the altacc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getALTACC() {
        return altacc;
    }

    /**
     * Sets the value of the altacc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setALTACC(String value) {
        this.altacc = value;
    }

    /**
     * Gets the value of the ecasyscode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getECASYSCODE() {
        return ecasyscode;
    }

    /**
     * Sets the value of the ecasyscode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setECASYSCODE(String value) {
        this.ecasyscode = value;
    }

    /**
     * Gets the value of the clraccno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLRACCNO() {
        return clraccno;
    }

    /**
     * Sets the value of the clraccno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLRACCNO(String value) {
        this.clraccno = value;
    }

    /**
     * Gets the value of the nocr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOCR() {
        return nocr;
    }

    /**
     * Sets the value of the nocr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOCR(String value) {
        this.nocr = value;
    }

    /**
     * Gets the value of the nodr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNODR() {
        return nodr;
    }

    /**
     * Sets the value of the nodr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNODR(String value) {
        this.nodr = value;
    }

    /**
     * Gets the value of the block property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBLOCK() {
        return block;
    }

    /**
     * Sets the value of the block property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBLOCK(String value) {
        this.block = value;
    }

    /**
     * Gets the value of the dormant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDORMANT() {
        return dormant;
    }

    /**
     * Sets the value of the dormant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDORMANT(String value) {
        this.dormant = value;
    }

    /**
     * Gets the value of the frozen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFROZEN() {
        return frozen;
    }

    /**
     * Sets the value of the frozen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFROZEN(String value) {
        this.frozen = value;
    }

}
