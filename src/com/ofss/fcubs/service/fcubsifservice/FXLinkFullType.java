
package com.ofss.fcubs.service.fcubsifservice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FXLink-Full-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FXLink-Full-Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FX_REFERENCE_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LINKED_REFERENCE_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MODULE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ACTION" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="COUNTERPARTY" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="LINKED_SEQ_NO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="LINKED_PRODUCT_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LINKED_CUST_AC_CCY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LINKED_TXN_CCY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LINKED_FCY_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="LINKED_TXN_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="LINKED_EVENT_DATE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="FX_EXRATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="SOURCE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EXTERNAL_REF_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FXLink-Full-Type", propOrder = {
    "fxreferenceno",
    "linkedreferenceno",
    "modulecode",
    "action",
    "counterparty",
    "linkedseqno",
    "linkedproducttype",
    "linkedcustacccy",
    "linkedtxnccy",
    "linkedfcyamt",
    "linkedtxnamt",
    "linkedeventdate",
    "fxexrate",
    "source",
    "externalrefno"
})
public class FXLinkFullType {

    @XmlElement(name = "FX_REFERENCE_NO", required = true)
    protected String fxreferenceno;
    @XmlElement(name = "LINKED_REFERENCE_NO", required = true)
    protected String linkedreferenceno;
    @XmlElement(name = "MODULE_CODE")
    protected String modulecode;
    @XmlElement(name = "ACTION", required = true)
    protected String action;
    @XmlElement(name = "COUNTERPARTY", required = true)
    protected BigDecimal counterparty;
    @XmlElement(name = "LINKED_SEQ_NO")
    protected BigDecimal linkedseqno;
    @XmlElement(name = "LINKED_PRODUCT_TYPE", required = true)
    protected String linkedproducttype;
    @XmlElement(name = "LINKED_CUST_AC_CCY", required = true)
    protected String linkedcustacccy;
    @XmlElement(name = "LINKED_TXN_CCY", required = true)
    protected String linkedtxnccy;
    @XmlElement(name = "LINKED_FCY_AMT")
    protected BigDecimal linkedfcyamt;
    @XmlElement(name = "LINKED_TXN_AMT")
    protected BigDecimal linkedtxnamt;
    @XmlElement(name = "LINKED_EVENT_DATE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar linkedeventdate;
    @XmlElement(name = "FX_EXRATE")
    protected BigDecimal fxexrate;
    @XmlElement(name = "SOURCE", required = true)
    protected String source;
    @XmlElement(name = "EXTERNAL_REF_NO", required = true)
    protected String externalrefno;

    /**
     * Gets the value of the fxreferenceno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFXREFERENCENO() {
        return fxreferenceno;
    }

    /**
     * Sets the value of the fxreferenceno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFXREFERENCENO(String value) {
        this.fxreferenceno = value;
    }

    /**
     * Gets the value of the linkedreferenceno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINKEDREFERENCENO() {
        return linkedreferenceno;
    }

    /**
     * Sets the value of the linkedreferenceno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINKEDREFERENCENO(String value) {
        this.linkedreferenceno = value;
    }

    /**
     * Gets the value of the modulecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODULECODE() {
        return modulecode;
    }

    /**
     * Sets the value of the modulecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODULECODE(String value) {
        this.modulecode = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACTION() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACTION(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the counterparty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCOUNTERPARTY() {
        return counterparty;
    }

    /**
     * Sets the value of the counterparty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCOUNTERPARTY(BigDecimal value) {
        this.counterparty = value;
    }

    /**
     * Gets the value of the linkedseqno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLINKEDSEQNO() {
        return linkedseqno;
    }

    /**
     * Sets the value of the linkedseqno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLINKEDSEQNO(BigDecimal value) {
        this.linkedseqno = value;
    }

    /**
     * Gets the value of the linkedproducttype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINKEDPRODUCTTYPE() {
        return linkedproducttype;
    }

    /**
     * Sets the value of the linkedproducttype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINKEDPRODUCTTYPE(String value) {
        this.linkedproducttype = value;
    }

    /**
     * Gets the value of the linkedcustacccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINKEDCUSTACCCY() {
        return linkedcustacccy;
    }

    /**
     * Sets the value of the linkedcustacccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINKEDCUSTACCCY(String value) {
        this.linkedcustacccy = value;
    }

    /**
     * Gets the value of the linkedtxnccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINKEDTXNCCY() {
        return linkedtxnccy;
    }

    /**
     * Sets the value of the linkedtxnccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINKEDTXNCCY(String value) {
        this.linkedtxnccy = value;
    }

    /**
     * Gets the value of the linkedfcyamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLINKEDFCYAMT() {
        return linkedfcyamt;
    }

    /**
     * Sets the value of the linkedfcyamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLINKEDFCYAMT(BigDecimal value) {
        this.linkedfcyamt = value;
    }

    /**
     * Gets the value of the linkedtxnamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLINKEDTXNAMT() {
        return linkedtxnamt;
    }

    /**
     * Sets the value of the linkedtxnamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLINKEDTXNAMT(BigDecimal value) {
        this.linkedtxnamt = value;
    }

    /**
     * Gets the value of the linkedeventdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLINKEDEVENTDATE() {
        return linkedeventdate;
    }

    /**
     * Sets the value of the linkedeventdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLINKEDEVENTDATE(XMLGregorianCalendar value) {
        this.linkedeventdate = value;
    }

    /**
     * Gets the value of the fxexrate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFXEXRATE() {
        return fxexrate;
    }

    /**
     * Sets the value of the fxexrate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFXEXRATE(BigDecimal value) {
        this.fxexrate = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOURCE() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOURCE(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the externalrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXTERNALREFNO() {
        return externalrefno;
    }

    /**
     * Sets the value of the externalrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXTERNALREFNO(String value) {
        this.externalrefno = value;
    }

}
