
package com.ofss.fcubs.service.fcubsifservice;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ofss.fcubs.service.fcubsifservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ofss.fcubs.service.fcubsifservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AUTHORIZEREPLIACCFSFSREQ }
     * 
     */
    public AUTHORIZEREPLIACCFSFSREQ createAUTHORIZEREPLIACCFSFSREQ() {
        return new AUTHORIZEREPLIACCFSFSREQ();
    }

    /**
     * Create an instance of {@link AUTHORIZEREPLIACCFSFSRES }
     * 
     */
    public AUTHORIZEREPLIACCFSFSRES createAUTHORIZEREPLIACCFSFSRES() {
        return new AUTHORIZEREPLIACCFSFSRES();
    }

    /**
     * Create an instance of {@link AUTHORIZEREPLIACCIOPKREQ }
     * 
     */
    public AUTHORIZEREPLIACCIOPKREQ createAUTHORIZEREPLIACCIOPKREQ() {
        return new AUTHORIZEREPLIACCIOPKREQ();
    }

    /**
     * Create an instance of {@link AUTHORIZEREPLIACCIOPKRES }
     * 
     */
    public AUTHORIZEREPLIACCIOPKRES createAUTHORIZEREPLIACCIOPKRES() {
        return new AUTHORIZEREPLIACCIOPKRES();
    }

    /**
     * Create an instance of {@link AUTHREPACCFSFSREQ }
     * 
     */
    public AUTHREPACCFSFSREQ createAUTHREPACCFSFSREQ() {
        return new AUTHREPACCFSFSREQ();
    }

    /**
     * Create an instance of {@link AUTHREPACCFSFSRES }
     * 
     */
    public AUTHREPACCFSFSRES createAUTHREPACCFSFSRES() {
        return new AUTHREPACCFSFSRES();
    }

    /**
     * Create an instance of {@link AUTHREPACCIOPKREQ }
     * 
     */
    public AUTHREPACCIOPKREQ createAUTHREPACCIOPKREQ() {
        return new AUTHREPACCIOPKREQ();
    }

    /**
     * Create an instance of {@link AUTHREPACCIOPKRES }
     * 
     */
    public AUTHREPACCIOPKRES createAUTHREPACCIOPKRES() {
        return new AUTHREPACCIOPKRES();
    }

    /**
     * Create an instance of {@link CREATEEXTACCECAENTRIESFSFSREQ }
     * 
     */
    public CREATEEXTACCECAENTRIESFSFSREQ createCREATEEXTACCECAENTRIESFSFSREQ() {
        return new CREATEEXTACCECAENTRIESFSFSREQ();
    }

    /**
     * Create an instance of {@link CREATEEXTACCECAENTRIESFSFSRES }
     * 
     */
    public CREATEEXTACCECAENTRIESFSFSRES createCREATEEXTACCECAENTRIESFSFSRES() {
        return new CREATEEXTACCECAENTRIESFSFSRES();
    }

    /**
     * Create an instance of {@link CREATEEXTACCECAENTRIESIOPKREQ }
     * 
     */
    public CREATEEXTACCECAENTRIESIOPKREQ createCREATEEXTACCECAENTRIESIOPKREQ() {
        return new CREATEEXTACCECAENTRIESIOPKREQ();
    }

    /**
     * Create an instance of {@link CREATEEXTACCECAENTRIESIOPKRES }
     * 
     */
    public CREATEEXTACCECAENTRIESIOPKRES createCREATEEXTACCECAENTRIESIOPKRES() {
        return new CREATEEXTACCECAENTRIESIOPKRES();
    }

    /**
     * Create an instance of {@link CREATEEXTACCENTRIESFSFSREQ }
     * 
     */
    public CREATEEXTACCENTRIESFSFSREQ createCREATEEXTACCENTRIESFSFSREQ() {
        return new CREATEEXTACCENTRIESFSFSREQ();
    }

    /**
     * Create an instance of {@link CREATEEXTACCENTRIESFSFSRES }
     * 
     */
    public CREATEEXTACCENTRIESFSFSRES createCREATEEXTACCENTRIESFSFSRES() {
        return new CREATEEXTACCENTRIESFSFSRES();
    }

    /**
     * Create an instance of {@link CREATEEXTACCENTRIESIOPKREQ }
     * 
     */
    public CREATEEXTACCENTRIESIOPKREQ createCREATEEXTACCENTRIESIOPKREQ() {
        return new CREATEEXTACCENTRIESIOPKREQ();
    }

    /**
     * Create an instance of {@link CREATEEXTACCENTRIESIOPKRES }
     * 
     */
    public CREATEEXTACCENTRIESIOPKRES createCREATEEXTACCENTRIESIOPKRES() {
        return new CREATEEXTACCENTRIESIOPKRES();
    }

    /**
     * Create an instance of {@link CREATEFXLINKFSFSREQ }
     * 
     */
    public CREATEFXLINKFSFSREQ createCREATEFXLINKFSFSREQ() {
        return new CREATEFXLINKFSFSREQ();
    }

    /**
     * Create an instance of {@link CREATEFXLINKFSFSRES }
     * 
     */
    public CREATEFXLINKFSFSRES createCREATEFXLINKFSFSRES() {
        return new CREATEFXLINKFSFSRES();
    }

    /**
     * Create an instance of {@link CREATEFXLINKIOPKREQ }
     * 
     */
    public CREATEFXLINKIOPKREQ createCREATEFXLINKIOPKREQ() {
        return new CREATEFXLINKIOPKREQ();
    }

    /**
     * Create an instance of {@link CREATEFXLINKIOPKRES }
     * 
     */
    public CREATEFXLINKIOPKRES createCREATEFXLINKIOPKRES() {
        return new CREATEFXLINKIOPKRES();
    }

    /**
     * Create an instance of {@link CREATEOLCONTRACTFSFSREQ }
     * 
     */
    public CREATEOLCONTRACTFSFSREQ createCREATEOLCONTRACTFSFSREQ() {
        return new CREATEOLCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link CREATEOLCONTRACTFSFSRES }
     * 
     */
    public CREATEOLCONTRACTFSFSRES createCREATEOLCONTRACTFSFSRES() {
        return new CREATEOLCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link CREATEOLCONTRACTIOPKREQ }
     * 
     */
    public CREATEOLCONTRACTIOPKREQ createCREATEOLCONTRACTIOPKREQ() {
        return new CREATEOLCONTRACTIOPKREQ();
    }

    /**
     * Create an instance of {@link CREATEOLCONTRACTIOPKRES }
     * 
     */
    public CREATEOLCONTRACTIOPKRES createCREATEOLCONTRACTIOPKRES() {
        return new CREATEOLCONTRACTIOPKRES();
    }

    /**
     * Create an instance of {@link CREATEREPACCFSFSREQ }
     * 
     */
    public CREATEREPACCFSFSREQ createCREATEREPACCFSFSREQ() {
        return new CREATEREPACCFSFSREQ();
    }

    /**
     * Create an instance of {@link CREATEREPACCFSFSRES }
     * 
     */
    public CREATEREPACCFSFSRES createCREATEREPACCFSFSRES() {
        return new CREATEREPACCFSFSRES();
    }

    /**
     * Create an instance of {@link CREATEREPACCIOPKREQ }
     * 
     */
    public CREATEREPACCIOPKREQ createCREATEREPACCIOPKREQ() {
        return new CREATEREPACCIOPKREQ();
    }

    /**
     * Create an instance of {@link CREATEREPACCIOPKRES }
     * 
     */
    public CREATEREPACCIOPKRES createCREATEREPACCIOPKRES() {
        return new CREATEREPACCIOPKRES();
    }

    /**
     * Create an instance of {@link CREATEREPLIACCFSFSREQ }
     * 
     */
    public CREATEREPLIACCFSFSREQ createCREATEREPLIACCFSFSREQ() {
        return new CREATEREPLIACCFSFSREQ();
    }

    /**
     * Create an instance of {@link CREATEREPLIACCFSFSRES }
     * 
     */
    public CREATEREPLIACCFSFSRES createCREATEREPLIACCFSFSRES() {
        return new CREATEREPLIACCFSFSRES();
    }

    /**
     * Create an instance of {@link CREATEREPLIACCIOPKREQ }
     * 
     */
    public CREATEREPLIACCIOPKREQ createCREATEREPLIACCIOPKREQ() {
        return new CREATEREPLIACCIOPKREQ();
    }

    /**
     * Create an instance of {@link CREATEREPLIACCIOPKRES }
     * 
     */
    public CREATEREPLIACCIOPKRES createCREATEREPLIACCIOPKRES() {
        return new CREATEREPLIACCIOPKRES();
    }

    /**
     * Create an instance of {@link DELETEOLCONTRACTFSFSREQ }
     * 
     */
    public DELETEOLCONTRACTFSFSREQ createDELETEOLCONTRACTFSFSREQ() {
        return new DELETEOLCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link DELETEOLCONTRACTFSFSRES }
     * 
     */
    public DELETEOLCONTRACTFSFSRES createDELETEOLCONTRACTFSFSRES() {
        return new DELETEOLCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link DELETEOLCONTRACTIOPKREQ }
     * 
     */
    public DELETEOLCONTRACTIOPKREQ createDELETEOLCONTRACTIOPKREQ() {
        return new DELETEOLCONTRACTIOPKREQ();
    }

    /**
     * Create an instance of {@link DELETEOLCONTRACTIOPKRES }
     * 
     */
    public DELETEOLCONTRACTIOPKRES createDELETEOLCONTRACTIOPKRES() {
        return new DELETEOLCONTRACTIOPKRES();
    }

    /**
     * Create an instance of {@link DELETEREPACCFSFSREQ }
     * 
     */
    public DELETEREPACCFSFSREQ createDELETEREPACCFSFSREQ() {
        return new DELETEREPACCFSFSREQ();
    }

    /**
     * Create an instance of {@link DELETEREPACCFSFSRES }
     * 
     */
    public DELETEREPACCFSFSRES createDELETEREPACCFSFSRES() {
        return new DELETEREPACCFSFSRES();
    }

    /**
     * Create an instance of {@link DELETEREPACCIOPKREQ }
     * 
     */
    public DELETEREPACCIOPKREQ createDELETEREPACCIOPKREQ() {
        return new DELETEREPACCIOPKREQ();
    }

    /**
     * Create an instance of {@link DELETEREPACCIOPKRES }
     * 
     */
    public DELETEREPACCIOPKRES createDELETEREPACCIOPKRES() {
        return new DELETEREPACCIOPKRES();
    }

    /**
     * Create an instance of {@link DELETEREPLIACCFSFSREQ }
     * 
     */
    public DELETEREPLIACCFSFSREQ createDELETEREPLIACCFSFSREQ() {
        return new DELETEREPLIACCFSFSREQ();
    }

    /**
     * Create an instance of {@link DELETEREPLIACCFSFSRES }
     * 
     */
    public DELETEREPLIACCFSFSRES createDELETEREPLIACCFSFSRES() {
        return new DELETEREPLIACCFSFSRES();
    }

    /**
     * Create an instance of {@link DELETEREPLIACCIOPKREQ }
     * 
     */
    public DELETEREPLIACCIOPKREQ createDELETEREPLIACCIOPKREQ() {
        return new DELETEREPLIACCIOPKREQ();
    }

    /**
     * Create an instance of {@link DELETEREPLIACCIOPKRES }
     * 
     */
    public DELETEREPLIACCIOPKRES createDELETEREPLIACCIOPKRES() {
        return new DELETEREPLIACCIOPKRES();
    }

    /**
     * Create an instance of {@link MODIFYOLCONTRACTFSFSREQ }
     * 
     */
    public MODIFYOLCONTRACTFSFSREQ createMODIFYOLCONTRACTFSFSREQ() {
        return new MODIFYOLCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link MODIFYOLCONTRACTFSFSRES }
     * 
     */
    public MODIFYOLCONTRACTFSFSRES createMODIFYOLCONTRACTFSFSRES() {
        return new MODIFYOLCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link MODIFYOLCONTRACTIOPKREQ }
     * 
     */
    public MODIFYOLCONTRACTIOPKREQ createMODIFYOLCONTRACTIOPKREQ() {
        return new MODIFYOLCONTRACTIOPKREQ();
    }

    /**
     * Create an instance of {@link MODIFYOLCONTRACTIOPKRES }
     * 
     */
    public MODIFYOLCONTRACTIOPKRES createMODIFYOLCONTRACTIOPKRES() {
        return new MODIFYOLCONTRACTIOPKRES();
    }

    /**
     * Create an instance of {@link MODIFYREPACCFSFSREQ }
     * 
     */
    public MODIFYREPACCFSFSREQ createMODIFYREPACCFSFSREQ() {
        return new MODIFYREPACCFSFSREQ();
    }

    /**
     * Create an instance of {@link MODIFYREPACCFSFSRES }
     * 
     */
    public MODIFYREPACCFSFSRES createMODIFYREPACCFSFSRES() {
        return new MODIFYREPACCFSFSRES();
    }

    /**
     * Create an instance of {@link MODIFYREPACCIOPKREQ }
     * 
     */
    public MODIFYREPACCIOPKREQ createMODIFYREPACCIOPKREQ() {
        return new MODIFYREPACCIOPKREQ();
    }

    /**
     * Create an instance of {@link MODIFYREPACCIOPKRES }
     * 
     */
    public MODIFYREPACCIOPKRES createMODIFYREPACCIOPKRES() {
        return new MODIFYREPACCIOPKRES();
    }

    /**
     * Create an instance of {@link MODIFYREPLIACCFSFSREQ }
     * 
     */
    public MODIFYREPLIACCFSFSREQ createMODIFYREPLIACCFSFSREQ() {
        return new MODIFYREPLIACCFSFSREQ();
    }

    /**
     * Create an instance of {@link MODIFYREPLIACCFSFSRES }
     * 
     */
    public MODIFYREPLIACCFSFSRES createMODIFYREPLIACCFSFSRES() {
        return new MODIFYREPLIACCFSFSRES();
    }

    /**
     * Create an instance of {@link MODIFYREPLIACCIOPKREQ }
     * 
     */
    public MODIFYREPLIACCIOPKREQ createMODIFYREPLIACCIOPKREQ() {
        return new MODIFYREPLIACCIOPKREQ();
    }

    /**
     * Create an instance of {@link MODIFYREPLIACCIOPKRES }
     * 
     */
    public MODIFYREPLIACCIOPKRES createMODIFYREPLIACCIOPKRES() {
        return new MODIFYREPLIACCIOPKRES();
    }

    /**
     * Create an instance of {@link QUERYEXTACCECAENTRIESIOFSREQ }
     * 
     */
    public QUERYEXTACCECAENTRIESIOFSREQ createQUERYEXTACCECAENTRIESIOFSREQ() {
        return new QUERYEXTACCECAENTRIESIOFSREQ();
    }

    /**
     * Create an instance of {@link QUERYEXTACCECAENTRIESIOFSRES }
     * 
     */
    public QUERYEXTACCECAENTRIESIOFSRES createQUERYEXTACCECAENTRIESIOFSRES() {
        return new QUERYEXTACCECAENTRIESIOFSRES();
    }

    /**
     * Create an instance of {@link QUERYREPACCIOFSREQ }
     * 
     */
    public QUERYREPACCIOFSREQ createQUERYREPACCIOFSREQ() {
        return new QUERYREPACCIOFSREQ();
    }

    /**
     * Create an instance of {@link QUERYREPACCIOFSRES }
     * 
     */
    public QUERYREPACCIOFSRES createQUERYREPACCIOFSRES() {
        return new QUERYREPACCIOFSRES();
    }

    /**
     * Create an instance of {@link QUERYREPLIACCIOFSREQ }
     * 
     */
    public QUERYREPLIACCIOFSREQ createQUERYREPLIACCIOFSREQ() {
        return new QUERYREPLIACCIOFSREQ();
    }

    /**
     * Create an instance of {@link QUERYREPLIACCIOFSRES }
     * 
     */
    public QUERYREPLIACCIOFSRES createQUERYREPLIACCIOFSRES() {
        return new QUERYREPLIACCIOFSRES();
    }

    /**
     * Create an instance of {@link REVERSEEXTACCENTRIESFSFSREQ }
     * 
     */
    public REVERSEEXTACCENTRIESFSFSREQ createREVERSEEXTACCENTRIESFSFSREQ() {
        return new REVERSEEXTACCENTRIESFSFSREQ();
    }

    /**
     * Create an instance of {@link REVERSEEXTACCENTRIESFSFSRES }
     * 
     */
    public REVERSEEXTACCENTRIESFSFSRES createREVERSEEXTACCENTRIESFSFSRES() {
        return new REVERSEEXTACCENTRIESFSFSRES();
    }

    /**
     * Create an instance of {@link REVERSEEXTACCENTRIESIOPKREQ }
     * 
     */
    public REVERSEEXTACCENTRIESIOPKREQ createREVERSEEXTACCENTRIESIOPKREQ() {
        return new REVERSEEXTACCENTRIESIOPKREQ();
    }

    /**
     * Create an instance of {@link REVERSEEXTACCENTRIESIOPKRES }
     * 
     */
    public REVERSEEXTACCENTRIESIOPKRES createREVERSEEXTACCENTRIESIOPKRES() {
        return new REVERSEEXTACCENTRIESIOPKRES();
    }

    /**
     * Create an instance of {@link ExtAccEcaEntriesFullType }
     * 
     */
    public ExtAccEcaEntriesFullType createExtAccEcaEntriesFullType() {
        return new ExtAccEcaEntriesFullType();
    }

    /**
     * Create an instance of {@link ExtAccEcaEntriesCreateIOType }
     * 
     */
    public ExtAccEcaEntriesCreateIOType createExtAccEcaEntriesCreateIOType() {
        return new ExtAccEcaEntriesCreateIOType();
    }

    /**
     * Create an instance of {@link ExtAccEntriesFullType }
     * 
     */
    public ExtAccEntriesFullType createExtAccEntriesFullType() {
        return new ExtAccEntriesFullType();
    }

    /**
     * Create an instance of {@link ExtAccEntriesCreateIOType }
     * 
     */
    public ExtAccEntriesCreateIOType createExtAccEntriesCreateIOType() {
        return new ExtAccEntriesCreateIOType();
    }

    /**
     * Create an instance of {@link OLContractFullType }
     * 
     */
    public OLContractFullType createOLContractFullType() {
        return new OLContractFullType();
    }

    /**
     * Create an instance of {@link OLContractCreateIOType }
     * 
     */
    public OLContractCreateIOType createOLContractCreateIOType() {
        return new OLContractCreateIOType();
    }

    /**
     * Create an instance of {@link FCUBSHEADERType }
     * 
     */
    public FCUBSHEADERType createFCUBSHEADERType() {
        return new FCUBSHEADERType();
    }

    /**
     * Create an instance of {@link FCUBSHEADERType.ADDL }
     * 
     */
    public FCUBSHEADERType.ADDL createFCUBSHEADERTypeADDL() {
        return new FCUBSHEADERType.ADDL();
    }

    /**
     * Create an instance of {@link AUTHORIZEREPLIACCFSFSREQ.FCUBSBODY }
     * 
     */
    public AUTHORIZEREPLIACCFSFSREQ.FCUBSBODY createAUTHORIZEREPLIACCFSFSREQFCUBSBODY() {
        return new AUTHORIZEREPLIACCFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEREPLIACCFSFSRES.FCUBSBODY }
     * 
     */
    public AUTHORIZEREPLIACCFSFSRES.FCUBSBODY createAUTHORIZEREPLIACCFSFSRESFCUBSBODY() {
        return new AUTHORIZEREPLIACCFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEREPLIACCIOPKREQ.FCUBSBODY }
     * 
     */
    public AUTHORIZEREPLIACCIOPKREQ.FCUBSBODY createAUTHORIZEREPLIACCIOPKREQFCUBSBODY() {
        return new AUTHORIZEREPLIACCIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEREPLIACCIOPKRES.FCUBSBODY }
     * 
     */
    public AUTHORIZEREPLIACCIOPKRES.FCUBSBODY createAUTHORIZEREPLIACCIOPKRESFCUBSBODY() {
        return new AUTHORIZEREPLIACCIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHREPACCFSFSREQ.FCUBSBODY }
     * 
     */
    public AUTHREPACCFSFSREQ.FCUBSBODY createAUTHREPACCFSFSREQFCUBSBODY() {
        return new AUTHREPACCFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHREPACCFSFSRES.FCUBSBODY }
     * 
     */
    public AUTHREPACCFSFSRES.FCUBSBODY createAUTHREPACCFSFSRESFCUBSBODY() {
        return new AUTHREPACCFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHREPACCIOPKREQ.FCUBSBODY }
     * 
     */
    public AUTHREPACCIOPKREQ.FCUBSBODY createAUTHREPACCIOPKREQFCUBSBODY() {
        return new AUTHREPACCIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHREPACCIOPKRES.FCUBSBODY }
     * 
     */
    public AUTHREPACCIOPKRES.FCUBSBODY createAUTHREPACCIOPKRESFCUBSBODY() {
        return new AUTHREPACCIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEEXTACCECAENTRIESFSFSREQ.FCUBSBODY }
     * 
     */
    public CREATEEXTACCECAENTRIESFSFSREQ.FCUBSBODY createCREATEEXTACCECAENTRIESFSFSREQFCUBSBODY() {
        return new CREATEEXTACCECAENTRIESFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEEXTACCECAENTRIESFSFSRES.FCUBSBODY }
     * 
     */
    public CREATEEXTACCECAENTRIESFSFSRES.FCUBSBODY createCREATEEXTACCECAENTRIESFSFSRESFCUBSBODY() {
        return new CREATEEXTACCECAENTRIESFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEEXTACCECAENTRIESIOPKREQ.FCUBSBODY }
     * 
     */
    public CREATEEXTACCECAENTRIESIOPKREQ.FCUBSBODY createCREATEEXTACCECAENTRIESIOPKREQFCUBSBODY() {
        return new CREATEEXTACCECAENTRIESIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEEXTACCECAENTRIESIOPKRES.FCUBSBODY }
     * 
     */
    public CREATEEXTACCECAENTRIESIOPKRES.FCUBSBODY createCREATEEXTACCECAENTRIESIOPKRESFCUBSBODY() {
        return new CREATEEXTACCECAENTRIESIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEEXTACCENTRIESFSFSREQ.FCUBSBODY }
     * 
     */
    public CREATEEXTACCENTRIESFSFSREQ.FCUBSBODY createCREATEEXTACCENTRIESFSFSREQFCUBSBODY() {
        return new CREATEEXTACCENTRIESFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEEXTACCENTRIESFSFSRES.FCUBSBODY }
     * 
     */
    public CREATEEXTACCENTRIESFSFSRES.FCUBSBODY createCREATEEXTACCENTRIESFSFSRESFCUBSBODY() {
        return new CREATEEXTACCENTRIESFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEEXTACCENTRIESIOPKREQ.FCUBSBODY }
     * 
     */
    public CREATEEXTACCENTRIESIOPKREQ.FCUBSBODY createCREATEEXTACCENTRIESIOPKREQFCUBSBODY() {
        return new CREATEEXTACCENTRIESIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEEXTACCENTRIESIOPKRES.FCUBSBODY }
     * 
     */
    public CREATEEXTACCENTRIESIOPKRES.FCUBSBODY createCREATEEXTACCENTRIESIOPKRESFCUBSBODY() {
        return new CREATEEXTACCENTRIESIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEFXLINKFSFSREQ.FCUBSBODY }
     * 
     */
    public CREATEFXLINKFSFSREQ.FCUBSBODY createCREATEFXLINKFSFSREQFCUBSBODY() {
        return new CREATEFXLINKFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEFXLINKFSFSRES.FCUBSBODY }
     * 
     */
    public CREATEFXLINKFSFSRES.FCUBSBODY createCREATEFXLINKFSFSRESFCUBSBODY() {
        return new CREATEFXLINKFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEFXLINKIOPKREQ.FCUBSBODY }
     * 
     */
    public CREATEFXLINKIOPKREQ.FCUBSBODY createCREATEFXLINKIOPKREQFCUBSBODY() {
        return new CREATEFXLINKIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEFXLINKIOPKRES.FCUBSBODY }
     * 
     */
    public CREATEFXLINKIOPKRES.FCUBSBODY createCREATEFXLINKIOPKRESFCUBSBODY() {
        return new CREATEFXLINKIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEOLCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public CREATEOLCONTRACTFSFSREQ.FCUBSBODY createCREATEOLCONTRACTFSFSREQFCUBSBODY() {
        return new CREATEOLCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEOLCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public CREATEOLCONTRACTFSFSRES.FCUBSBODY createCREATEOLCONTRACTFSFSRESFCUBSBODY() {
        return new CREATEOLCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEOLCONTRACTIOPKREQ.FCUBSBODY }
     * 
     */
    public CREATEOLCONTRACTIOPKREQ.FCUBSBODY createCREATEOLCONTRACTIOPKREQFCUBSBODY() {
        return new CREATEOLCONTRACTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEOLCONTRACTIOPKRES.FCUBSBODY }
     * 
     */
    public CREATEOLCONTRACTIOPKRES.FCUBSBODY createCREATEOLCONTRACTIOPKRESFCUBSBODY() {
        return new CREATEOLCONTRACTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEREPACCFSFSREQ.FCUBSBODY }
     * 
     */
    public CREATEREPACCFSFSREQ.FCUBSBODY createCREATEREPACCFSFSREQFCUBSBODY() {
        return new CREATEREPACCFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEREPACCFSFSRES.FCUBSBODY }
     * 
     */
    public CREATEREPACCFSFSRES.FCUBSBODY createCREATEREPACCFSFSRESFCUBSBODY() {
        return new CREATEREPACCFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEREPACCIOPKREQ.FCUBSBODY }
     * 
     */
    public CREATEREPACCIOPKREQ.FCUBSBODY createCREATEREPACCIOPKREQFCUBSBODY() {
        return new CREATEREPACCIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEREPACCIOPKRES.FCUBSBODY }
     * 
     */
    public CREATEREPACCIOPKRES.FCUBSBODY createCREATEREPACCIOPKRESFCUBSBODY() {
        return new CREATEREPACCIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEREPLIACCFSFSREQ.FCUBSBODY }
     * 
     */
    public CREATEREPLIACCFSFSREQ.FCUBSBODY createCREATEREPLIACCFSFSREQFCUBSBODY() {
        return new CREATEREPLIACCFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEREPLIACCFSFSRES.FCUBSBODY }
     * 
     */
    public CREATEREPLIACCFSFSRES.FCUBSBODY createCREATEREPLIACCFSFSRESFCUBSBODY() {
        return new CREATEREPLIACCFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEREPLIACCIOPKREQ.FCUBSBODY }
     * 
     */
    public CREATEREPLIACCIOPKREQ.FCUBSBODY createCREATEREPLIACCIOPKREQFCUBSBODY() {
        return new CREATEREPLIACCIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEREPLIACCIOPKRES.FCUBSBODY }
     * 
     */
    public CREATEREPLIACCIOPKRES.FCUBSBODY createCREATEREPLIACCIOPKRESFCUBSBODY() {
        return new CREATEREPLIACCIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEOLCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public DELETEOLCONTRACTFSFSREQ.FCUBSBODY createDELETEOLCONTRACTFSFSREQFCUBSBODY() {
        return new DELETEOLCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEOLCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public DELETEOLCONTRACTFSFSRES.FCUBSBODY createDELETEOLCONTRACTFSFSRESFCUBSBODY() {
        return new DELETEOLCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEOLCONTRACTIOPKREQ.FCUBSBODY }
     * 
     */
    public DELETEOLCONTRACTIOPKREQ.FCUBSBODY createDELETEOLCONTRACTIOPKREQFCUBSBODY() {
        return new DELETEOLCONTRACTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEOLCONTRACTIOPKRES.FCUBSBODY }
     * 
     */
    public DELETEOLCONTRACTIOPKRES.FCUBSBODY createDELETEOLCONTRACTIOPKRESFCUBSBODY() {
        return new DELETEOLCONTRACTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEREPACCFSFSREQ.FCUBSBODY }
     * 
     */
    public DELETEREPACCFSFSREQ.FCUBSBODY createDELETEREPACCFSFSREQFCUBSBODY() {
        return new DELETEREPACCFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEREPACCFSFSRES.FCUBSBODY }
     * 
     */
    public DELETEREPACCFSFSRES.FCUBSBODY createDELETEREPACCFSFSRESFCUBSBODY() {
        return new DELETEREPACCFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEREPACCIOPKREQ.FCUBSBODY }
     * 
     */
    public DELETEREPACCIOPKREQ.FCUBSBODY createDELETEREPACCIOPKREQFCUBSBODY() {
        return new DELETEREPACCIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEREPACCIOPKRES.FCUBSBODY }
     * 
     */
    public DELETEREPACCIOPKRES.FCUBSBODY createDELETEREPACCIOPKRESFCUBSBODY() {
        return new DELETEREPACCIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEREPLIACCFSFSREQ.FCUBSBODY }
     * 
     */
    public DELETEREPLIACCFSFSREQ.FCUBSBODY createDELETEREPLIACCFSFSREQFCUBSBODY() {
        return new DELETEREPLIACCFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEREPLIACCFSFSRES.FCUBSBODY }
     * 
     */
    public DELETEREPLIACCFSFSRES.FCUBSBODY createDELETEREPLIACCFSFSRESFCUBSBODY() {
        return new DELETEREPLIACCFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEREPLIACCIOPKREQ.FCUBSBODY }
     * 
     */
    public DELETEREPLIACCIOPKREQ.FCUBSBODY createDELETEREPLIACCIOPKREQFCUBSBODY() {
        return new DELETEREPLIACCIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEREPLIACCIOPKRES.FCUBSBODY }
     * 
     */
    public DELETEREPLIACCIOPKRES.FCUBSBODY createDELETEREPLIACCIOPKRESFCUBSBODY() {
        return new DELETEREPLIACCIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYOLCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public MODIFYOLCONTRACTFSFSREQ.FCUBSBODY createMODIFYOLCONTRACTFSFSREQFCUBSBODY() {
        return new MODIFYOLCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYOLCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public MODIFYOLCONTRACTFSFSRES.FCUBSBODY createMODIFYOLCONTRACTFSFSRESFCUBSBODY() {
        return new MODIFYOLCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYOLCONTRACTIOPKREQ.FCUBSBODY }
     * 
     */
    public MODIFYOLCONTRACTIOPKREQ.FCUBSBODY createMODIFYOLCONTRACTIOPKREQFCUBSBODY() {
        return new MODIFYOLCONTRACTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYOLCONTRACTIOPKRES.FCUBSBODY }
     * 
     */
    public MODIFYOLCONTRACTIOPKRES.FCUBSBODY createMODIFYOLCONTRACTIOPKRESFCUBSBODY() {
        return new MODIFYOLCONTRACTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYREPACCFSFSREQ.FCUBSBODY }
     * 
     */
    public MODIFYREPACCFSFSREQ.FCUBSBODY createMODIFYREPACCFSFSREQFCUBSBODY() {
        return new MODIFYREPACCFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYREPACCFSFSRES.FCUBSBODY }
     * 
     */
    public MODIFYREPACCFSFSRES.FCUBSBODY createMODIFYREPACCFSFSRESFCUBSBODY() {
        return new MODIFYREPACCFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYREPACCIOPKREQ.FCUBSBODY }
     * 
     */
    public MODIFYREPACCIOPKREQ.FCUBSBODY createMODIFYREPACCIOPKREQFCUBSBODY() {
        return new MODIFYREPACCIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYREPACCIOPKRES.FCUBSBODY }
     * 
     */
    public MODIFYREPACCIOPKRES.FCUBSBODY createMODIFYREPACCIOPKRESFCUBSBODY() {
        return new MODIFYREPACCIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYREPLIACCFSFSREQ.FCUBSBODY }
     * 
     */
    public MODIFYREPLIACCFSFSREQ.FCUBSBODY createMODIFYREPLIACCFSFSREQFCUBSBODY() {
        return new MODIFYREPLIACCFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYREPLIACCFSFSRES.FCUBSBODY }
     * 
     */
    public MODIFYREPLIACCFSFSRES.FCUBSBODY createMODIFYREPLIACCFSFSRESFCUBSBODY() {
        return new MODIFYREPLIACCFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYREPLIACCIOPKREQ.FCUBSBODY }
     * 
     */
    public MODIFYREPLIACCIOPKREQ.FCUBSBODY createMODIFYREPLIACCIOPKREQFCUBSBODY() {
        return new MODIFYREPLIACCIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYREPLIACCIOPKRES.FCUBSBODY }
     * 
     */
    public MODIFYREPLIACCIOPKRES.FCUBSBODY createMODIFYREPLIACCIOPKRESFCUBSBODY() {
        return new MODIFYREPLIACCIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYEXTACCECAENTRIESIOFSREQ.FCUBSBODY }
     * 
     */
    public QUERYEXTACCECAENTRIESIOFSREQ.FCUBSBODY createQUERYEXTACCECAENTRIESIOFSREQFCUBSBODY() {
        return new QUERYEXTACCECAENTRIESIOFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYEXTACCECAENTRIESIOFSRES.FCUBSBODY }
     * 
     */
    public QUERYEXTACCECAENTRIESIOFSRES.FCUBSBODY createQUERYEXTACCECAENTRIESIOFSRESFCUBSBODY() {
        return new QUERYEXTACCECAENTRIESIOFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYREPACCIOFSREQ.FCUBSBODY }
     * 
     */
    public QUERYREPACCIOFSREQ.FCUBSBODY createQUERYREPACCIOFSREQFCUBSBODY() {
        return new QUERYREPACCIOFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYREPACCIOFSRES.FCUBSBODY }
     * 
     */
    public QUERYREPACCIOFSRES.FCUBSBODY createQUERYREPACCIOFSRESFCUBSBODY() {
        return new QUERYREPACCIOFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYREPLIACCIOFSREQ.FCUBSBODY }
     * 
     */
    public QUERYREPLIACCIOFSREQ.FCUBSBODY createQUERYREPLIACCIOFSREQFCUBSBODY() {
        return new QUERYREPLIACCIOFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYREPLIACCIOFSRES.FCUBSBODY }
     * 
     */
    public QUERYREPLIACCIOFSRES.FCUBSBODY createQUERYREPLIACCIOFSRESFCUBSBODY() {
        return new QUERYREPLIACCIOFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link REVERSEEXTACCENTRIESFSFSREQ.FCUBSBODY }
     * 
     */
    public REVERSEEXTACCENTRIESFSFSREQ.FCUBSBODY createREVERSEEXTACCENTRIESFSFSREQFCUBSBODY() {
        return new REVERSEEXTACCENTRIESFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link REVERSEEXTACCENTRIESFSFSRES.FCUBSBODY }
     * 
     */
    public REVERSEEXTACCENTRIESFSFSRES.FCUBSBODY createREVERSEEXTACCENTRIESFSFSRESFCUBSBODY() {
        return new REVERSEEXTACCENTRIESFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link REVERSEEXTACCENTRIESIOPKREQ.FCUBSBODY }
     * 
     */
    public REVERSEEXTACCENTRIESIOPKREQ.FCUBSBODY createREVERSEEXTACCENTRIESIOPKREQFCUBSBODY() {
        return new REVERSEEXTACCENTRIESIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link REVERSEEXTACCENTRIESIOPKRES.FCUBSBODY }
     * 
     */
    public REVERSEEXTACCENTRIESIOPKRES.FCUBSBODY createREVERSEEXTACCENTRIESIOPKRESFCUBSBODY() {
        return new REVERSEEXTACCENTRIESIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link RepAccMaintPKType }
     * 
     */
    public RepAccMaintPKType createRepAccMaintPKType() {
        return new RepAccMaintPKType();
    }

    /**
     * Create an instance of {@link RepAccMaintPKACRType }
     * 
     */
    public RepAccMaintPKACRType createRepAccMaintPKACRType() {
        return new RepAccMaintPKACRType();
    }

    /**
     * Create an instance of {@link RepAccMaintFullType }
     * 
     */
    public RepAccMaintFullType createRepAccMaintFullType() {
        return new RepAccMaintFullType();
    }

    /**
     * Create an instance of {@link RepAccMaintIOType }
     * 
     */
    public RepAccMaintIOType createRepAccMaintIOType() {
        return new RepAccMaintIOType();
    }

    /**
     * Create an instance of {@link RepAccMaintModifyIOType }
     * 
     */
    public RepAccMaintModifyIOType createRepAccMaintModifyIOType() {
        return new RepAccMaintModifyIOType();
    }

    /**
     * Create an instance of {@link FCUBSNotifHeaderType }
     * 
     */
    public FCUBSNotifHeaderType createFCUBSNotifHeaderType() {
        return new FCUBSNotifHeaderType();
    }

    /**
     * Create an instance of {@link ERRORType }
     * 
     */
    public ERRORType createERRORType() {
        return new ERRORType();
    }

    /**
     * Create an instance of {@link ERRORDETAILSType }
     * 
     */
    public ERRORDETAILSType createERRORDETAILSType() {
        return new ERRORDETAILSType();
    }

    /**
     * Create an instance of {@link WARNINGType }
     * 
     */
    public WARNINGType createWARNINGType() {
        return new WARNINGType();
    }

    /**
     * Create an instance of {@link WARNINGDETAILSType }
     * 
     */
    public WARNINGDETAILSType createWARNINGDETAILSType() {
        return new WARNINGDETAILSType();
    }

    /**
     * Create an instance of {@link RepliAccPKType }
     * 
     */
    public RepliAccPKType createRepliAccPKType() {
        return new RepliAccPKType();
    }

    /**
     * Create an instance of {@link RepliAccFullType }
     * 
     */
    public RepliAccFullType createRepliAccFullType() {
        return new RepliAccFullType();
    }

    /**
     * Create an instance of {@link RepliAccQueryIOType }
     * 
     */
    public RepliAccQueryIOType createRepliAccQueryIOType() {
        return new RepliAccQueryIOType();
    }

    /**
     * Create an instance of {@link RepliAccCreateIOType }
     * 
     */
    public RepliAccCreateIOType createRepliAccCreateIOType() {
        return new RepliAccCreateIOType();
    }

    /**
     * Create an instance of {@link RepliAccModifyIOType }
     * 
     */
    public RepliAccModifyIOType createRepliAccModifyIOType() {
        return new RepliAccModifyIOType();
    }

    /**
     * Create an instance of {@link RepliAccAuthorizeIOType }
     * 
     */
    public RepliAccAuthorizeIOType createRepliAccAuthorizeIOType() {
        return new RepliAccAuthorizeIOType();
    }

    /**
     * Create an instance of {@link RepliAccDeleteIOType }
     * 
     */
    public RepliAccDeleteIOType createRepliAccDeleteIOType() {
        return new RepliAccDeleteIOType();
    }

    /**
     * Create an instance of {@link ExtAccEcaEntriesPKType }
     * 
     */
    public ExtAccEcaEntriesPKType createExtAccEcaEntriesPKType() {
        return new ExtAccEcaEntriesPKType();
    }

    /**
     * Create an instance of {@link ExtAccEcaEntriesQueryIOType }
     * 
     */
    public ExtAccEcaEntriesQueryIOType createExtAccEcaEntriesQueryIOType() {
        return new ExtAccEcaEntriesQueryIOType();
    }

    /**
     * Create an instance of {@link ExtAccEntriesPKType }
     * 
     */
    public ExtAccEntriesPKType createExtAccEntriesPKType() {
        return new ExtAccEntriesPKType();
    }

    /**
     * Create an instance of {@link ExtAccEntriesReverseIOType }
     * 
     */
    public ExtAccEntriesReverseIOType createExtAccEntriesReverseIOType() {
        return new ExtAccEntriesReverseIOType();
    }

    /**
     * Create an instance of {@link FXLinkPKType }
     * 
     */
    public FXLinkPKType createFXLinkPKType() {
        return new FXLinkPKType();
    }

    /**
     * Create an instance of {@link FXLinkFullType }
     * 
     */
    public FXLinkFullType createFXLinkFullType() {
        return new FXLinkFullType();
    }

    /**
     * Create an instance of {@link FXLinkCreateIOType }
     * 
     */
    public FXLinkCreateIOType createFXLinkCreateIOType() {
        return new FXLinkCreateIOType();
    }

    /**
     * Create an instance of {@link OLContractPKType }
     * 
     */
    public OLContractPKType createOLContractPKType() {
        return new OLContractPKType();
    }

    /**
     * Create an instance of {@link OLContractModifyIOType }
     * 
     */
    public OLContractModifyIOType createOLContractModifyIOType() {
        return new OLContractModifyIOType();
    }

    /**
     * Create an instance of {@link OLContractDeleteIOType }
     * 
     */
    public OLContractDeleteIOType createOLContractDeleteIOType() {
        return new OLContractDeleteIOType();
    }

    /**
     * Create an instance of {@link ExtAccEcaEntriesFullType.AccEntryDetails }
     * 
     */
    public ExtAccEcaEntriesFullType.AccEntryDetails createExtAccEcaEntriesFullTypeAccEntryDetails() {
        return new ExtAccEcaEntriesFullType.AccEntryDetails();
    }

    /**
     * Create an instance of {@link ExtAccEcaEntriesCreateIOType.AccEntryDetails }
     * 
     */
    public ExtAccEcaEntriesCreateIOType.AccEntryDetails createExtAccEcaEntriesCreateIOTypeAccEntryDetails() {
        return new ExtAccEcaEntriesCreateIOType.AccEntryDetails();
    }

    /**
     * Create an instance of {@link ExtAccEntriesFullType.ExtSysAccDetails }
     * 
     */
    public ExtAccEntriesFullType.ExtSysAccDetails createExtAccEntriesFullTypeExtSysAccDetails() {
        return new ExtAccEntriesFullType.ExtSysAccDetails();
    }

    /**
     * Create an instance of {@link ExtAccEntriesCreateIOType.ExtSysAccDetails }
     * 
     */
    public ExtAccEntriesCreateIOType.ExtSysAccDetails createExtAccEntriesCreateIOTypeExtSysAccDetails() {
        return new ExtAccEntriesCreateIOType.ExtSysAccDetails();
    }

    /**
     * Create an instance of {@link OLContractFullType.IftbOlamountDue }
     * 
     */
    public OLContractFullType.IftbOlamountDue createOLContractFullTypeIftbOlamountDue() {
        return new OLContractFullType.IftbOlamountDue();
    }

    /**
     * Create an instance of {@link OLContractCreateIOType.IftbOlamountDue }
     * 
     */
    public OLContractCreateIOType.IftbOlamountDue createOLContractCreateIOTypeIftbOlamountDue() {
        return new OLContractCreateIOType.IftbOlamountDue();
    }

    /**
     * Create an instance of {@link FCUBSHEADERType.ADDL.PARAM }
     * 
     */
    public FCUBSHEADERType.ADDL.PARAM createFCUBSHEADERTypeADDLPARAM() {
        return new FCUBSHEADERType.ADDL.PARAM();
    }

}
