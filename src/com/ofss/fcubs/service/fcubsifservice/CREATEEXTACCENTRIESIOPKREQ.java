
package com.ofss.fcubs.service.fcubsifservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FCUBS_HEADER" type="{http://fcubs.ofss.com/service/FCUBSIFService}FCUBS_HEADERType"/&gt;
 *         &lt;element name="FCUBS_BODY"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Ext-Sys-Acc-Master-IO" type="{http://fcubs.ofss.com/service/FCUBSIFService}ExtAccEntries-Create-IO-Type"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fcubsheader",
    "fcubsbody"
})
@XmlRootElement(name = "CREATEEXTACCENTRIES_IOPK_REQ")
public class CREATEEXTACCENTRIESIOPKREQ {

    @XmlElement(name = "FCUBS_HEADER", required = true)
    protected FCUBSHEADERType fcubsheader;
    @XmlElement(name = "FCUBS_BODY", required = true)
    protected CREATEEXTACCENTRIESIOPKREQ.FCUBSBODY fcubsbody;

    /**
     * Gets the value of the fcubsheader property.
     * 
     * @return
     *     possible object is
     *     {@link FCUBSHEADERType }
     *     
     */
    public FCUBSHEADERType getFCUBSHEADER() {
        return fcubsheader;
    }

    /**
     * Sets the value of the fcubsheader property.
     * 
     * @param value
     *     allowed object is
     *     {@link FCUBSHEADERType }
     *     
     */
    public void setFCUBSHEADER(FCUBSHEADERType value) {
        this.fcubsheader = value;
    }

    /**
     * Gets the value of the fcubsbody property.
     * 
     * @return
     *     possible object is
     *     {@link CREATEEXTACCENTRIESIOPKREQ.FCUBSBODY }
     *     
     */
    public CREATEEXTACCENTRIESIOPKREQ.FCUBSBODY getFCUBSBODY() {
        return fcubsbody;
    }

    /**
     * Sets the value of the fcubsbody property.
     * 
     * @param value
     *     allowed object is
     *     {@link CREATEEXTACCENTRIESIOPKREQ.FCUBSBODY }
     *     
     */
    public void setFCUBSBODY(CREATEEXTACCENTRIESIOPKREQ.FCUBSBODY value) {
        this.fcubsbody = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Ext-Sys-Acc-Master-IO" type="{http://fcubs.ofss.com/service/FCUBSIFService}ExtAccEntries-Create-IO-Type"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "extSysAccMasterIO"
    })
    public static class FCUBSBODY {

        @XmlElement(name = "Ext-Sys-Acc-Master-IO", required = true)
        protected ExtAccEntriesCreateIOType extSysAccMasterIO;

        /**
         * Gets the value of the extSysAccMasterIO property.
         * 
         * @return
         *     possible object is
         *     {@link ExtAccEntriesCreateIOType }
         *     
         */
        public ExtAccEntriesCreateIOType getExtSysAccMasterIO() {
            return extSysAccMasterIO;
        }

        /**
         * Sets the value of the extSysAccMasterIO property.
         * 
         * @param value
         *     allowed object is
         *     {@link ExtAccEntriesCreateIOType }
         *     
         */
        public void setExtSysAccMasterIO(ExtAccEntriesCreateIOType value) {
            this.extSysAccMasterIO = value;
        }

    }

}
