
package com.ofss.fcubs.service.fcubsifservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FXLink-PK-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FXLink-PK-Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FX_REFERENCE_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LINKED_REFERENCE_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FXLink-PK-Type", propOrder = {
    "fxreferenceno",
    "linkedreferenceno"
})
public class FXLinkPKType {

    @XmlElement(name = "FX_REFERENCE_NO", required = true)
    protected String fxreferenceno;
    @XmlElement(name = "LINKED_REFERENCE_NO", required = true)
    protected String linkedreferenceno;

    /**
     * Gets the value of the fxreferenceno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFXREFERENCENO() {
        return fxreferenceno;
    }

    /**
     * Sets the value of the fxreferenceno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFXREFERENCENO(String value) {
        this.fxreferenceno = value;
    }

    /**
     * Gets the value of the linkedreferenceno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLINKEDREFERENCENO() {
        return linkedreferenceno;
    }

    /**
     * Sets the value of the linkedreferenceno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLINKEDREFERENCENO(String value) {
        this.linkedreferenceno = value;
    }

}
