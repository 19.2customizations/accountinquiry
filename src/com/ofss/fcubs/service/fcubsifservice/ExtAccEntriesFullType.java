
package com.ofss.fcubs.service.fcubsifservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ExtAccEntries-Full-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtAccEntries-Full-Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UNIQUEREFNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="EXTERNALREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Ext-Sys-Acc-Details" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="MODULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TRNREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EVENTSRNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="EVENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="AMTBLOCKNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ACBRANCH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ACNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ACCCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="FORCEPOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DRCRIND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TRNCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="AMOUNTTAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="FCYAMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="EXCHRATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="LCYAMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="RELATEDCUSTOMER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RELATEDACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RELATEDREFERENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TRNDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                   &lt;element name="VALUEDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                   &lt;element name="INSTRUMENTCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MISHEAD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PRODUCT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="REMARKS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="NETTINGIND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtAccEntries-Full-Type", propOrder = {
    "uniquerefno",
    "externalrefno",
    "extSysAccDetails"
})
public class ExtAccEntriesFullType {

    @XmlElement(name = "UNIQUEREFNO")
    protected BigDecimal uniquerefno;
    @XmlElement(name = "EXTERNALREFNO")
    protected String externalrefno;
    @XmlElement(name = "Ext-Sys-Acc-Details")
    protected List<ExtAccEntriesFullType.ExtSysAccDetails> extSysAccDetails;

    /**
     * Gets the value of the uniquerefno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUNIQUEREFNO() {
        return uniquerefno;
    }

    /**
     * Sets the value of the uniquerefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUNIQUEREFNO(BigDecimal value) {
        this.uniquerefno = value;
    }

    /**
     * Gets the value of the externalrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXTERNALREFNO() {
        return externalrefno;
    }

    /**
     * Sets the value of the externalrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXTERNALREFNO(String value) {
        this.externalrefno = value;
    }

    /**
     * Gets the value of the extSysAccDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extSysAccDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtSysAccDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtAccEntriesFullType.ExtSysAccDetails }
     * 
     * 
     */
    public List<ExtAccEntriesFullType.ExtSysAccDetails> getExtSysAccDetails() {
        if (extSysAccDetails == null) {
            extSysAccDetails = new ArrayList<ExtAccEntriesFullType.ExtSysAccDetails>();
        }
        return this.extSysAccDetails;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="MODULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TRNREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EVENTSRNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="EVENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="AMTBLOCKNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ACBRANCH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ACNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ACCCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="FORCEPOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DRCRIND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TRNCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="AMOUNTTAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="FCYAMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="EXCHRATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="LCYAMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="RELATEDCUSTOMER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RELATEDACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RELATEDREFERENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TRNDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *         &lt;element name="VALUEDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *         &lt;element name="INSTRUMENTCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MISHEAD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PRODUCT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="REMARKS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="NETTINGIND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "module",
        "trnrefno",
        "eventsrno",
        "event",
        "amtblockno",
        "acbranch",
        "acno",
        "acccy",
        "forcepost",
        "drcrind",
        "trncode",
        "amounttag",
        "fcyamount",
        "exchrate",
        "lcyamount",
        "relatedcustomer",
        "relatedaccount",
        "relatedreference",
        "trndt",
        "valuedt",
        "instrumentcode",
        "mishead",
        "product",
        "remarks",
        "nettingind"
    })
    public static class ExtSysAccDetails {

        @XmlElement(name = "MODULE")
        protected String module;
        @XmlElement(name = "TRNREFNO")
        protected String trnrefno;
        @XmlElement(name = "EVENTSRNO")
        protected BigDecimal eventsrno;
        @XmlElement(name = "EVENT")
        protected String event;
        @XmlElement(name = "AMTBLOCKNO")
        protected String amtblockno;
        @XmlElement(name = "ACBRANCH")
        protected String acbranch;
        @XmlElement(name = "ACNO")
        protected String acno;
        @XmlElement(name = "ACCCY")
        protected String acccy;
        @XmlElement(name = "FORCEPOST")
        protected String forcepost;
        @XmlElement(name = "DRCRIND")
        protected String drcrind;
        @XmlElement(name = "TRNCODE")
        protected String trncode;
        @XmlElement(name = "AMOUNTTAG")
        protected String amounttag;
        @XmlElement(name = "FCYAMOUNT")
        protected BigDecimal fcyamount;
        @XmlElement(name = "EXCHRATE")
        protected BigDecimal exchrate;
        @XmlElement(name = "LCYAMOUNT")
        protected BigDecimal lcyamount;
        @XmlElement(name = "RELATEDCUSTOMER")
        protected String relatedcustomer;
        @XmlElement(name = "RELATEDACCOUNT")
        protected String relatedaccount;
        @XmlElement(name = "RELATEDREFERENCE")
        protected String relatedreference;
        @XmlElement(name = "TRNDT")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar trndt;
        @XmlElement(name = "VALUEDT")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar valuedt;
        @XmlElement(name = "INSTRUMENTCODE")
        protected String instrumentcode;
        @XmlElement(name = "MISHEAD")
        protected String mishead;
        @XmlElement(name = "PRODUCT")
        protected String product;
        @XmlElement(name = "REMARKS")
        protected String remarks;
        @XmlElement(name = "NETTINGIND")
        protected String nettingind;

        /**
         * Gets the value of the module property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMODULE() {
            return module;
        }

        /**
         * Sets the value of the module property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMODULE(String value) {
            this.module = value;
        }

        /**
         * Gets the value of the trnrefno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTRNREFNO() {
            return trnrefno;
        }

        /**
         * Sets the value of the trnrefno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTRNREFNO(String value) {
            this.trnrefno = value;
        }

        /**
         * Gets the value of the eventsrno property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getEVENTSRNO() {
            return eventsrno;
        }

        /**
         * Sets the value of the eventsrno property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setEVENTSRNO(BigDecimal value) {
            this.eventsrno = value;
        }

        /**
         * Gets the value of the event property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEVENT() {
            return event;
        }

        /**
         * Sets the value of the event property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEVENT(String value) {
            this.event = value;
        }

        /**
         * Gets the value of the amtblockno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAMTBLOCKNO() {
            return amtblockno;
        }

        /**
         * Sets the value of the amtblockno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAMTBLOCKNO(String value) {
            this.amtblockno = value;
        }

        /**
         * Gets the value of the acbranch property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACBRANCH() {
            return acbranch;
        }

        /**
         * Sets the value of the acbranch property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACBRANCH(String value) {
            this.acbranch = value;
        }

        /**
         * Gets the value of the acno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACNO() {
            return acno;
        }

        /**
         * Sets the value of the acno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACNO(String value) {
            this.acno = value;
        }

        /**
         * Gets the value of the acccy property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACCCY() {
            return acccy;
        }

        /**
         * Sets the value of the acccy property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACCCY(String value) {
            this.acccy = value;
        }

        /**
         * Gets the value of the forcepost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFORCEPOST() {
            return forcepost;
        }

        /**
         * Sets the value of the forcepost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFORCEPOST(String value) {
            this.forcepost = value;
        }

        /**
         * Gets the value of the drcrind property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDRCRIND() {
            return drcrind;
        }

        /**
         * Sets the value of the drcrind property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDRCRIND(String value) {
            this.drcrind = value;
        }

        /**
         * Gets the value of the trncode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTRNCODE() {
            return trncode;
        }

        /**
         * Sets the value of the trncode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTRNCODE(String value) {
            this.trncode = value;
        }

        /**
         * Gets the value of the amounttag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAMOUNTTAG() {
            return amounttag;
        }

        /**
         * Sets the value of the amounttag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAMOUNTTAG(String value) {
            this.amounttag = value;
        }

        /**
         * Gets the value of the fcyamount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getFCYAMOUNT() {
            return fcyamount;
        }

        /**
         * Sets the value of the fcyamount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setFCYAMOUNT(BigDecimal value) {
            this.fcyamount = value;
        }

        /**
         * Gets the value of the exchrate property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getEXCHRATE() {
            return exchrate;
        }

        /**
         * Sets the value of the exchrate property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setEXCHRATE(BigDecimal value) {
            this.exchrate = value;
        }

        /**
         * Gets the value of the lcyamount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLCYAMOUNT() {
            return lcyamount;
        }

        /**
         * Sets the value of the lcyamount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLCYAMOUNT(BigDecimal value) {
            this.lcyamount = value;
        }

        /**
         * Gets the value of the relatedcustomer property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRELATEDCUSTOMER() {
            return relatedcustomer;
        }

        /**
         * Sets the value of the relatedcustomer property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRELATEDCUSTOMER(String value) {
            this.relatedcustomer = value;
        }

        /**
         * Gets the value of the relatedaccount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRELATEDACCOUNT() {
            return relatedaccount;
        }

        /**
         * Sets the value of the relatedaccount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRELATEDACCOUNT(String value) {
            this.relatedaccount = value;
        }

        /**
         * Gets the value of the relatedreference property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRELATEDREFERENCE() {
            return relatedreference;
        }

        /**
         * Sets the value of the relatedreference property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRELATEDREFERENCE(String value) {
            this.relatedreference = value;
        }

        /**
         * Gets the value of the trndt property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getTRNDT() {
            return trndt;
        }

        /**
         * Sets the value of the trndt property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setTRNDT(XMLGregorianCalendar value) {
            this.trndt = value;
        }

        /**
         * Gets the value of the valuedt property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getVALUEDT() {
            return valuedt;
        }

        /**
         * Sets the value of the valuedt property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setVALUEDT(XMLGregorianCalendar value) {
            this.valuedt = value;
        }

        /**
         * Gets the value of the instrumentcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSTRUMENTCODE() {
            return instrumentcode;
        }

        /**
         * Sets the value of the instrumentcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSTRUMENTCODE(String value) {
            this.instrumentcode = value;
        }

        /**
         * Gets the value of the mishead property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMISHEAD() {
            return mishead;
        }

        /**
         * Sets the value of the mishead property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMISHEAD(String value) {
            this.mishead = value;
        }

        /**
         * Gets the value of the product property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPRODUCT() {
            return product;
        }

        /**
         * Sets the value of the product property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPRODUCT(String value) {
            this.product = value;
        }

        /**
         * Gets the value of the remarks property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getREMARKS() {
            return remarks;
        }

        /**
         * Sets the value of the remarks property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setREMARKS(String value) {
            this.remarks = value;
        }

        /**
         * Gets the value of the nettingind property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNETTINGIND() {
            return nettingind;
        }

        /**
         * Sets the value of the nettingind property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNETTINGIND(String value) {
            this.nettingind = value;
        }

    }

}
