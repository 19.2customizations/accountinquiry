
package com.ofss.fcubs.service.fcubsifservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OLContract-Full-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OLContract-Full-Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CONTRACT_REF_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SCODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="XREF" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="USER_DEFINED_STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="MODULE_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PRODUCT_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BOOK_DATE" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="VALUE_DATE" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="MATURITY_DATE" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="VERSION_NO" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="COUNTERPARTY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BRANCH" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LATEST_ESN" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="CONTRACT_STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PACKING_CREDIT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PRODUCT_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CONTRACT_CCY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PRODUCT_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Iftb-Olamount-Due" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="CONTRACT_REF_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="DUE_DATE" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                   &lt;element name="COMPONENT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="SCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="XREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="BRANCH_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="COMPONENT_CCY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="AMOUNT_DUE" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="AMOUNT_SETTLED" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="ACCRUED_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="COMPONENT_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="LATEST_INT_RATE" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="EMI_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="ADJUSTED_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OLContract-Full-Type", propOrder = {
    "contractrefno",
    "scode",
    "xref",
    "userdefinedstatus",
    "amount",
    "modulecode",
    "producttype",
    "bookdate",
    "valuedate",
    "maturitydate",
    "versionno",
    "counterparty",
    "branch",
    "latestesn",
    "contractstatus",
    "packingcredit",
    "productdesc",
    "contractccy",
    "productcode",
    "iftbOlamountDue"
})
public class OLContractFullType {

    @XmlElement(name = "CONTRACT_REF_NO", required = true)
    protected String contractrefno;
    @XmlElement(name = "SCODE", required = true)
    protected String scode;
    @XmlElement(name = "XREF", required = true)
    protected String xref;
    @XmlElement(name = "USER_DEFINED_STATUS", required = true)
    protected String userdefinedstatus;
    @XmlElement(name = "AMOUNT", required = true)
    protected BigDecimal amount;
    @XmlElement(name = "MODULE_CODE", required = true)
    protected String modulecode;
    @XmlElement(name = "PRODUCT_TYPE", required = true)
    protected String producttype;
    @XmlElement(name = "BOOK_DATE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar bookdate;
    @XmlElement(name = "VALUE_DATE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar valuedate;
    @XmlElement(name = "MATURITY_DATE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar maturitydate;
    @XmlElement(name = "VERSION_NO", required = true)
    protected BigDecimal versionno;
    @XmlElement(name = "COUNTERPARTY", required = true)
    protected String counterparty;
    @XmlElement(name = "BRANCH", required = true)
    protected String branch;
    @XmlElement(name = "LATEST_ESN", required = true)
    protected BigDecimal latestesn;
    @XmlElement(name = "CONTRACT_STATUS", required = true)
    protected String contractstatus;
    @XmlElement(name = "PACKING_CREDIT", required = true)
    protected String packingcredit;
    @XmlElement(name = "PRODUCT_DESC")
    protected String productdesc;
    @XmlElement(name = "CONTRACT_CCY", required = true)
    protected String contractccy;
    @XmlElement(name = "PRODUCT_CODE", required = true)
    protected String productcode;
    @XmlElement(name = "Iftb-Olamount-Due")
    protected List<OLContractFullType.IftbOlamountDue> iftbOlamountDue;

    /**
     * Gets the value of the contractrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONTRACTREFNO() {
        return contractrefno;
    }

    /**
     * Sets the value of the contractrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONTRACTREFNO(String value) {
        this.contractrefno = value;
    }

    /**
     * Gets the value of the scode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSCODE() {
        return scode;
    }

    /**
     * Sets the value of the scode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSCODE(String value) {
        this.scode = value;
    }

    /**
     * Gets the value of the xref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXREF() {
        return xref;
    }

    /**
     * Sets the value of the xref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXREF(String value) {
        this.xref = value;
    }

    /**
     * Gets the value of the userdefinedstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSERDEFINEDSTATUS() {
        return userdefinedstatus;
    }

    /**
     * Sets the value of the userdefinedstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSERDEFINEDSTATUS(String value) {
        this.userdefinedstatus = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAMOUNT() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAMOUNT(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the modulecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODULECODE() {
        return modulecode;
    }

    /**
     * Sets the value of the modulecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODULECODE(String value) {
        this.modulecode = value;
    }

    /**
     * Gets the value of the producttype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRODUCTTYPE() {
        return producttype;
    }

    /**
     * Sets the value of the producttype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRODUCTTYPE(String value) {
        this.producttype = value;
    }

    /**
     * Gets the value of the bookdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBOOKDATE() {
        return bookdate;
    }

    /**
     * Sets the value of the bookdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBOOKDATE(XMLGregorianCalendar value) {
        this.bookdate = value;
    }

    /**
     * Gets the value of the valuedate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVALUEDATE() {
        return valuedate;
    }

    /**
     * Sets the value of the valuedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVALUEDATE(XMLGregorianCalendar value) {
        this.valuedate = value;
    }

    /**
     * Gets the value of the maturitydate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMATURITYDATE() {
        return maturitydate;
    }

    /**
     * Sets the value of the maturitydate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMATURITYDATE(XMLGregorianCalendar value) {
        this.maturitydate = value;
    }

    /**
     * Gets the value of the versionno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVERSIONNO() {
        return versionno;
    }

    /**
     * Sets the value of the versionno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVERSIONNO(BigDecimal value) {
        this.versionno = value;
    }

    /**
     * Gets the value of the counterparty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOUNTERPARTY() {
        return counterparty;
    }

    /**
     * Sets the value of the counterparty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOUNTERPARTY(String value) {
        this.counterparty = value;
    }

    /**
     * Gets the value of the branch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRANCH() {
        return branch;
    }

    /**
     * Sets the value of the branch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRANCH(String value) {
        this.branch = value;
    }

    /**
     * Gets the value of the latestesn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLATESTESN() {
        return latestesn;
    }

    /**
     * Sets the value of the latestesn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLATESTESN(BigDecimal value) {
        this.latestesn = value;
    }

    /**
     * Gets the value of the contractstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONTRACTSTATUS() {
        return contractstatus;
    }

    /**
     * Sets the value of the contractstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONTRACTSTATUS(String value) {
        this.contractstatus = value;
    }

    /**
     * Gets the value of the packingcredit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPACKINGCREDIT() {
        return packingcredit;
    }

    /**
     * Sets the value of the packingcredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPACKINGCREDIT(String value) {
        this.packingcredit = value;
    }

    /**
     * Gets the value of the productdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRODUCTDESC() {
        return productdesc;
    }

    /**
     * Sets the value of the productdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRODUCTDESC(String value) {
        this.productdesc = value;
    }

    /**
     * Gets the value of the contractccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONTRACTCCY() {
        return contractccy;
    }

    /**
     * Sets the value of the contractccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONTRACTCCY(String value) {
        this.contractccy = value;
    }

    /**
     * Gets the value of the productcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRODUCTCODE() {
        return productcode;
    }

    /**
     * Sets the value of the productcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRODUCTCODE(String value) {
        this.productcode = value;
    }

    /**
     * Gets the value of the iftbOlamountDue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the iftbOlamountDue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIftbOlamountDue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OLContractFullType.IftbOlamountDue }
     * 
     * 
     */
    public List<OLContractFullType.IftbOlamountDue> getIftbOlamountDue() {
        if (iftbOlamountDue == null) {
            iftbOlamountDue = new ArrayList<OLContractFullType.IftbOlamountDue>();
        }
        return this.iftbOlamountDue;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="CONTRACT_REF_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="DUE_DATE" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *         &lt;element name="COMPONENT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="SCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="XREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="BRANCH_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="COMPONENT_CCY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="AMOUNT_DUE" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="AMOUNT_SETTLED" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="ACCRUED_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="COMPONENT_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="LATEST_INT_RATE" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="EMI_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="ADJUSTED_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contractrefno",
        "duedate",
        "component",
        "scode",
        "xref",
        "branchcode",
        "componentccy",
        "amountdue",
        "amountsettled",
        "accruedamount",
        "componenttype",
        "latestintrate",
        "emiamount",
        "adjustedamount"
    })
    public static class IftbOlamountDue {

        @XmlElement(name = "CONTRACT_REF_NO", required = true)
        protected String contractrefno;
        @XmlElement(name = "DUE_DATE", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar duedate;
        @XmlElement(name = "COMPONENT", required = true)
        protected String component;
        @XmlElement(name = "SCODE")
        protected String scode;
        @XmlElement(name = "XREF")
        protected String xref;
        @XmlElement(name = "BRANCH_CODE", required = true)
        protected String branchcode;
        @XmlElement(name = "COMPONENT_CCY", required = true)
        protected String componentccy;
        @XmlElement(name = "AMOUNT_DUE", required = true)
        protected BigDecimal amountdue;
        @XmlElement(name = "AMOUNT_SETTLED", required = true)
        protected BigDecimal amountsettled;
        @XmlElement(name = "ACCRUED_AMOUNT", required = true)
        protected BigDecimal accruedamount;
        @XmlElement(name = "COMPONENT_TYPE", required = true)
        protected String componenttype;
        @XmlElement(name = "LATEST_INT_RATE", required = true)
        protected BigDecimal latestintrate;
        @XmlElement(name = "EMI_AMOUNT", required = true)
        protected BigDecimal emiamount;
        @XmlElement(name = "ADJUSTED_AMOUNT", required = true)
        protected BigDecimal adjustedamount;

        /**
         * Gets the value of the contractrefno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCONTRACTREFNO() {
            return contractrefno;
        }

        /**
         * Sets the value of the contractrefno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCONTRACTREFNO(String value) {
            this.contractrefno = value;
        }

        /**
         * Gets the value of the duedate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDUEDATE() {
            return duedate;
        }

        /**
         * Sets the value of the duedate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDUEDATE(XMLGregorianCalendar value) {
            this.duedate = value;
        }

        /**
         * Gets the value of the component property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOMPONENT() {
            return component;
        }

        /**
         * Sets the value of the component property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOMPONENT(String value) {
            this.component = value;
        }

        /**
         * Gets the value of the scode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSCODE() {
            return scode;
        }

        /**
         * Sets the value of the scode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSCODE(String value) {
            this.scode = value;
        }

        /**
         * Gets the value of the xref property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXREF() {
            return xref;
        }

        /**
         * Sets the value of the xref property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXREF(String value) {
            this.xref = value;
        }

        /**
         * Gets the value of the branchcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBRANCHCODE() {
            return branchcode;
        }

        /**
         * Sets the value of the branchcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBRANCHCODE(String value) {
            this.branchcode = value;
        }

        /**
         * Gets the value of the componentccy property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOMPONENTCCY() {
            return componentccy;
        }

        /**
         * Sets the value of the componentccy property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOMPONENTCCY(String value) {
            this.componentccy = value;
        }

        /**
         * Gets the value of the amountdue property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAMOUNTDUE() {
            return amountdue;
        }

        /**
         * Sets the value of the amountdue property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAMOUNTDUE(BigDecimal value) {
            this.amountdue = value;
        }

        /**
         * Gets the value of the amountsettled property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAMOUNTSETTLED() {
            return amountsettled;
        }

        /**
         * Sets the value of the amountsettled property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAMOUNTSETTLED(BigDecimal value) {
            this.amountsettled = value;
        }

        /**
         * Gets the value of the accruedamount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getACCRUEDAMOUNT() {
            return accruedamount;
        }

        /**
         * Sets the value of the accruedamount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setACCRUEDAMOUNT(BigDecimal value) {
            this.accruedamount = value;
        }

        /**
         * Gets the value of the componenttype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOMPONENTTYPE() {
            return componenttype;
        }

        /**
         * Sets the value of the componenttype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOMPONENTTYPE(String value) {
            this.componenttype = value;
        }

        /**
         * Gets the value of the latestintrate property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLATESTINTRATE() {
            return latestintrate;
        }

        /**
         * Sets the value of the latestintrate property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLATESTINTRATE(BigDecimal value) {
            this.latestintrate = value;
        }

        /**
         * Gets the value of the emiamount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getEMIAMOUNT() {
            return emiamount;
        }

        /**
         * Sets the value of the emiamount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setEMIAMOUNT(BigDecimal value) {
            this.emiamount = value;
        }

        /**
         * Gets the value of the adjustedamount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getADJUSTEDAMOUNT() {
            return adjustedamount;
        }

        /**
         * Sets the value of the adjustedamount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setADJUSTEDAMOUNT(BigDecimal value) {
            this.adjustedamount = value;
        }

    }

}
