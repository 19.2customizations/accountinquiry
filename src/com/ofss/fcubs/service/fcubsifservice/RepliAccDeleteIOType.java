
package com.ofss.fcubs.service.fcubsifservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RepliAcc-Delete-IO-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliAcc-Delete-IO-Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BRNCDE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="REP_ACC_NUM" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ACCCCY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliAcc-Delete-IO-Type", propOrder = {
    "brncde",
    "repaccnum",
    "accccy"
})
public class RepliAccDeleteIOType {

    @XmlElement(name = "BRNCDE", required = true)
    protected String brncde;
    @XmlElement(name = "REP_ACC_NUM", required = true)
    protected String repaccnum;
    @XmlElement(name = "ACCCCY", required = true)
    protected String accccy;

    /**
     * Gets the value of the brncde property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRNCDE() {
        return brncde;
    }

    /**
     * Sets the value of the brncde property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRNCDE(String value) {
        this.brncde = value;
    }

    /**
     * Gets the value of the repaccnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREPACCNUM() {
        return repaccnum;
    }

    /**
     * Sets the value of the repaccnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREPACCNUM(String value) {
        this.repaccnum = value;
    }

    /**
     * Gets the value of the accccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCCCY() {
        return accccy;
    }

    /**
     * Sets the value of the accccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCCCY(String value) {
        this.accccy = value;
    }

}
