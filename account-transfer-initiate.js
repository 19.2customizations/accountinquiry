define([
    "knockout",
    "ojL10n!extensions/resources/nls/account-transfer",
    "./model",
    "ojs/ojbutton",
    "ojs/ojinputtext"
], function(ko, ResourceBundle, AccountTransferModel) {
    "use strict";

    return function(rootParams) {
		
       const self = this,
	   
      getNewKoModel = function() {
        const KoModel = ko.mapping.fromJS(AccountTransferModel.getNewModel());

        return KoModel;
      };
	  
	    self.payload = getNewKoModel().accountTransfer;
		self.resource = ResourceBundle;
		self.stageOne = ko.observable(true);
		self.stageTwo = ko.observable(false);
		self.readData = ko.observable();
		//Defining the variables 
		self.senderAccountNo = ko.observable();
		self.senderName = ko.observable();
		self.beneficiaryAccountNo = ko.observable();
		self.beneficiaryAccountSelf = ko.observable();
		self.beneficiaryName = ko.observable();
		self.amount = ko.observable(0);
		self.exchRate = ko.observable(0);
		self.receiveAmount = ko.observable(0);
		self.tranType = ko.observable();
		self.transferWhen = ko.observable();
		self.transferType = ko.observable('Others');
		self.beneficiaryEmail = ko.observable();
		self.purpose = ko.observable();
		
		self.selfTransfer = ko.observable(false);
		self.sourceAccountDetails = ko.observable();
		self.beneficiaryAccountDetails = ko.observable();
		self.additionalDetails = ko.observable();
		
		//self.accountList = rootParams.account;
		
		//alert(self.accountList);
			
		
        ko.utils.extend(self, rootParams.rootModel);
        //self.alternateLogin = ko.observable();

		 rootParams.dashboard.headerName(self.resource.header);
		//rootParams.baseModel.registerComponent("review-Passport-Detail","passport");
 
        //rootParams.dashboard.headerCaption(self.nls.loginForm.labels.subHeader);
        //rootParams.baseModel.registerComponent("user-credentials", "registration");
		//rootParams.baseModel.registerComponent("review-Passport-Detail","review-Passport-Detail");

      //  let GenericViewModel = null;

       // self.showPopup = false;


		//self.payload.passportNumber = self.passportNumber();
		//self.payload.mobileNumber = self.mobileNumber();
		
		
		self.transferPeriodArray = [{
                        id: "Now",
                        label: self.resource.AccountTransfer.transferNow
                    },
                    {
                        id: "Later",
                        label: self.resource.AccountTransfer.transferLater
                    }
        ];
		
		self.transferTypeArray = [{
                        id: "Self",
                        label: self.resource.AccountTransfer.transferTypeOwn
                    },
                    {
                        id: "Others",
                        label: self.resource.AccountTransfer.transferTypeOthers
                    }
        ];
		
		
		 var additionalDetails = self.additionalDetails.subscribe(function(newValue) {
			 
			 alert('yy ' + newValue.account.id.value);
			 
			 alert(additionalDetails);
			 
               
          });
		  
		  
		  
		 self.beneficiaryAccountDetails.subscribe(function(newValue) {
			 
			 alert('yy ' + newValue.account.id.value);
               
          });
		  
    self.senderAccountNo.subscribe(function(newValue) {
		
		var sourceAccountDetails = self.additionalDetails().account;
		
		alert(sourceAccountDetails);
		
       
	   /*alert('leave: ' + newValue.account);
	   
	  //alert('a3:' + additionalDetails) ; //.account.id.displayValue);
	   
	   alert('a1:' + newValue.account.id.value);
	   alert('a2:' + newValue.account.id);
	  
	  alert("sender: " + self.senderAccountNo());*/
		   
	  
    });
		  
		
    self.beneficiaryAccountNo.subscribe(function(value) {
      /*if (self.currentAccountType() === "INTERNAL" && value !== "" && value.indexOf("undefined") === -1) {
        AdhocPaymentModel.validateAndFetchCurrency(value).done(function(data) {
          self.customCurrencyURL("payments/currencies?type=INTERNALFT&currency=" + data.currencyCode);
        });
      }*/
	  
		
		    AccountTransferModel.getAccountNameInquiry(self.beneficiaryAccountNo()).done(function (data) {
			       //alert("Name: " + data.accountName);
				   self.beneficiaryName(data.accountName);
            });
			
	  
    });
		
		
		
		self.transferTypeChange = function(event) {
			
			alert('transfer -- ' + event.detail.value + ' ' + self.transferTypeArray[0].id);
			if (event) {
				
				if (event.detail.value === "Self") 
				{
				    self.transferType('Self');
					self.selfTransfer(true);
				}
				 else if (event.detail.value === "Others")
				 {
					 self.transferType('Others');
					 self.selfTransfer(false);
				 }
			}
		
        };
		
		
		self.transferWhenChange = function(event) {
			
			alert('transfer -- ' + event.detail.value + ' ' + self.transferWhenArray[0].id);
			if (event) {
				
				if (event.detail.value === "Now") 
				    self.transferWhen('Now');
				 else if (event.detail.value === "Later")
					 self.transferWhen('Now');
			}
		
        };
		
		
		
        self.submit = function() {
			
			self.payload.senderAccountNo(self.senderAccountNo());
			self.payload.senderName(self.senderName());
		    self.payload.beneficiaryAccountNo(self.beneficiaryAccountNo());
		
		    self.payload.beneficiaryName(self.beneficiaryName());
		
		//alert ('Submit: ' + self.payload.senderAccountNo() );
		var uidx = self.generateUid();
		self.payload.externalRefNo(uidx);
		self.payload.amount(1);
		//alert(uidx);
		
		//self.payload.amount = self.amount();
		//self.payload.exchRate = self.exchRate();
		//self.payload.receiveAmount = self.receiveAmount();
		//self.payload.tranType = self.tranType();
		
			
		AccountTransferModel.sendAccountTransfer(ko.toJSON(self.payload)).done(function(data) {
       
	      self.readData = data;
          self.stageOne(false);
		  self.stageTwo(true);
      });
            //
			
			
     };
	 
	 self.validateSenderAccount = function () {
           //alert("I am in validate account: " + self.senderAccountNo());
		   
		    AccountTransferModel.getAccountNameInquiry(self.senderAccountNo()).done(function (data) {
			       alert("Name: " + data.accountName);
				   self.senderName(data.accountName);
            });
		   
        };
		
		self.validateBeneficiaryAccount = function () {
           //alert("I am in validate account: " + self.beneficiaryAccountNo());
		   
		    AccountTransferModel.getAccountNameInquiry(self.beneficiaryAccountNo()).done(function (data) {
			       //alert("Name: " + data.accountName);
				   self.beneficiaryName(data.accountName);
            });
		   
        };
		
		
		
		self.generateUid = function()
		{
		   var chars = '0123456789abcdef'.split('');

		   var uuid = [], rnd = Math.random, r;
		   uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
		   uuid[14] = '4'; // version 4

		   for (var i = 0; i < 36; i++)
		   {
			  if (!uuid[i])
			  {
				 r = 0 | rnd()*16;

				 uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
			  }
		   }

		   return uuid.join('');
		}

        

       
    };
});